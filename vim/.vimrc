" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" Investigate me {{{
" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif
" }}}

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" no write backup
" set nowritebackup

" no swap files
set noswapfile

if has("vms")
  set nobackup          " do not keep a backup file, use versions instead
else
  set backup            " keep a backup file
endif
set history=100          " keep 100 lines of command line history
set ruler               " show the cursor position all the time
set showcmd             " display incomplete commands
set incsearch           " do incremental searching
set smartcase           " ignore case in search
set ignorecase          " ignore \c escape character in searches
set nowrap              " turn word wrap off

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

  augroup fileTypes

  " Ansible indentation correction
  autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

  " HTML formatting
  autocmd FileType html setlocal ts=2 sts=2 sw=2 expandtab

  " Python settings
  autocmd FileType py setlocal ts=4 sts=4 sw=4 expandtab

  " C indentation
  autocmd FileType c setlocal ts=4 sts=4 sw=4 expandtab

  " git formatting
  autocmd FileType gitcommit setlocal spell textwidth=72

  " markdown formatting
  autocmd FileType markdown setlocal wrap linebreak nolist textwidth=0 wrapmargin=0
  autocmd Bufread,BufNewFile *.md set filetype=markdown " vim interprets .md as 'modula2' otherwise

  " recipe formatting
  autocmd FileType recipe setlocal wrap linebreak nolist textwidth=0 wrapmargin=0
  autocmd Bufread,BufNewFile *.recipe set filetype=markdown

  " various shells
  autocmd FileType sh,cucumber,ruby,yaml,zsh,vim setlocal shiftwidth=2 tabstop=2 expandtab

  augroup END

endif " has("autocmd")

" highlight words to avoid in tech writing
" http://css-tricks.com/words-avoid-educational-writing/
highlight TechWordsToAvoid ctermbg=red ctermfg=white
match TechWordsToAvoid /\cobviously\|basically\|simply\|of\scourse\|clearly\|just\|everyone\sknows\|however\|so,\|easy/
autocmd BufWinEnter * match TechWordsToAvoid /\cobviously\|basically\|simply\|of\scourse\|clearly\|just\|everyone\sknows\|however\|so,\|easy/
autocmd InsertEnter * match TechWordsToAvoid /\cobviously\|basically\|simply\|of\scourse\|clearly\|just\|everyone\sknows\|however\|so,\|easy/
autocmd InsertLeave * match TechWordsToAvoid /\cobviously\|basically\|simply\|of\scourse\|clearly\|just\|everyone\sknows\|however\|so,\|easy/
autocmd BufWinLeave * call clearmatches()

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
                  \ | wincmd p | diffthis
endif

set number                    " show line numbers
set relativenumber            " relative line numbers
set cursorline                " show underline for cursor line
set autoindent                " always set autoindenting on
set expandtab                 " convert tabs to spaces
set tabstop=4                 " set tab size in spaces
set softtabstop=4             " unclear rn TODO
set shiftwidth=4              " num of spaces < or > shifts indent
set list listchars=tab:\ \ ,trail:·   " highlight trailing whitespace
set autoread                  " autoload files changed outside session
set clipboard+=unnamed        " use system clipboard
set shortmess+=I              " hide intro screen
set splitbelow                " better splits vvv
set splitright                " (new windows appear below and to the right)
set visualbell                " vim doesn't yell at me anymore
set wildmenu                  " visual autocomplete for command menu
set showmatch                 " highlight matching paren(s)
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*.,*/.DS_Store  " ignore these filetypes when expanding wildcards

" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with spacebar
nnoremap <space> za

" set cc=79

" places temp files in a special directory! Don't forget to clean this out.
set backupdir=~/.vimtemp//
set directory=~/.vimtemp//

" map key command for exiting insert in terminal mode
tnoremap <Esc> <C-\><C-n>

" vim-airline configurations {{{

" displays all buffers when there's only one one tab open
let g:airline#extensions#tabline#enabled = 1

" theme settings
" let g:airline_theme='base16_chalk'
" let g:airline_theme='base16_railscasts'
let g:airline_theme='zenburn'
" let g:airline_theme='understated'

" }}}

" better rainbow parenthesis {{{

" download https://github.com/kien/rainbow_parentheses.vim
if exists(':RainbowParenthesesToggle')
  autocmd VimEnter * RainbowParenthesesToggle
  autocmd Syntax * RainbowParenthesesLoadRound
  autocmd Syntax * RainbowParenthesesLoadSquare
  autocmd Syntax * RainbowParenthesesLoadBraces
endif

" }}}

" change colorscheme when diffing
fun! SetDiffColors()
  highlight DiffAdd     cterm=bold ctermfg=white cterm=DarkGreen
  highlight DiffDelete  cterm=bold ctermfg=white cterm=DarkGrey
  highlight DiffChange  cterm=bold ctermfg=white cterm=DarkBlue
  highlight DiffText    cterm=bold ctermfg=white cterm=DarkRed
endfun
autocmd FilterWritePre * call SetDiffColors()

" Pathogen set up
execute pathogen#infect()
