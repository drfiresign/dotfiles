#!/bin/sh

NEWHOME=$(pwd)

check() {
    if [[ "$1" == '!' ]]; then
        if
            command -v "$2" &>/dev/null; then
            return 1
        else
            return 0
        fi
    else
        if
            command -v "$1" &>/dev/null; then
            return 0
        else
            return 1
        fi
    fi
}

# Make sure you are able to use your yubikeys:
# https://fedoraproject.org/wiki/Using_Yubikeys_with_Fedora

echo installing ubuntu packages...
sudo apt install -y \
     # pam_yubico \
     aspell \
     bash \
     bash-completion \
     blueman \
     calibre \
     cmake \
     coreutils \
     cowsay \
     dbus \
     fontconfig \
     golang \
     htop \
     httpie \
     mailutils \
     neovim \
     nodejs \
     notmuch \
     npm \
     pandoc \
     pasystray \
     ripgrep \
     stow \
     surf \
     surfraw \
     tldr \
     tmux \
     tree \
     wireless-tools \
     wordnet \
     zoxide

echo installing snaps...
# snap install emacs --beta --classic
# snap install mailspring

echo installing npm packages...
sudo npm install -g \
     bash-language-server \
     dockerfile-language-server-nodejs \
     eslint \
     indium \
     javascript-typescript-langserver \
     js-beautify \
     markdownlint \
     n \
     npm \
     npm-check \
     prettier \
     stylelint \
     tree-sitter-cli \
     typescript-language-server \
     yaml-language-server


echo installing python tools...
pip install --user \
    black \
    flake8 \
    isort \
    nose \
    pipenv \
    pyflakes \
    pytest \
    rope
