STYLESHEET=$(grep '^include .*/stylesheets/.*/sway' $XDG_CONFIG_HOME/sway/config | cut -f2 -d' ')

background=$(grep background "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #101010;
foreground=$(grep foreground "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #FFFFFF;
border=$(grep border "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #202020;

activeForeground=$(grep activeForeground "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #7E7E7E;
inactiveForeground=$(grep inactiveForeground "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #707070;
activeBackground=$(grep activeBackground "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #161616;
inactiveBackground=$(grep inactiveBackground "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #202020;

activeRegionForeground=$(grep activeRegionForeground "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #FFC799;
inactiveRegionBackground=$(grep inactiveRegionBackground "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #232323;
selectionBackground=$(grep selectionBackground "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #1C1C1C;
selectionHighlightBackground=$(grep selectionHighlightBackground "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #A0A0A0;

green=$(grep green "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #99FFE4;
greenFaint=$(grep greenFaint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #7accb6;
greenTint=$(grep greenTint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #3d665b;
greenSlight=$(grep greenSlight "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #1e332d;
greenSubtle=$(grep greenSubtle "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #0f1916;

orange=$(grep orange "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #FF7300;
orangeFaint=$(grep orangeFaint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #cc5b00;
orangeTint=$(grep orangeTint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #662d00;
orangeSlight=$(grep orangeSlight "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #331600;
orangeSubtle=$(grep orangeSubtle "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #190b00;

red=$(grep red "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #FF8080;
redFaint=$(grep redFaint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #cc6666;
redTint=$(grep redTint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #663333;
redSlight=$(grep redSlight "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #331919;
redSubtle=$(grep redSubtle "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #190c0c;

yellow=$(grep yellow "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #FFC799;
yellowFaint=$(grep yellowFaint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #cc9f7a;
yellowTint=$(grep yellowTint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #664f3d;
yellowSlight=$(grep yellowSlight "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #33271e;
yellowSubtle=$(grep yellowSubtle "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #19130f;

magenta=$(grep magenta "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #FBADFF;
magentaFaint=$(grep magentaFaint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #c88acc;
magentaTint=$(grep magentaTint "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #966899;
magentaSlight=$(grep magentaSlight "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #644566;
magentaSubtle=$(grep magentaSubtle "$STYLESHEET" | cut -f3 -d' ' | tr -d ';#') #322233;
