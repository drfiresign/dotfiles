#!/usr/bin/env bash

light -G | cut -d'.' -f1 | xargs ~/.config/wob/run.sh
