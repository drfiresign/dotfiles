#!/usr/bin/env bash
gammastep_active=$(systemctl --user is-active gammastep.service)

if [[ "$gammastep_active" == "active" ]]; then
    systemctl --user stop gammastep.service
else
    systemctl --user start gammastep.service
fi
