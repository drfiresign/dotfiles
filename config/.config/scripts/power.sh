#!/usr/bin/env bash
case "$1" in
    "lock")
        swaynag -t extreme \
                -m "EXAMPLE LOCK" \
                -s 'No, cancel'

        sleep 0.25
        bash ${XDG_CONFIG_HOME}/sway/scripts/lock.sh
        ;;
    "suspend")
        sleep 0.25
        systemctl suspend
        ;;
    "reboot")
        swaynag -t extreme \
                -m "Are you sure you want to reboot?" \
                -B 'Yes, reboot' 'systemctl reboot' \
                -s 'No, cancel'
        ;;
    "logout")
        swaynag -t warn \
                -m "Are you sure you want to logout? This will end your Wayland session." \
                -B 'Yes, logout' 'swaymsg exit' \
                -s 'No, cancel'
        ;;
    "shutdown")
        swaynag -t extreme \
                -m "Are you sure you want to shutdown?" \
                -B 'Yes, shutdown' 'systemctl poweroff' \
                -s 'No, cancel'
        ;;
esac
