#!/bin/bash

# sudo cp laptop-lid.sh /etc/acpi/laptop-lid.sh
# chmod +x /etc/acpi/laptop-lid.sh
# echo "event=button/lid.*\naction=/etc/acpi/laptop-lid.sh" > sudo tee /etc/acpi/events/laptop-lid
# sudo service acpid restart

lock=$HOME/.cache/fprint-disabled

if /bin/grep -Fq /proc/acpi/button/lid/LID0/state &&
        /bin/grep -Fxq connected /sys/class/drm/card1/card1-DP-2/status
then
    touch "$lock"
    systemctl stop fprintd
    systemctl mask fprintd
elif [ -f "$lock" ]
then
    systemctl unmask fprintd
    systemctl start fprintd
    rm "$lock"
fi
