XDG_CONFIG_HOME="$HOME/.config"
XDG_BASH="$XDG_CONFIG_HOME/bash/bashrc"
LOCAL_BASH="$HOME/.bashrc"

# if [[ -f $XDG_BASH ]]; then
#     source $XDG_BASH
# elif [[ -f $LOCAL_BASH ]]; then
#     source $LOCAL_BASH
# fi

source "$XDG_CONFIG_HOME/common-shell/05-truisms" && jenny_cat
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
export PATH=$PATH:$HOME/.rd/bin
