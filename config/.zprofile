# -*- mode: sh -*-

if [[ -z "$XDG_CONFIG_HOME" ]] && [[ -d "$XDG_CONFIG_HOME/zsh" ]]; then
    export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
fi

source "$HOME/.common/05-truisms" && jenny_cat
export PATH=$PATH:$HOME/.rd/bin
