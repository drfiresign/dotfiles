#!/bin/sh

NEWHOME=$(pwd)

check() {
    if [[ "$1" == '!' ]]; then
        if
            command -v "$2" &>/dev/null; then
            return 1
        else
            return 0
        fi
    else
        if
            command -v "$1" &>/dev/null; then
            return 0
        else
            return 1
        fi
    fi
}

echo Make sure you are able to use your yubikeys:
echo https://fedoraproject.org/wiki/Using_Yubikeys_with_Fedora

echo installing fedora packages...
sudo dnf upgrade -y
sudo dnf install -y \
     aspell \
     aspell-en \
     autoconf \
     bash \
     bash-completion \
     bat \
     bind-utils \
     cmake \
     coreutils \
     cowsay \
     dbus-devel \
     dotnet-sdk-5.0 \
     eza \
     fd-find \
     fish \
     giflib-devel \
     glibc-all-langpacks \
     gnutls-devel \
     golang \
     htop \
     httpie \
     hyperfine \
     java-1.8.0-openjdk-devel \
     java-11-openjdk-devel \
     jq \
     kubernetes-client \
     libXaw-devel \
     libjpeg-devel \
     libpng-devel \
     librsvg2-devel \
     libtiff-devel \
     libxml2-devel \
     ncurses-devel \
     neovim \
     nmap-ncat \
     nodejs \
     openssl-devel \
     pam_yubico \
     pandoc \
     postgresql \
     python3 \
     python3-devel \
     python3-pip \
     racket \
     ripgrep \
     stow \
     surfraw \
     systemd-devel \
     telnet \
     texinfo \
     tldr \
     tmux \
     tokei \
     w3m \
     wireless-tools \
     wob \
     xclip \
     yarnpkg \
     zoxide

sudo dnf install -y @development-tools
dnf clean all

echo installing npm packages...
sudo yarn global add  \
     bash-language-server \
     dockerfile-language-server-nodejs \
     eslint \
     indium \
     js-beautify \
     markdownlint \
     n \
     prettier \
     stylelint \
     tree-sitter-cli \
     typescript-language-server \
     yaml-language-server

# javascript-typescript-langserver \
    # npm \
    # npm-check \


echo installing python tools...
pip3 install --user \
     black \
     flake8 \
     isort \
     nose \
     pipenv \
     pyflakes \
     pytest \
     rope \
     virtualfish     # todo: finish set up of vf
