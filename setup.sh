#!/bin/sh
#title          :setup.sh
#description    :This script properly sets up my local environment
#instructions   :Run this inside ~/.dotfiles/
#author         :drfiresign (with thanks to hrs and ghaiklor)
#date           :2019-01-20
#version        :0.1
#usage          :bash <(curl -s )
#===========================================================================

CWD=$HOME/dotfiles

mkdir -p ~/code/languages/python
mkdir -p ~/code/languages/perl
mkdir -p ~/code/languages/lisp
mkdir -p ~/code/languages/ruby
mkdir -p ~/code/languages/javascript

mkdir -p ~/bin
mkdir -p ~/code/work

stow alacritty
stow shell
stow emacs
stow errata -t ~/code/
stow git
# stow org
stow roswell
stow tmux
stow vim
# stow config
stow starship

cd $CWD/errata/ && stow custom-scripts -t ~/bin/ && cd $CWD

# these shouldn't be necessary unless the bash_profile doesn't work
# # link .bash_profile > .bashrc
# rm -f ~/.bash_profile
# ln -s ~/.bashrc ~/.bash_profile
