#!/usr/bin/env bash
songwhip() {
  if which https >/dev/null; then
    https songwhip.com url=$1 | jq -r .url | pbcopy
  else
    data=$(printf '{"url":"%s"}' $1)
    curl -X POST https://songwhip.com/ -d $data | jq -r .url | pbcopy
  fi
}
