#!/usr/bin/env bash

# bash completion?
source /usr/local/etc/bash_completion.d/**
[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. $NVM_DIR/bash_completion# This loads nvm bash_completion
if [[ -e "$GOPATH/bin/jira" ]] ; then eval "$(jira --completion-script-bash)"; fi
