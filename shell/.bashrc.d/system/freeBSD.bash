# aliases
alias gls="gnuls"
alias ls="gls -NF --color --ignore='.DS_Store' --ignore='.CFUser*'" # use gnu ls and add colors
if [[ -x nvim ]] ; then
	alias vi="nvim"
fi

# variables
export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export MM_CHARSET="UTF-8"
