#!/usr/bin/env bash

# linux specific aliases for wayland
alias pbcopy=wl-copy
alias pbpaste=wl-paste
