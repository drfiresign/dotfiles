export TITLE=$(pwd)

# emacs vterm ; from the vterm config
function vterm_printf(){
  if [ -n "$TMUX" ]; then
    # Tell tmux to pass the escape sequences through
    # (Source: http://permalink.gmane.org/gmane.comp.terminal-emulators.tmux.user/1324)
    printf "\ePtmux;\e\e]%s\007\e\\" "$1"
  elif [ "${TERM%%-*}" = "screen" ]; then
    # GNU screen (screen, screen-256color, screen-256color-bce)
    printf "\eP\e]%s\007\e\\" "$1"
  else
    printf "\e]%s\e\\" "$1"
  fi
}

if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
  if [[ -n "$EMACS_VTERM_PATH" ]] && [[ -f ${EMACS_VTERM_PATH}/etc/emacs-vterm-bash.sh ]]; then
    source "$EMACS_VTERM_PATH"/etc/emacs-vterm-bash.sh
    export TITLE=$(pwd)
  fi

  function clear(){
    vterm_printf "51;Evterm-clear-scrollback";
    tput clear;
  }
fi

vterm_cmd() {
  local vterm_elisp
  vterm_elisp=""
  while [ $# -gt 0 ]; do
    vterm_elisp="$vterm_elisp""$(printf '"%s" ' "$(printf "%s" "$1" | sed -e 's|\\|\\\\|g' -e 's|"|\\"|g')")"
    shift
  done
  vterm_printf "51;E$vterm_elisp"
}

vterm_prompt_end(){
  # vterm_printf "51;A$(pwd);"
  vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
  # vterm_printf "51;A$(pwd);"
}

PS1=$PS1'\[$(vterm_prompt_end)\]'

# emacs commands inside vterm

# adding a command here also needs to be "allowed" via the `vterm-eval-cmds` custom
# section of ~/.emacs.d/modules/dc-terminal.el

ff() {
  vterm_cmd find-file "$(realpath "${@:-.}")"
}

ffo() {
  vterm_cmd find-file-other-window "$(realpath "${@:-.}")"
}

man() {
  vterm_cmd man "$*"
}

view() {
  vterm_cmd find-file-read-only "$*"
}

say() {
  vterm_cmd message "%s" "$*"
}

dired() {
  vterm_cmd dired "$*"
}
alias d="dired"

PROMPT_COMMAND=__prompt_command

# export JIRA_EDITOR=ff
