# application specific
if which zoxide >/dev/null; then eval "$(zoxide init bash)"; fi
if which starship >/dev/null; then eval "$(starship init bash)"; fi
[[ -f "$HOME/.fzf.bash" ]] && source "$HOME/.fzf.bash"
[[ -f "$HOME/.cargo/env" ]] && source "$HOME/.cargo/env"
