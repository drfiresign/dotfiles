collectpath () {
  if ! echo $PATH | grep -qx -E "(^|:)$1($|:)" ; then
    if [ "$2" = "after" ] ; then
      PATH=$PATH:$1
    else
      PATH=$1:$PATH
    fi
  fi
}

### general use
collectpath /usr/local/bin/
collectpath /usr/local/opt/
collectpath /usr/local/sbin/
collectpath /usr/bin

collectpath $HOME/bin after # custom bin directory
# collectpath $HOME/.cargo/env after # rust
. "$HOME/.cargo/env"


### for potential removal
# collectpath $HOME/.local/bin
# collectpath $HOME/.roswell/ after # lisp
