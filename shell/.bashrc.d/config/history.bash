#!/usr/bin/env bash

### history settings
# variables
export HISTSIZE=10000
export HISTFILESIZE=10000
export HISTIGNORE="ls:ll:la:el:eva:et:exit:clear:z:cd:top:htop*:history*:rm*:pwd:df:du:git status:which*"
export HISTCONTROL="ignoreboth:erasedups"
export HISTIMEFORMAT="%m/%d: "

stty -ixon # enable ctrl-s isearch-forward
shopt -s histappend # append to the history file, don't overwrite it
shopt -s cmdhist # write a multi line command in a single line
