#!/usr/bin/env zsh

# Most of this file is taken from the thoughtbot dotfiles repo. It loads custom functions, then iterates through the ~/.zsh directory, loading the individual configuration files located in there. Thanks thoughtbot, your inspiration is always appreciated.

# Nix
if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
    . '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi
# End Nix


# load custom executable functions
for function in ~/.zshrc.d/functions/*; do
    source $function
done

_load_settings() {
    _dir="$1"
    if [ -d $_dir ]; then
        for config in $_dir/*; do
            . $config
        done
    fi
}

_load_settings ~/.common
_load_settings ~/.zshrc.d/config
[[ -f ~/.zshrc.local ]] && source ~/.zshrc.local
[[ -f ~/.common.local ]] && source ~/.common.local
[[ -f ~/.secrets ]] && source ~/.secrets
[[ -f ~/.zshrc.d/post.zsh ]] && source ~/.zshrc.d/post.zsh
alias reload="source ~/.zshrc"
eval "$(atuin init zsh)"
