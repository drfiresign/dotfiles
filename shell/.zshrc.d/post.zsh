
autoload -U +X bashcompinit && bashcompinit
autoload -Uz compinit && compinit
complete -o nospace -C /usr/local/bin/vault vault
complete -F _ssh_autocomplete ssh
