#!/usr/bin/env zsh

zmodload zsh/compctl
zmodload zsh/complete
zmodload zsh/complist
zmodload zsh/computil
zmodload zsh/datetime
zmodload zsh/main
zmodload zsh/mathfunc
zmodload zsh/parameter
zmodload zsh/terminfo
zmodload zsh/zle
zmodload zsh/zleparameter
zmodload zsh/zutil
