export ALTERNATE_EDITOR="/usr/local/Cellar/emacs-plus@29/29.0.50/bin/emacsclient -Q"
export VISUAL="/usr/local/Cellar/emacs-plus@29/29.0.50/bin/emacsclient" # $VISUAL opens in GUI mode
export EDITOR="$VISUAL" # $EDITOR opens in terminal
