autoload -U select-word-style
autoload -Uz run-help run-help-git run-help-ip run-help-openssl run-help-p4
autoload -Uz run-help-sudo run-help-svk run-help-svn
(( ${+aliases[run-help]} )) && unalias run-help
alias help=run-help

select-word-style shell

# taken from the zsh man pages
# kills whole word to next space character
# zle -N backward-kill-space-word backward-kill-word-match
# zstyle :zle:backward-kill-space-word word-style space

zstyle ':zle:*' word-context \
       "*/*" filename "[[:space:]]" whitespace
zstyle ':zle:transpose-words:whitespace' word-style shell
zstyle ':zle:transpose-words:filename' word-style normal
zstyle ':zle:transpose-words:filename' word-chars ''
