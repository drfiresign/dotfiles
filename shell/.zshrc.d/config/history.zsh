setopt hist_ignore_all_dups inc_append_history
export HISTIGNORE="ls:ll:la:el:eva:et:exit:clear:z:cd:top:htop*:history*:rm*:pwd:df:du:git status:which*"
export HISTSIZE=8192
export SAVEHIST=8192
