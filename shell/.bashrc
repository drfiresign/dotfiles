#!/usr/bin/env bash

# load custom executable functions
# for function in "$HOME"/.bashrc.d/functions/*; do
#     source $function
# done

# Nix
if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
    source '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi
# End Nix


_load_settings() {
    _dir="$1"
    if [ -d $_dir ]; then
        for config in $_dir/*; do
            . $config
        done
    fi
}
#_load_settings ~/.bashrc.d/config/
_load_settings ~/.common/

[[ $OSTYPE == "freebsd"* ]] && source ~/.bashrc.d/system/freeBSD.bash
[[ $OSTYPE == "linux-gnu"* ]] && source ~/.bashrc.d/system/linux.bash
[[ $TERM == "emacs" || $INSIDE_EMACS == "vterm" ]] && source ~/.bashrc.d/system/emacs.bash

### local config settings, if any
[[ -f ~/.bashrc.local ]] && source ~/.bashrc.local
[[ -f ~/.common.local ]] && source ~/.common.local
[[ -f ~/.secrets ]] && source ~/.secrets

alias reload="source ~/.bashrc"
