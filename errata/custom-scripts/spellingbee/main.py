"""Calculate a list of valid words for NYT Games Spelling Bee."""
import itertools
import argparse
import re
import enchant

from pathlib import Path

en_DICT = enchant.Dict('en')
DEFAULT_DICT_DIR = Path('/usr/share/dict')
EXCLUDE_DICTS = "readme,propernames"
MAX_LEN = 9

parser = argparse.ArgumentParser(
    prog='spelling-bee',
    add_help=False,
    conflict_handler='resolve',
    allow_abbrev=True,
    description="Generate a list of possible word entries for the NYT Game, Spelling Bee.")

parser.add_argument('letters', help='letters to spell (enter center letter first)')
parser.add_argument('-l', '--limit', help='only print words with LENGTH',
                    dest='limit', type=int, required=False)
parser.add_argument('-m', '--max', help='only generate words up to MAX length',
                    dest='max_len', type=int, required=False)
parser.add_argument('-p', '--prefix', help='only generate words with PREFIX',
                    dest='prefix', type=str, default='', required=False)
parser.add_argument('-v', '--verbose', help='print explanatory messages',
                    dest='verbose', type=bool, default=False, required=False)
parser.add_argument('--dict', help='user dictionary file to use',
                    type=argparse.FileType('r'), required=False)
parser.add_argument('--dir', help='directory containing user dictionaries',
                    default=DEFAULT_DICT_DIR, type=Path, required=False)
parser.add_argument('--exclude', help='comma separated dictionary names to exclude',
                    default=EXCLUDE_DICTS,
                    type=lambda x: x.lower().split(','),
                    required=False)
parser.add_argument('--lines', help='print one entry per line', action='store_true')
args = parser.parse_args()

if not args.dir.exists():
    parser.error(f"Dictionary directory {args.dir.name} does not exist!")
    parser.exit()

DICTS = filter(lambda x: x.is_file() and x.name.lower() not in args.exclude, args.dir.iterdir())
LIMIT = args.limit is not None
MAX = args.max_len is not None
SEP = '\n' if args.lines else ' '
WORD_LENGTH = args.max_len + 1 if MAX else MAX_LEN


def limit_len(word):
    """WORD is less than WORD_LENGTH"""
    return len(word) <= WORD_LENGTH


def limit_one(word):
    """WORD is exactly LIMIT length"""
    return len(word) == args.limit


def limit_min(word):
    """WORD is longer than 3"""
    return len(word) > 3


def generate_spelling_bee_entries(center, letters, prefix=''):
    """Generate english word entries for the NYT Games Spelling Bee title."""
    letters = ''.join(letters)
    regexp = fr"^{prefix}[{letters}]*{center}[{letters}]*$"
    pattern = re.compile(regexp, re.I | re.M)

    matches = []
    for dictionary in DICTS:
        with dictionary.open() as file:
            matches.append(pattern.findall(file.read()))

    flat_matches = itertools.chain.from_iterable(matches)
    flat_word_list = map(lambda x: x.lower(), flat_matches)
    # return filter(en_DICT.check, flat_word_list)
    return flat_word_list

if __name__ == "__main__":
    count = len(args.letters)
    if count != 7:
        parser.error(f'Spelling bee entries have 7 letters, you only entered {count}')

    words = generate_spelling_bee_entries(args.letters[0], sorted(args.letters), prefix=args.prefix)
    words = filter(limit_min, words)

    if MAX:
        words = filter(limit_len, words)
    if LIMIT:
        words = filter(limit_one, words)

    unique = [k for k, g in itertools.groupby(sorted(words))]
    print(*unique, sep=SEP)
    parser.exit()
