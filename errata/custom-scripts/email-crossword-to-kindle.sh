#!/usr/bin/env bash

# no need to worry about a directory with mktemp
dir=$(mktemp -d)

# separate url string for readability; arguably these query params are not necessary either
puzzle_url="https://nyt-games-prd.appspot.com/svc/crosswords/v3/36569100/puzzles.json"

# no cookies are required to get a response from this endpoint, -s to hide progress bar
puzzid=$(curl -s -b cookies.txt $puzzle_url | jq '.results[0].puzzle_id')
pdf_url="https://www.nytimes.com/svc/crosswords/v2/puzzle/$puzzid.pdf"

# avoid using eval in any script, curl by itself should be just fine here. you can also skip
# renaming the file by formatting the date more precisely then using curl flag --output
file_name="$(date +%b%d%y-%A.pdf)"
curl -s -b cookies.txt $pdf_url --output $dir/$file_name

echo "daily crossword" | /usr/bin/mutt -s "Daily Crossword" -a $file_name -- youremail@kindle.com

# clean up
rm -r $dir
