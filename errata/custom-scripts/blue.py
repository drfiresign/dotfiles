#!/usr/bin/env python3
"""Connect, disconnect, and query status of previously paired Bluetooth devices."""

import subprocess, json, re, argparse
from sys import argv


SWW = "Connection failure, something went wrong"
CONNECTION_REGEXP = re.compile(r'--(?:dis)?connect$')
ADDRESS_REGEXP = re.compile(r'(?:[\d\w]{2}-?){6}')
change_status = lambda x: bool([j for j in [CONNECTION_REGEXP.match(i) for i in x] if j])


def _run(cmd=[]):
    """Run a command with the Blueutil CLI tool, returning a json formatted response."""
    cmd += ["--paired"] if len(cmd) == 0 else []
    # NOTE[dcascio|2022-03-31] https://github.com/toy/blueutil/issues/58#issuecomment-954237721
    # Assume that the second item is always the address
    if len(cmd) > 1 and change_status(cmd):
        cmd += ["--info", cmd[1]]
    run_command = ["/usr/local/bin/blueutil", "--format", "json"] + cmd
    # print(">>> RUN_COMMAND:", run_command)
    output = subprocess.run(run_command, capture_output=True).stdout
    # print(">>> OUTPUT:", output)
    try:
        json_dict = json.loads(output)
    except Exception:
        return None
    return json_dict


# TODO[dcascio|2022-04-04] Probably swap the storage mechanism and store by id.
# Then have a helper functions to separately look through the item name to
# return the item id instead. Will require subsequent function signatures to be
# updated.

DEVICES = {d["name"]: dict(address=d["address"],connected=d["connected"]) for d in _run()}
device_name = lambda id: [i[0] for i in DEVICES.items() if i[1]["address"] == id][0]
device_id = lambda name: DEVICES[name]["address"]


def is_connected(id):
    """Get last known status by id."""
    stat = DEVICES[id]["connected"]
    # print(">>>", id, stat)
    return stat


def update_status(id, state):
    """Query and update current status of Bluetooth device by address."""
    DEVICES[device_name(id)]["connected"] = state
    return None


def valid(func):
    """Validate that a passed device identifier is an address not a name."""
    def validate_device_id(*args, **kwargs):
        identifier = args[0]
        # print(">>> VALIDATE ID:", identifier)
        id = device_id(identifier) if not ADDRESS_REGEXP.match(identifier) else identifier
        return func(id, *args[1:], **kwargs)
    return validate_device_id


@valid
def notify(dev):
    name = device_name(dev)
    title = f"{name} Status"
    status_msg = "Connected" if DEVICES[name]["connected"] else "Disconnected"

    result = f"display notification \"{status_msg}\" with title \"{title}\""
    osascript = lambda x: subprocess.run(["/usr/bin/osascript", "-e", x], capture_output=False)

    return osascript(result)


@valid
def connect(id):
    """Connect a Bluetooth device by address."""
    update_status(id, True)
    return _run(["--connect", id])


@valid
def disconnect(id):
    """Disconnect a Bluetooth device by address."""
    update_status(id, False)
    return _run(["--disconnect", id])


def toggle(dev):
    """Toggle the current connection status of a Bluetooth device by address."""
    if is_connected(dev):
        disconnect(dev)
    else:
        connect(dev)
    return notify(dev)


def list_devices():
    for device in DEVICES:
        print(device)
    exit(0)

def device_prompt():
    """Prompt the user for a Bluetooth device to toggle the connection status of."""
    pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("device", nargs="?", default="list",
                        help="The name of a previously paired Bluetooth device")
    parser.add_argument("-l", "--list", help="List known Bluetooth devices")

    args = parser.parse_args()
    # print(">>> ARGS:", args)
    if args.device is None or  args.device == "list":
        list_devices()
    if args.device in DEVICES:
        toggle(args.device)
        exit(0)
    else:
        exit(1)
