#!/bin/sh

NEWHOME=$(pwd)

check() {
    if [[ "$1" == '!' ]]; then
        if
            command -v "$2" &>/dev/null; then
            return 1
        else
            return 0
        fi
    else
        if
            command -v "$1" &>/dev/null; then
            return 0
        else
            return 1
        fi
    fi
}

if check "!" "xcode-select"; then
    echo please install xcode to proceed.
    exit 1
fi

xcode-select --install

### Homebrew utilities --------------------------------------------------------
# Check for Homebrew
if check "!" "brew"; then
    echo Installing Homebrew...
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Install shell utilities
echo installing env utilities...
brew install \
     ag \
     aspell \
     bash \
     bash-completion@2 \
     bat \
     bitwarden-cli \
     coreutils \
     cowsay \
     dbus \
     enchant \
     eza \
     fd \
     fontconfig \
     gnutls \
     go \
     htop \
     httpie \
     isync \
     libvterm \
     mactex \
     mongodb \
     homebrew/cask/mpv \
     neovim \
     node \
     openssl \
     pandoc \
     rbenv \
     reattach-to-user-namespace \
     ripgrep \
     shellcheck \
     shfmt \
     sqlite \
     starship \
     stow \
     surfraw \
     telnet \
     tldr \
     tmux \
     tree \
     w3m \
     watchexec \
     wiki \
     wordnet \
     yarn \
     yq \
     zoxide

# Install various casks
echo installing casks
brew install --cask \
     alacritty \
     betterzip \
     calibre \
     dash \
     docker \
     insomnia \
     mpv \
     quicklook-csv \
     quicklook-json \
     quicklookase \
     rectangle \
     suspicious-package

brew install koekeishiya/formulae/skhd
brew install koekeishiya/formulae/yabai

echo starting brew services
brew services start skhd
brew services start yabai

# # Install various npm packages
# echo installing npm packages
# # language servers
# npm install -g \
    #     bash-language-server \
    #     dockerfile-language-server-nodejs \
    #     javascript-typescript-langserver \
    #     typescript-language-server \
    #     yaml-language-server \
    #     yaml-lint \
    #     tree-sitter-cli \
    #     stylelint \
    #     js-beautify \
    #     prettier \
    #     eslint \
    #     indium \
    #     markdownlint

### Python setup
echo installing python tools
pip3 install \
     black \
     nose \
     pipenv \
     pyflakes \
     pytest \
     flake8 \
     isort \
     rope \
     "python-language-server[all]"

### Install Emacs! ------------------------------------------------------------
echo installing emacs
brew tap railwaycat/emacs-mac
brew install emacs-mac --with-natural-title-bar --with-emacs-big-sur-icon --with-starter --with-mac-metal
ln -Fs "$(find /usr/local -name \"Emacs.app\")" "/Applications/Emacs.app"

cd "$NEWHOME" || exit 1

### Enable emacs keybindings everywhere
cp errata/DefaultKeyBinding.dict ~/Library/KeyBindings/DefaultKeyBinding.dict
echo Copied your keybindings... please remember to restart.
