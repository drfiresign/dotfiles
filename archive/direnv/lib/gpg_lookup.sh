#!/usr/bin/sh

gpg_lookup()
{
    gpg --decrypt --quiet < "$1"
}
