#!/usr/bin/env sh

TRANSFORM_STATE=$(swaymsg -t get_outputs --raw | jq 'map(select(.active)) | .[].transform')

[[ $TRANSFORM_STATE == "90" ]] && echo true || echo false
