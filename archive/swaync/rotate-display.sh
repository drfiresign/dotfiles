#!/usr/bin/env bash

OUTPUT=$(swaymsg -t get_outputs --raw | jq 'map(select(.active)) | .[].name')

[[ "$SWAYNC_TOGGLE_STATE" == true ]] && swaymsg output $OUTPUT transform 90 || swaymsg output $OUTPUT transform normal
