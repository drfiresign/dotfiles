#!/usr/bin/env bash
# -*- mode: sh -*-

PROMPT_COMMAND=__prompt_command

# control characters
rcol="\e[0m" # reset column
under="\e[4m" # underline

# new colors
red="\e[1;38;5;161m"
lred="\e[1;38;5;203m"
# _promptc="\e[1;38;5;244m"
blue="\e[1;38;5;69m" # 33 27
lblue="\e[1;38;5;111m"
green="\e[1;38;5;108m" # 30
purple="\e[1;38;5;146m"
yellow="\e[1;38;5;221m" # 220 184 226
orange="\e[1;38;5;214m"

git-status() {
  local branch
  local status
  local prompt
  local clean_git_status

  branch=$(git symbolic-ref HEAD 2>/dev/null | awk -F/ '{print $NF}')
  status=$(git status 2>/dev/null | tail -n 1)
  clean_git_status="nothing to commit, working tree clean"
  prompt="${purple}\u${rcol} ${blue}\W${rcol} "

  if [[ -z $status ]]; then
    echo -ne "$prompt${rcol}"
    exit 0
  fi

  if [[ $status != "${clean_git_status}" ]]; then
    local branch="[${red}$branch${rcol}]"
  else
    local branch="[${green}$branch${rcol}]"
  fi

  echo -ne "$prompt$branch "
}

virtual_prompt() {
  if [[ $VIRTUAL_ENV ]]; then
    local en
    en=$(echo -ne "${VIRTUAL_ENV}" | sed 's/\/Users\/dcascio\/.virtualenvs\///g')
    echo -n ":: venv: ${lblue}${en}${rcol} "
  fi
}

__shell_check() {
  if [[ "$INSIDE_EMACS" == 'vterm' ]]; then
    echo -ne "vterm "
  else
    echo -ne "\s:\V "
  fi

}

__kube_ctx() {
  if [[ -n "$KUBE_ENABLE" ]] ; then
    echo -ne ":: ${red}$(kubens -c)${rcol} in ${under}$(kubectx -c)${rcol} "
  fi
}

__prompt_command() {
  local EXIT="$?"
  PS1=""
  PS1+="$(git-status)"
  PS1+="$(__kube_ctx)"
  PS1+="$(virtual_prompt)"
  if [[ $EXIT != 0 ]]
  then
    PS1+="${yellow}(${lred}${EXIT}${yellow})${rcol} "
  fi
  PS1+="\n"
  PS1+="\$ "
}
