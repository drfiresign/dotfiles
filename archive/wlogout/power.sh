#!/usr/bin/env bash
case "$1" in
    "logout")
        echo ":: Logging Out"
        sleep 0.25
        bash ${HOME}/bin/graceful-exit
        ;;
    "lock")
        echo ":: Locking"
        sleep 0.25
        bash ${HOME}/bin/lock
        ;;
    "reboot")
        echo ":: Rebooting"
        sleep 0.25
        systemctl reboot
        ;;
    "shutdown")
        echo ":: Shutting Down"
        sleep 0.25
        systemctl poweroff
        ;;
    "suspend")
        echo ":: Suspending"
        sleep 0.25
        systemctl suspend
        ;;
    "hibernate")
        echo ":: Hibernating"
        sleep 0.25
        systemctl hibernate
        ;;
esac
