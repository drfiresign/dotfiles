#!/usr/bin/env fish
set -l yabai_options "[-v,--version|-V,--verbose|-m,--message msg|-c,--config config_file|--uninstall-sa|--load-sa]"
set -l yabai_domains config display space window query rule signal
complete -f -c yabai -n "not __fish_seen_subcommand_from $yabai_domains" -a config -d "Get or set the value of a setting"
complete -f -c yabai -n "not __fish_seen_subcommand_from $yabai_domains" -a display -d "Display commands"
complete -f -c yabai -n "not __fish_seen_subcommand_from $yabai_domains" -a space -d "Space commands"
complete -f -c yabai -n "not __fish_seen_subcommand_from $yabai_domains" -a window -d "Window commands"
complete -f -c yabai -n "not __fish_seen_subcommand_from $yabai_domains" -a query -d "Query for information about the current window arrangement"
complete -f -c yabai -n "not __fish_seen_subcommand_from $yabai_domains" -a rule -d "Rule commands"
complete -f -c yabai -n "not __fish_seen_subcommand_from $yabai_domains" -a signal -d "Signal commands"

# complete -f -c yabai -n "" -a "" -d ""
