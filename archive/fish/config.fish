#!/usr/bin/env fish

if test -e "$HOME/.fish.local"
    source "$HOME/.fish.local"
end

if test (uname) = Linux
    function which
        argparse s/silent -- $argv
        if set -q _flag_s
            /usr/bin/which &>/dev/null $argv
        else
            /usr/bin/which $argv
        end
    end
end

if status is-interactive
    if test (uname) = Linux
        set -l kring (gnome-keyring-daemon -s 2>/dev/null)
        for var in $kring
            set (string split = -f1 $var) (string split = -f2  $var)
        end
    end

    which -s direnv; and direnv hook fish | source
    which -s kubectl; and kubectl completion fish | source
    which -s brew; and brew shellenv | source
    which -s atuin; and atuin init fish | source
    if test "$TERM" != dumb
        which -s starship; and starship init fish | source
        enable_transience
        which -s zoxide; and zoxide init fish | source
        which -s pyenv; and pyenv init - | source
    end
end

if test "$INSIDE_EMACS" = 'vterm'
    source "$HOME/.config/fish/vterm.fish"
end

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/dcascio/code/google-cloud-sdk/path.fish.inc' ]; . '/Users/dcascio/code/google-cloud-sdk/path.fish.inc'; end

# # pnpm
# set -gx PNPM_HOME "/home/dcascio/.local/share/pnpm"
# if not string match -q -- $PNPM_HOME $PATH
#   set -gx PATH "$PNPM_HOME" $PATH
# end
# # pnpm end
