#!/usr/bin/env fish

function sshp
    set -l host $argv[1]
    if test -z $host
        echo "Usage: sshp <hostname>"
        return
    end

    sshu postgres $host $argv[2..]
end
