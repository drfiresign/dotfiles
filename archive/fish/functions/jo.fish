#!/usr/bin/env fish
function jo --description "Filter out any piped content that is not JSON."
    rg --line-buffered '^\{'
end
