#!/usr/bin/env fish
function update-db --description "Run updatedb with personal preferences"
    # binary to run
    set -lx cmd updatedb

    set -lx prunepaths /tmp
    set -a prunepaths  /usr/tmp
    set -a prunepaths  /var/tmp

    if test (uname) = Darwin
        set -a prunepaths /Users/dcascio/Library
        set -a prunepaths /Users/dcascio/.Trash
        set -a prunepaths /Users
        set -a prunepaths /System
        set -a prunepaths /Library
        set -a prunepaths /afs
        set -a prunepaths /private
        set -a prunepaths /usr/sbin/authserver

        set cmd gupdatedb
    end

    sudo $cmd --add-prunenames "$prunepaths"
end
