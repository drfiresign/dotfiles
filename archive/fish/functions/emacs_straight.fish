#!/usr/bin/env fish

function emacs_straight --description "Emacs with only straight.el bootstrap."
    command emacs -Q -l ~/.straight.el/straight/repos/straight.el/bootstrap.el
end
