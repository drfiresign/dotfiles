#!/usr/bin/env fish

function clear
    vterm_printf "51;Evterm-clear-scrollback";
    tput clear;
end
