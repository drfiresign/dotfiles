#!/usr/bin/env fish
function random-line -a filename -d "print a random line from a given file"
    test -e $filename; or return
    set -l list_len (count-lines $filename)
    set -l rand (random)
    set -l index (math -s0 $rand % $list_len)

    cat -p $filename -r (math $index + 1)

end
