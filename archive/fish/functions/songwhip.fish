#!/usr/bin/env fish
function songwhip --description "Convert any streaming service music link into a Songwhip link."
    if not type -q jq
        echo 'You must have jq installed to use this.'
        return 1
    end
    if type -q https
        https songwhip.com url=$argv | jq -r .url | pbcopy
    else
        set -lx data (printf '{"url":"%s"}' $argv)
        curl -X POST https://songwhip.com/ -d $data | jq -r .url | pbcopy
    end
end
