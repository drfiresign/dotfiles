#!/usr/bin/env fish
function logl
    argparse l/log e/error w/warn d/debug -- $argv
    set level LOG
    if set -q _flag_d
        set level DEBUG
    else if set -q _flag_w
        set level WARN
    else if set -q _flag_e
        set level ERROR
    end

    if isatty stdin
        set msg "$argv"
    else
        read msg
    end
    printf "[%s:%s] %s\n" $level (gdate -Is) (string trim $msg)
end
