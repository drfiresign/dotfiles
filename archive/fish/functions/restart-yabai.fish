#!/usr/bin/env fish
function restart-yabai
    brew services stop yabai && brew services start yabai
end
