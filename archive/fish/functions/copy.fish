#!/usr/bin/env fish

function copy --description "Echo current the following command into system clipboard."
    if test (uname) != "Linux"
        echo "$argv" | pbcopy
    else
        echo "$argv" | xargs wl-copy -p --
    end
end
