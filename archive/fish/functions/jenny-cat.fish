#!/usr/bin/env fish

function jenny-cat -a side
    if test (string sub -l 1 "$side") = "r"
        echo "("(jenny)")>" (term-cat); and return 1
    else
        echo (term-cat) "<("(jenny)")"; and return 1
    end
end
