#!/usr/bin/env fish

function sshu
    set -l user $argv[1]
    if test -z $user
        echo "Usage: sshu <user> <hostname>"
        return
    end
    set -l host $argv[2]
    if test -z $host
        echo "Usage: sshu <user> <hostname>"
        return
    end
    set -l the_rest $argv[3..]

    ssh -o LogLevel=QUIET -t $host sudo -iu $user (string escape -- $the_rest)
end
