#!/usr/bin/env fish
function backspace-or-kill
    if test (count (commandline -s)) -gt 0
        commandline -f kill-selection
    else
        commandline -f backward-delete-char
    end
end
