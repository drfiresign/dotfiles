#!/usr/bin/env fish
function kill-region-or-backward-kill-path --description "If selection is active kill it, othwerwise backward-kill-path-component"
    if test (count (commandline -s)) -gt 0
        commandline -f kill-selection
    else
        commandline -f backward-kill-path-component
    end
end
