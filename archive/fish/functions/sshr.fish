#!/usr/bin/env fish

function sshr
    set -l host $argv[1]
    if test -z $host
        echo "Usage: sshr <hostname>"
        return
    end

    sshu root $host $argv[2..]
end
