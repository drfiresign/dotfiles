#!/usr/bin/env fish

function jenny-tmux
    set -l left_cat "｡˄o ｪ o˄｡ <("(jenny)")"
    set -l right_cat "("(jenny)")>" (term-cat)
    if test (count $argv) -gt 0
        echo $right_cat
    else
        echo $left_cat
    end
end
