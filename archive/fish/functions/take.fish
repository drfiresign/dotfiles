#!/usr/bin/env fish
function take
    if test (count $argv) -eq 0
        awk '{print $1}'; and return
    end
    awk (printf '{print $%s}' $argv)
end
