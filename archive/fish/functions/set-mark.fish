#!/usr/bin/env fish

function set-mark --description "Toggle the current selection mark."
    if test (count (commandline -s)) -gt 0
        echo (commandline -s)
        commandline -f begin-selection
        bind --user \cg end-selection
        bind --user \eu togglecase-selection
    else
        commandline -f end-selection
        bind --user \eu upcase-word
    end
end
