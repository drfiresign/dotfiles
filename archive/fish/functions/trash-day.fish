#!/usr/bin/env fish
function trash-day --description "Check for the on time status of garbage pick-up in Philadelphia."
    if set -q TRASH_DAY
    end

    set -lx resp (https api.phila.gov/phila/trashday/v1)
    set -lx delayed (echo $resp | jq .delay)
    set -lx msg (echo $resp | jq .status)
    set -lx all_holidays (echo $resp | jq -r .holidays[].start_date)
    set -lx next_two_weeks (for j in (seq 30); gdate -d "$j days" '+%F'; end) # (for i in (seq 14); gdate -d "$i days" '+%F'; end)
    set -lx holiday
    set -lx days_away
    set -lx holiday_date
    set -lx holiday_index

    for date in $all_holidays
        if contains $date $next_two_weeks
            set holiday_index (math (contains -i $date $all_holidays) - 1)
            set days_away (math (contains -i $date $next_two_weeks))
            set holiday (echo $resp | jq .holidays[$holiday_index].holiday_label | string unescape)
            set holiday_date (gdate -d $date '+%A, %b %d')
            break
        end
    end

    echo Today is (gdate '+%A, %b %d')
    echo (string unescape $msg)
    if test -n "$days_away"
        echo $holiday is in $days_away days on $holiday_date.
    end
end
