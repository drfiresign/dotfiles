function ldap-pass --description "LDAP Password"
    gpg --decrypt --quiet < ~/.ldap-pass.gpg
end
