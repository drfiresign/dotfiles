#!/usr/bin/env fish
function count-lines -a filename -d "just the number of lines in a given file"
    wc -l $filename | cut -f1 -d' '
end
