#!/usr/bin/env fish

function fish_greeting
    if not set -q TMUX
        jenny-cat
    end
end
