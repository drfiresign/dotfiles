#!/usr/bin/env fish

function cat --description "Use bat if available, cat if not."
    # if type -q batcat
    #     batcat $argv
    # else
    if type -q bat
        bat $argv
    else
        cat $argv
    end
end
