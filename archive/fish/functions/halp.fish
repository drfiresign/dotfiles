#!/usr/bin/env fish
function halp
    $argv --help 2>&1 | bat --plain --language=help
end
