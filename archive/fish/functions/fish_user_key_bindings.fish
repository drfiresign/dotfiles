#!/usr/bin/env fish

function fish_user_key_bindings --description "Personal key bindings for Fish."
    # erase preset edit_command_buffer to allow use of scroll-up command in an
    # emacs controlled terminal
    # bind --erase --preset \ev
    bind --erase --preset \cj
    bind --erase --preset \ep

    # Undo the base fzf bindings I don't use
    bind --user \ec capitalize-word
    bind --user \ct transpose-chars

    # Make commands match their emacs equivalent
    bind --user \e/ undo
    bind --user ^\x1F undo
    bind --user \cg cancel
    bind --user \cs 'forward-jump-search-till'
    bind --user \e\co insert-line-over
    bind --user \e\cj insert-line-under
    bind --user \cx swap-selection-start-stop
    bind --user \e\cb backward-bigword
    bind --user \e\cf forward-bigword
    bind --user \e\ck kill-bigword
    bind --user \e^\x7F backward-kill-bigword
    bind --user \co commandline\ -f\ expand-abbr\;\ commandline\ -i\ \\n
    bind --user \cj commandline\ -f\ expand-abbr\;\ commandline\ -i\ \\n
    # My custom key functions operate on text in a more emacs-y way
    # bind --user \r '__execute'
    bind --user \cw 'kill-region-or-backward-kill-path'
    # bind --user -k nul 'set-mark'
    bind --user -k backspace 'backspace-or-kill'

    bind --user \ep history-prefix-search-backward
    bind --user \en history-prefix-search-forward

end
