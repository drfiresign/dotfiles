#!/usr/bin/env fish

function alacritty-font -a size
    alacritty msg config font.size=$size -w -1
end
