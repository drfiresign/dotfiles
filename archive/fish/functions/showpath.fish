#!/usr/bin/env fish
function showpath
    # TODO[dcascio|2022-12-11] Add a verbose flag here
    set -l count_user_path (count $fish_user_paths)
    set -l count_sys_path (count $PATH)

    echo fish_user_paths:
    echo ----------------
    for i in (seq $count_user_path)
        echo (string pad -w 2 $i): $fish_user_paths[$i]
    end
    echo \nPATH:
    echo -----
    for i in (seq $count_sys_path)
        echo (string pad -w 2 $i): $PATH[$i]
    end

    # echo $PATH | tr ' ' '\n'
end
