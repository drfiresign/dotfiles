#!/usr/bin/env fish

### with thanks to Jenny Holzer -----------------------------------------------

function jenny
    random-line $HOME/code/txt-collections/jenny-holzer-truisms.txt
end
