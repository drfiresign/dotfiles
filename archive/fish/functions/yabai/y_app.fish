#!/usr/bin/env fish
function y_app -a app
    yabai -m query --windows | jq -r ".[]|select(.app==\"$app\")"
end
