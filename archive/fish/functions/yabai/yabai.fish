#!/usr/bin/env fish
set -gxa fish_function_path $HOME/.config/fish/functions/yabai/

function yabai
    command yabai $argv
    # sketchybar -m --trigger yabai_command_run >/dev/null 2>/dev/null &disown
end
