#!/usr/bin/env fish
function y_title -a title
    yabai -m query --windows | jq -r ".[]|select(.title==\"$title\")"
end
