#!/usr/bin/env fish
function y_alacritty_cycle
    set -l windows_on_space (yabai -m query --spaces --space | jq ".windows[]")
    set -l alacritty_windows_on_space
    for w in $windows_on_space
        set -a alacritty_windows_on_space (yabai -m query --windows --window $w | jq "select(.app==\"Alacritty\" and .\"has-focus\"==false)|.id")
    end
    for i in $alacritty_windows_on_space
        yabai -m window --focus $i
    end
end
