#!/usr/bin/env fish
function forward-jump-search-till --description "Search through autocompletions if active, otherwise search forward in entry."
    if commandline --paging-mode
        commandline -f pager-toggle-search
    else
        commandline -f forward-jump-till
        commandline -f forward-char
    end
end
