#!/usr/bin/env fish

function last_history_item; echo $history[1]; end
abbr -a !! --position anywhere --function last_history_item


abbr -a e --position command -- emacs
abbr -a ec --position command -- emacsclient -nw
abbr -a r --position command -- rg
# abbr -a s --position command -- structy
# abbr -a xa --position command -- xargs
