#!/usr/bin/env fish

alias .. "cd .."
alias ... "cd ../.."
alias .... "cd ../../.."

if which rg >/dev/null
    alias rgrep (which grep)
    alias grep 'rg'
end

# workflow tooling
# alias e "emacs"
# alias e "emacsclient -nw"
# alias ec "emacsclient -t"
alias tmain "tmux new -A -s main"
alias tmls "tmux list-sessions"
alias tmh "tmux has 2>/dev/null"

alias first "take"
alias second "take 2"
alias third "take 3"
alias fourth "take 4"
alias fifth "take 5"
alias timestamp "gdate -Is"

# python
# alias python python3
# alias pip pip3
# alias pe pipenv
# alias pepr "pipenv run"

# ls alterations
alias la "ls -a --color=always --group-directories-first" # list all
alias ll "ls -alhGgF --color=always --group-directories-first" # long list, but hide unneeded output

# eza: a better ls
alias ez 'eza --sort=name --git --group-directories-first'
alias el 'eza -laH'
alias ea 'eza -a'
# eza: tree-ish
alias et 'eza -T'
alias eta 'eza -Ta'
alias etal 'eza -Ta -L'

# stow aliases
alias restow "stow -R"
alias unstow "stow -D"

# various aliases
alias cenv "env | sort | rg -N '^[^=]+'"
alias calias 'alias | rg \\s\\w+\\s'
alias mkdir "mkdir -p" # create intermediate dirs
# alias rmds "find . -name .DS_Store -type f -delete"
alias rmf "rm -rf"
alias st "starship"
alias stt "starship toggle"
alias sttk "stt kubernetes"
alias tree "tree -C" # add colors
alias riff "riffdiff"
# alias dc-qmk "qmk compile -kb keyboardio/atreus -km drfiresign && qmk flash -kb keyboardio/atreus -km drfiresign"
alias sk "sk --reverse --bind=ctrl-v:page-down,alt-v:page-up"
# alias htop "btm -b"

# git things
alias gcm "git checkout master"
alias gs "git status"
alias gss "gs -s"

# various bat aliases
# if test (uname) = Linux
#     alias bat batcat
# end
alias pcat "bat --plain --color never"      # plain-cat
alias dcat "bat -d"                         # diff-cat
alias ycat "bat --language=yaml"            # yaml-cat
alias y "ycat"

if which podman >/dev/null; alias pod="podman"; alias docker="podman"; end

### radio aliases
alias xrayfm "mpv http://listen.xray.fm:8000/stream" # pdx based alt-rock
alias nprfm "mpv https://www.npr.org/streams/mp3/nprlive24.m3u" # talk talk talk
alias opbfm "mpv https://stream5.opb.org/radio_player.mp3" # oregon public broadcasting

alias ssh "TERM=xterm /usr/bin/ssh"

# linux audio things
alias restart-audio "systemctl --user restart wireplumber pipewire pipewire-pulse.service pipewire-pulse.socket"
alias wlcopy wl-copy
