#!/usr/bin/env fish
if test (status is-login) -o (status is-interactive)
    switch (uname)
        case Darwin
            for toolset in coreutils findutils libtool gsed gawk gtar
                set -l gnubin (path normalize (brew --prefix)/opt/$toolset/libexec/gnubin)
                set -l gnuman (path normalize $gnubin/../gnuman)
                if test -d $gnubin
                    fish_add_path --global $gnubin
                    # if test -d $gnuman
                    #     # set -ax MANPATH_MAP $gnubin $gnuman
                    # end
                end
            end
            # set -l manbin (path normalize /usr/local/opt/man-db/libexec/bin)
            # set -l manman (path normalize $manbin/../man)
            # if test -d $manbin
            #     fish_add_path --global $manbin
            #     if test -d $manman
            #         # set -ax MANPATH_MAP $manbin $manman
            #     end
            # end
    end
end
