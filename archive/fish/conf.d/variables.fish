#!/usr/bin/env fish

# don't look at me!
set -gx DOTNET_CLI_TELEMETRY_OPTOUT 1

# personal niceties & general path related variables
set -gx XDG_CONFIG_HOME "$HOME/.config"
set -gx XDG_DATA_HOME "$HOME/.local/share"
set -gx XDG_BIN_HOME "$HOME/.local/bin"
set -gx code "$HOME/code"

# application vars
set -gx MOZ_ENABLE_WAYLAND 1
set -gx DOTNET_NEW_PREFERRED_LANG "F#"
set -gx DOTNET_ROOT "/usr/local/share/dotnet"
set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME
set -gx GHCUP_USE_XDG_DIRS 1
set -gx GOENV_ROOT "$XDG_CONFIG_HOME/goenv"
set -gx KALEIDOSCOPE_DIR "$HOME/code/Kaleidoscope"
set -gx PIPENV_DONT_LOAD_ENV
set -gx PIPENV_DONT_USE_PYENV
set -gx PYTHON_LATEST_LOCAL "python@3.11"
set -gx RIPGREP_CONFIG_PATH "$XDG_CONFIG_HOME/ripgrep/config"
set -gx STARSHIP_CACHE "$XDG_CONFIG_HOME/starship/cache"
set -gx STARSHIP_CONFIG "$XDG_CONFIG_HOME/starship/config.toml"
set -gx VOLTA_HOME "$HOME/.volta"
set -gx PNPM_HOME "$HOME/.local/share/pnpm"
set -gx PYENV_ROOT "$HOME/.pyenv"

# shell vars
# set -gx ALTERNATE_EDITOR "/usr/local/bin/emacsclient -Q"
set -gx ALTERNATE_EDITOR ""
set -gx EDITOR "emacsclient -t"
set -gx VISUAL "emacsclient -t"
set -gx TERMINFO "/usr/share/terminfo"
set -gx MANROFFOPT "-c"
set -gx LESS "-R -F"
set -gx PAGER "bat --style=changes,rule,snip"
set -gx BAT_STYLE "changes,rule,snip"
set -gx COLORTERM truecolor

switch (uname)
    case Darwin
        # set -gx BROWSER "/Applications/Arc.app/Contents/MacOS/Arc"
        set -gx MANPAGER "sh -c 'col -bx | bat -l man -p'"
    case Linux
        # set -gx BROWSER "org.mozilla.firefox"
        set -gx MANPAGER "sh -c 'col -bx | bat -l man -p'"
end

# terminal tools
set -gx _ZO_FZF_OPTS "--extended --cycle --filepath-word"
set -gx _ZO_RESOLVE_SYMLINKS 1

set -lx fzf_opts '--cycle --layout=reverse'
set -a fzf_opts "--border --height=40%"
set -a fzf_opts "--preview-window=wrap"
set -a fzf_opts '--marker="*"'
set -a fzf_opts "--bind 'alt-,:last'"
set -a fzf_opts "--bind 'alt-.:first'"
set -a fzf_opts "--bind 'alt-n:next-history'"
set -a fzf_opts "--bind 'alt-p:previous-history'"
set -a fzf_opts "--bind 'alt-v:page-up'"
set -a fzf_opts "--bind 'ctrl-k:kill-line'"
set -a fzf_opts "--bind 'ctrl-space:toggle'"
set -a fzf_opts "--bind 'ctrl-v:page-down'"
set -a fzf_opts "--bind 'ctrl-m:execute:(qlmanage -p {})'"
set -a fzf_opts "--bind 'enter:accept'"
set -a fzf_opts "--bind 'tab:replace-query'"
set -gx FZF_DEFAULT_OPTS $fzf_opts

set fzf_preview_dir_cmd "eza --all --color=always"

## construct path
set -lx append_dirs \
    "$HOME/.cargo/bin" \
    "$HOME/bin" \
    "$XDG_BIN_HOME" \
    "/var/lib/flatpak/exports/bin/"

set -lx prepend_dirs \
    "$HOME/.cabal/bin" \
    "$VOLTA_HOME/bin" \
    "$PNPM_HOME"


for dir in $append_dirs
    if test -e $dir -a -d $dir
        fish_add_path --global --append $dir
    end
end

for dir in $prepend_dirs
    if test -e $dir -a -d $dir
        fish_add_path --global --prepend $dir
    end
end
