#!/usr/bin/env fish
set -gxa fish_function_path $HOME/.config/fish/functions/vterm/

alias ff find_file
