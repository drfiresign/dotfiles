#!/usr/bin/env bash

CLASS="--class '__dnf_update__'"
UPDATES=$(dnf --quiet check-update | tail -n +2 | tr '\n' '|')

if [[ -z $UPDATES ]]; then
    return 1
fi

dnf makecache

SELECTION=$(echo "all|$UPDATES" \
                | rofi -sep '|' -dmenu -multi-select \
                | cut -f1 -d'.' \
                | tr '\n' '|')

if [[ "$!" == 1 ]]; then
    return 1
fi

if [[ $SELECTION == "all|" ]]; then
    CLEANED=""
else
    CLEANED=$(echo $SELECTION | tr '|' ' ')
fi

alacritty $CLASS --hold -e sudo dnf upgrade $CLEANED
