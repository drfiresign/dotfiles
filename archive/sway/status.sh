# this is called by the sway configuration file in ~/.config/sway/config.
# you should see changes to the status bar after saving this script.
# if not, do "killall swaybar" and $mod+Shift+c to reload configuration.

t_comma() {
    sed s:[%,]$::g
}



uptime_formatted=$(uptime | cut -d ',' -f1 | cut -d ' ' -f4,5)
date_formatted=$(date +'  W%V: %a %m/%d/%y |  %R')
linux_version=$(uname -r | cut -d '-' -f1)

# network
ssid=$(iwgetid --raw)

# battery
battery_status=$(acpi | cut -d' ' -f3 | t_comma | tr '[:upper:]' '[:lower:]')
battery_level=$(acpi | cut -d' ' -f4 | t_comma)
batt_str=$(echo $battery_level | t_comma)
batt_num=$(($batt_str))

if [ $batt_num  <= 15 ]; then
    batt_icon=
elif [ $batt_num <= 25 ]; then
    batt_icon=
elif [ $batt_num <= 50 ]; then
    batt_icon=
elif [ $batt_num <= 75 ]; then
    batt_icon=
else
    batt_icon=
fi
battery_output="$battery_status: $batt_icon $batt_num"

echo " $uptime_formatted     $linux_version     $ssid    $battery_output    $date_formatted  "
