#!/bin/sh

for p in $(ls /home/dcascio/code/base16/templates/alacritty/colors/* | rg -v 256);
do
    basename $p | sed -e 's/base16-//g' -e 's/.yml//g'
done | wofi --dmenu | {
    read -r style
    echo $style | xargs -I_ sed -i -e 's/set $theme .*/set $theme _/' /home/dcascio/.config/sway/config
    echo $style | xargs -I_ emacsclient --eval "(load-theme 'base16-_ 't)"
    swaymsg reload
}
