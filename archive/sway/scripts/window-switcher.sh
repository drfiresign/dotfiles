#!/bin/bash
swaymsg -t get_tree \
    | jq -r '.nodes[].nodes[] | if .nodes then [recurse(.nodes[])] else [] end + .floating_nodes | .[] | select(.nodes==[]) | select(.name | contains("__i3_scratch") | not) | ((.id | tostring) + " " + .app_id + " " + .name )' \
    | wofi --dmenu --prompt "open windows" \
    | awk '{print $1}' | xargs -I{} swaymsg "[con_id={}]" focus
