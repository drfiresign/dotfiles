#!/usr/bin/bash

GRIM_DEFAULT_DIR="$(xdg-user-dir PICTURES)/Screenshots"

SWAYNC=`pidof swaync`

sway_prop() {
    jq_arg=`printf  "$1"`

}

WINDOWS=`swaymsg -t get_tree | jq -r '.. | (.nodes? // empty)[] | select(.visible and .pid) | "\(.app_id): \(.name)"'`
FOCUSED=`swaymsg -t get_tree | jq -r '.. | (.nodes? // empty)[] | select(.focused and .pid) | "\(.app_id): \(.name) \(.rect.x),\(.rect.y) \(.rect.width)x\(.rect.height)"'`

TIME="$(date -Iminutes)"
FILE="$TIME"_screenshot.png

CHOICE="$1"
if [[ -z "$CHOICE" ]]; then

    CHOICE=`rofi -dmenu <<EOF
fullscreen
region
focused
$WINDOWS
EOF`

    if [[ -z "$CHOICE" ]]; then
        exit 1
    fi
fi

if [ "$CHOICE" = fullscreen ]; then
    grim "$GRIM_DEFAULT_DIR/$FILE"
elif [ "$CHOICE" = region ]; then
    RECT="$(slurp)"
    if [[ -z "$RECT" ]]; then
        exit 1
    fi

    grim -g "$RECT" "$GRIM_DEFAULT_DIR/$FILE"
else
    if [ "$CHOICE" = focused ]; then
        echo $FOCUSED
        APP_ID=`echo -e $FOCUSED | cut -f1 -d' ' | tr -d ':'`
        RECT=`echo -e $FOCUSED | cut -f3,4 -d' '`
        echo $APP_ID DOOOOODE $RECT
    else
        APP_ID=`echo -e $CHOICE | cut -f1 -d' '| tr -d ':'`
        NAME=`echo -e $CHOICE | cut -f2 -d' '`

        jq_query=`printf '.. | (.nodes? // empty)[] | select(.visible and (.name=="%s") and (.app_id=="%s")) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' $NAME $APP_ID`
        RECT="$(swaymsg -t get_tree | jq -r "$jq_query")"
    fi

    FILE="$TIME"_"$APP_ID"_screenshot.png
    grim -g "$RECT" "$GRIM_DEFAULT_DIR/$FILE"
fi

if [[ "$SWAYNC" ]]; then
    OPEN=`notify-send "Screenshot" "Saved: $FILE \nCopied to clipboard" -i "$GRIM_DEFAULT_DIR/$FILE" -A open="Open File" -u low -w -t 3000`
    if [ "$OPEN" = open ]; then
        xdg-open "$GRIM_DEFAULT_DIR/$FILE"
    fi
fi
exit 0
