IMAGE=$(xdg-user-dir PICTURES)/Screenshots/lock.png
FONT="/usr/share/fonts/source-code-pro/SauceCodeProNerdFont-Regular.ttf"
EMPTY="00000000"

grim -o $(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name') $IMAGE

# We do this in two passes so the text is layered on *after* the blur effect
gm convert $IMAGE \
   -scale 25% \
   -blur 0x5 \
   -scale 400% \
   -fill black \
   -colorize 25% \
   $IMAGE

gm convert \
   -font $FONT \
   -fill BlanchedAlmond \
   -gravity center \
   -pointsize 36 \
   -draw 'text 0,100 "Enter Password to Unlock"' \
   $IMAGE $IMAGE

swaylock -F -f -s fill -i $IMAGE
