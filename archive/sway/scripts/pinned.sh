#!/usr/bin/env bash
PINNED=$(swaymsg -t get_tree | jq -r  '.nodes[].nodes[]  |  if  .nodes  then  [recurse(.nodes[])]  else [] end + .floating_nodes | .[] | select(.nodes==[]) | select(.marks==["📌"]) | .id')
FOCUSED=$(swaymsg -t get_tree | jq -r  '.nodes[].nodes[]  |  if  .nodes  then  [recurse(.nodes[])]  else [] end + .floating_nodes | .[] | select(.nodes==[]) | select(.focused) | .id')

if [[ -z $PINNED || -z $FOCUSED ]]; then
    return
elif [[ $PINNED -eq $FOCUSED ]];then
    return
fi
