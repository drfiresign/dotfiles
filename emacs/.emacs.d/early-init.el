;;; early-init.el --- early initialization file -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Early initialization is performed here. This is where we are going increase
;; the garbage collection threshold, and load straight.el.

;;; Code:

(setq gc-cons-threshold 100000000
      package-enable-at-startup nil)

;; (setenv "PATH" (getenv "PATH"))

;; get some info if something goes wrong
(defun toggle-init-debug ()
  (toggle-debug-on-quit)
  (toggle-debug-on-error))
(toggle-init-debug)
(add-hook 'after-init-hook #'toggle-init-debug)

(if (and (fboundp 'native-comp-available-p)
         (native-comp-available-p))
    (setq-default
     native-comp-deferred-compilation t
     native-comp-deferred-compilation-deny-list nil
     comp-async-report-warnings-errors nil
     native-comp-async-report-warnings-errors 'silent))

;; Do not resize the frame at this early stage.
(setq frame-inhibit-implied-resize t
      frame-resize-pixelwise t)


;; Disable GUI elements
;; No titlebar
;; (add-to-list 'default-frame-alist '(undecorated . t))
;; No titlebar with round corners
(add-to-list 'default-frame-alist '(undecorated-round . t))
(set-window-scroll-bars (minibuffer-window) 0 0)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(defun dc/compare-frame-fn (new-elem old-elem)
  "Only compare the car of NEW-ELEM and OLD-ELEM."
  (equal (car new-elem)
         (car old-elem)))

;; (let* ((font-name '"monospase")
;;        (font-string (concat font-name "-" (number-to-string 14))))
;;   (add-to-list 'default-frame-alist `(font . ,font-string) nil 'dc/compare-frame-fn)
;;   (set-face-attribute 'variable-pitch nil
;;                       :font font-name
;;                       :height 1.0
;;                       ))

(setq inhibit-splash-screen t
      use-dialog-box t   ; only for mouse events
      use-file-dialog nil
      user-full-name "Dylan Cascio"
      user-mail-address "dc@dcascio.com"
      inhibit-splash-screen t
      inhibit-startup-echo-area-message t ; read the docstring
      inhibit-startup-screen t
      inhibit-startup-buffer-menu t
      echo-keystrokes 1e-6 ; display keystrokes in the echo area immediately.
      custom-file (locate-user-emacs-file "custom.el"))
(load custom-file :no-error-if-file-is-missing)

;;; package management -- straight.el
(setq straight-repository-branch "develop"
      straight-base-dir "~/.straight.el")

;; check for watchexec and python3 to speed up loading of straight.el.
(if (and (executable-find "watchexec")
         (executable-find "python3"))
    (setq straight-check-for-modifications '(watch-files find-when-checking))
  (setq straight-check-for-modifications '(find-at-startup find-when-checking)))

;; bootstrap straight.el from source if it isn't installed.
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "~/.straight.el/straight/repos/straight.el/bootstrap.el"))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;; use-package

(declare-function straight-use-package "straight.el" ())
(straight-use-package 'use-package)

;; follow `use-package' syntax unless specified using the `:straight' keyword.
(setq straight-use-package-by-default t)

;; tell `use-package' to always load features lazily. if `:demand' is present,
;; the loading is eager; otherwise, the loading is lazy. See
;; https://github.com/jwiegley/use-package#notes-about-lazy-loading.
(setq-default use-package-always-defer nil
              straight-vc-git-default-protocol 'ssh) ; without github added to ssh known hosts, change this to https
;; lifted with thanks from <https://github.com/raxod502/radian>
(defmacro use-feature (name &rest args)
  "Like `use-package NAME &rest ARGS', but with `straight-use-package-by-default' disabled."
  (declare (indent defun))
  `(use-package ,name
     :straight nil
     ,@args))

(provide 'early-init)

;;; early-init.el ends here
