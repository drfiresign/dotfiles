;;; simple-init.el --- Fallback init file -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;; Version: version
;; Package-Requires: (dependencies)
;; Homepage: homepage
;; Keywords: keywords


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; commentary

;;; Code:

(add-to-list 'load-path "~/.straight.el/repos/" t)
(add-to-list 'load-path "~/.straight.el/repos/org/contrib/lisp/" t)
(add-to-list 'load-path "~/.emacs.d/modules/" t)
(add-to-list 'load-path "~/.emacs.d/private/" t)

(defvar dc/sys-mac (eq system-type 'darwin))
(defvar dc/sys-bsd (eq system-type 'berkley-unix))
(defvar dc/sys-linux  (eq system-type 'gnu/linux))

(require 'dc-straight-utilities)
(require 'dc-config-defaults)

(use-package s :demand t)
(use-package f :demand t)
(use-package dash :demand t)

(require 'dc-init)
(require 'dc-keybindings)
(require 'dc-core)
(require 'dc-functions)
(require 'dc-windows)
(require 'dc-editor)
(require 'dc-search)
(require 'dc-programming)
(require 'dc-projects)
(require 'dc-movec)
(require 'work-stuff)

(when dc/sys-mac (require 'dc-mac))
(when dc/sys-bsd (require 'dc-freebsd))
(when dc/sys-linux (require 'dc-linux))

(provide 'simple-init)

;;; simple-init.el ends here
