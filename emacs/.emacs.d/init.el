;;; Turntable --- A revolving door of package configuration for Emacs -*- lexical-binding: t; -*-
;; Copyright (c) 2019 Dylan Cascio
;;
;; Author: Dylan Cascio <dc@drfiresign.com>
;; URL: drfiresign.com
;; Version: 2.0

;; This file is not part of GNU Emacs.

;;; Commentary:

;; This file is for setting my personal options in requires the use of
;; a package manager. Currently, that package manager is straight.el.
;; I have received inspiration from numerous sources across the internet.
;; Especially: Harry R. Schwartz (hrs) and Ross Donaldson (Gastov).

;; `early-init.el' gets loaded before anything else, this contains what I need
;; to run this config with straight.el.

;;; Code:

;; -------------------- load path -------------------------
(add-to-list 'load-path "~/.emacs.d/modules/" t)
(add-to-list 'load-path "~/.emacs.d/templates/" t)
(add-to-list 'load-path "~/.emacs.d/private/" t)
(add-to-list 'load-path "~/.straight.el/repos/" t)
(add-to-list 'load-path "~/.straight.el/repos/org/contrib/lisp/" t)

;; debug loadtime
;; (use-package benchmark-init
;;   :ensure t
;;   :init (benchmark-init/activate)
;;   :config
;;   ;; To disable collection of benchmark data after init is done.
;;   (add-hook 'after-init-hook 'benchmark-init/deactivate))

;; ----------------- config & utils ----------------------
(require 'dc-straight-utilities)

;; module dependencies
(use-package s :demand t)
(use-package f :demand t)
(use-package dash :demand t :config (dash-enable-font-lock))

;; ephemeral preferences
(require 'dc-config-defaults)

(require 'dc-functions)
;; ------------------- os config ------------------------
(defvar dc/sys-mac (when (eq system-type 'darwin)
                     (progn (require 'dc-mac) t)))
(defvar dc/sys-linux (when (eq system-type 'gnu/linux)
                       (progn (require 'dc-linux) t)))
(defvar dc/sys-bsd (when (eq system-type 'berkley-unix)
		                 (progn (require 'dc-freebsd) t)))
(defvar dc/exwm (when (and (getenv "EXWM") dc/sys-bsd)
		              (progn (autoload 'exwm-enable "dc-exwm.el") t)))

;; -------------------- modules -------------------------
(require 'dc-init)
(require 'dc-keybindings)
(require 'dc-core)
(require 'dc-windows)
(require 'dc-avy)
(require 'dc-editor)
(require 'dc-search)
(require 'dc-dired)
(require 'dc-projects)
(require 'dc-snippets)
(require 'dc-movec)
(require 'dc-notes)

;; languages
(require 'dc-programming)
(require 'dc-clojure)
(require 'dc-go)
(require 'dc-haskell)
(require 'dc-javascript)
(require 'dc-lisp)
(require 'dc-lua)
(require 'dc-magit)
(require 'dc-perl)
(require 'dc-python)
(require 'dc-rust)
(require 'dc-scala)
(require 'dc-fsharp)
;; (require 'dc-smartparens)
(require 'dc-parens)
(require 'dc-web)
(require 'dc-internet)
(require 'dc-containers)
;; (require 'dc-kubernetes)
(require 'dc-terminal)
(require 'dc-fish)
(require 'dc-misc)
(require 'dc-org)
(require 'dc-txt)
(require 'dc-fonts)
(require 'dc-themes)
(require 'dc-modeline)
(require 'dc-fun)
(require 'dc-ui)
(require 'dc-dashboard)
;; (unless (or dc/term dc/exwm)
;;   (require 'dc-dashboard))
;; (require 'dc-mail)
;; (require 'dc-eshell)
(require 'dc-security)
(require 'dc-personal-packages)
(if (f-directory-p "~/.emacs.d/private")
    (require 'work-stuff))

;; ------------------------ cleanup -----------------------------
(require 'dc-cleanup)

;; vroom vroom
(unless (server-running-p)
  (server-start))

(provide 'init)
;;; init.el ends here
