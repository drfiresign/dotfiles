;;; dc-ivy.el --- ivy / swiper / etc. -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Command and search completion provided by Ivy / Swiper.

;;; Code:

(use-package ivy
  :init
  (ivy-mode 1)
  :config
  (setq ivy-use-virtual-buffers t
        enable-recursive-minibuffers t
        ivy-wrap t
        ivy-count-format "(%d/%d) "
        ivy-height 20)
  (push '(completion-at-point . ivy--regex-ignore-order) ivy-re-builders-alist)
  (push '(swiper . ivy--regex-ignore-order) ivy-re-builders-alist)
  (push '(counsel-M-x . ivy--regex-ignore-order) ivy-re-builders-alist)
  (use-package flx :init (setq ivy-flx-limit 100000))
  (defun ivy-yank-action (x)
    (kill-new x))
  (ivy-set-actions
   t
   '(("y" ivy-yank-action "yank")))

  :bind (("M-o" . swiper)
         ("M-s i" . swiper-isearch)
         ("M-s r" . swiper-isearch-backward)
         ("M-s M-s" . swiper-thing-at-point)
         ("C-c M-s M-s" . swiper-all-thing-at-point)
         ("C-." . ivy-imenu-anywhere)
         ("C-c C-r" . ivy-resume)
         ;; ("C-c V" . ivy-pop-view)
         ;; ("C-c v" . ivy-push-view)
         ("C-x C-f" . counsel-find-file)
         ;; ("C-x f" . counsel-recentf) ;; projectile-recentf "s-p e" is better
         ("C-x b" . ivy-switch-buffer)
         ("M-x" . counsel-M-x)
         ("M-y" . counsel-yank-pop)
         ("H-r" . counsel-register)
         (:map ivy-minibuffer-map
               ("TAB" . ivy-alt-done)
               ("C-l" . ivy-alt-done)
               ("RET" . ivy-alt-done))
         (:map ivy-switch-buffer-map
               ("C-l" . ivy-done)
               ("C-d" . ivy-switch-buffer-kill))
         (:map ivy-reverse-i-search-map
               ("C-k" . ivy-previous-line)
               ("C-d" . ivy-reverse-i-search-kill))
         (:map help-map
               ("f" . counsel-describe-function)
               ("v" . counsel-describe-variable)
               ("o" . counsel-describe-symbol)
               ("l" . counsel-find-library)
               ("i" . counsel-info-lookup-symbol)
               ("u" . counsel-unicode-char))))

;; (use-package ivy-posframe
;;   :config
;;   (setq ivy-posframe-display-functions-alist
;;         '((complete-symbol . ivy-posframe-display-at-point)
;;           (t               . ivy-display-function-fallback))
;;         ;; ivy-posframe-parameters '((left-fringe . 4)
;;         ;;                           (right-fringe . 4))
;;         ;; ivy-posframe-width 120
;;         ;; ivy-posframe-border-width 4
;;         )
;;   (add-hook 'ivy-mode-hook 'ivy-posframe-mode))
;; (ivy-posframe-mode 1)

(use-package counsel
  :init
  (counsel-mode)
  :config
  (setq counsel-describe-function-function #'helpful-callable
        counsel-describe-variable-function #'helpful-variable)

  (use-package smex) ; Adds M-x recent command sorting for counsel-M-x
  (use-package counsel-projectile
    :demand t
    :bind
    ("H-s" . counsel-projectile-rg)
    :init (counsel-projectile-mode))

  :custom
  (counsel-rg-base-command (append dc/rg-command (list "%s")))
  :bind
  (("C-c g" . counsel-git)
   ("C-c j" . counsel-git-grep)
   ("C-c C-s" . counsel-rg)
   ("C-x l" . counsel-locate)))

(unless dc/term
  (use-package all-the-icons-ivy-rich
    :init
    (all-the-icons-ivy-rich-mode 1)))

(use-package ivy-rich
  :config
  (setq ivy-rich-path-style 'abbrev)
  (setq ivy-set-display-transformer
        '(counsel-projectile-switch-to-buffer . ivy-switch-buffer-rich-transformer))
  :init
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line)
  (ivy-rich-mode 1))

;; (use-package mini-frame
;;   :straight (:host github :repo "muffinmad/emacs-mini-frame"))
;; (mini-frame-mode +1)

(use-package ivy-prescient
  :after counsel
  :custom
  (ivy-prescient-enable-filtering nil)
  :config
  ;; Uncomment the following line to have sorting remembered across sessions!
  (prescient-persist-mode 1)
  (ivy-prescient-mode 1))

(provide 'dc-ivy)

;;; dc-ivy.el ends here
