;;; dc-misc.el --- miscellaneous programming modes -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

(use-package i3wm-config-mode :straight (:host github :repo "Alexander-Miller/i3wm-Config-Mode"))
(use-package hyprlang-ts-mode :straight (:host github :repo "Nathan-Melaku/hyprlang-ts-mode")
  :config
  (add-to-list 'treesit-language-source-alist
               '(hyprlang "https://github.com/tree-sitter-grammars/tree-sitter-hyprlang")))
(use-package apache-mode) ; major mode for .htaccess and similar files
(use-package ansible)
(use-package bbcode-mode :straight (:host github :repo "ejmr/bbcode-mode"))
(use-package crontab-mode)
(use-package csv-mode)
;; (use-package dockerfile-mode
;;   :custom (dockerfile-enable-auto-indent nil))
(use-package haproxy-mode)
(use-package symbol-overlay :straight (:host github :repo "wolray/symbol-overlay")
  :bind
  ("M-i" . symbol-overlay-put)
  ("M-n" . symbol-overlay-jump-next)
  ("M-p" . symbol-overlay-jump-prev)

  (:map symbol-overlay-map
        ("<escape>" . symbol-overlay-remove-all)
        ("M-n" . symbol-overlay-switch-forward)
        ("M-p" . symbol-overlay-switch-backward)
        ))
(use-package casual :straight (:host github :repo "kickingvegas/casual"))
(use-package casual-suite :straight (:host github :repo "kickingvegas/casual-suite")
  :ensure t
  :bind
  ("C-c a o" . casual-avy-tmenu)
  ;; ("C-o" . casual-editkit-main-tmenu)
  (:map calc-mode-map ("C-o" . casual-calc-tmenu))
  (:map dired-mode-map ("C-o" . casual-dired-tmenu))
  (:map isearch-mode-map ("C-o" . casual-isearch-tmenu))
  (:map ibuffer-mode-map ("C-o" . casual-ibuffer-tmenu))
  (:map ibuffer-mode-map ("F" . casual-ibuffer-filter-tmenu))
  (:map ibuffer-mode-map ("s" . casual-ibuffer-sortby-tmenu))
  (:map Info-mode-map ("C-o" . casual-info-tmenu))
  (:map reb-mode-map ("C-o" . casual-re-builder-tmenu))
  (:map reb-lisp-mode-map ("C-o" . casual-re-builder-tmenu))
  (:map bookmark-bmenu-mode-map ("C-o" . casual-bookmarks-tmenu))
  (:map org-agenda-mode-map ("C-o" . casual-agenda-tmenu))
  (:map symbol-overlay-map ("C-o" . casual-symbol-overlay-tmenu))
  )
(use-package jinja2-mode
  :straight (:host github :repo "paradoxxxzero/jinja2-mode"))
(use-package json-mode)
(use-package jsonnet-mode :straight (:host github :repo "tminor/jsonnet-mode"))
(use-package jq-mode :straight (:host github :repo "ljos/jq-mode")
  :commands (jq-mode)
  :mode (("\\.jq$" . jq-mode))
  :bind (:map jq-mode-map
              ("C-c C-j" . jq-interactively)))
(use-package just-mode)
(use-package justl
  :straight (:host github :repo "psibi/justl.el")
  :custom
  (justl-executable "/usr/local/bin/just")
  ;; (justl-recipe-width 25)
  )
(use-package nix-mode
  :mode "\\.nix\\'")
(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :custom (markdown-command "pandoc")
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . gfm-mode)
         ("\\.markdown\\'" . markdown-mode))
  ;; :hook (markdown-mode . visual-line-mode)
  )
(use-package protobuf-mode
  :straight (:host github :repo "protocolbuffers/protobuf" :files ("editors/protobuf-mode.el") :build (:not compile)))
(use-package sdlang-mode
  :mode (("\\.kdl\\'" . sdlang-mode)))
(use-package sqlformat :straight (:host github :repo "purcell/sqlformat")
  :custom
  (sqlformat-command 'pgformatter)
  (sqlformat-args '("-s2" "-g"))
  :bind (:map sql-mode-map
              ("C-c C-f" . sqlformat))
  :hook (sql-mode . sqlformat-on-save-mode))
(use-package terraform-mode
  :mode "\\.tf$")
(use-package yaml-mode
  :mode (("vars\\'" . yaml-mode))
  :hook (yaml-mode . (lambda ()
                       (when (and (boundp 'projectile-project-root)
                                  (s-match "ansible/$" (projectile-project-root)))
                         (ansible 1))
                       (highlight-symbol-nav-mode 1))))
(use-package yaml :straight (:host github :repo "zkry/yaml.el"))
(use-package yaml-pro :straight (:host github :repo "zkry/yaml-pro")
  :hook (yaml-mode . yaml-pro-mode))
(use-package pip-requirements)
(use-package ssh-config-mode)
(use-package vimrc-mode :straight (:host github :repo "mcandre/vimrc-mode")
  :mode ("\\.vim\\(rc\\)?\\'" . vimrc-mode))
(use-package pdf-tools
  :config (pdf-tools-install)
  :custom ((pdf-view-display-size 'fit-page)
           (pdf-annot-activate-created-annotations t))
  :bind (:map pdf-view-mode ("C-s" . isearch-forward)))
(use-package hcl-mode)

(use-package pocket-reader :straight (:host github :repo "alphapapa/pocket-reader.el"))

(provide 'dc-misc)

;;; dc-misc.el ends here
