;;; dc-exwm.el --- exwm configuration -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Contains exwm config. this currently features configuration from the example
;; in the exwm repo.

;;; Code:

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(defun efs/run-in-background (command)
  (let ((command-parts (split-string command "[ ]+")))
    (apply #'call-process `(,(car command-parts) nil 0 nil ,@(cdr command-parts)))))

(defun efs/set-wallpaper ()
  "Set wallpaper."
  (interactive)
  (start-process-shell-command "feh" nil "feh --bg-scale /usr/share/wallpapers/2004_misty-vineethkartha.jpg"))

(defun dc/exwm-init-hook ()
  "Initialize EXWM."

  (multi-vterm-dedicated-toggle)
  (display-battery-mode 1)
  (display-time-mode 1)
  (setq display-time-default-load-average nil
        display-time-day-and-date t)
  ;; Launch apps that will run in the background
  (efs/run-in-background "nm-applet")
  (efs/run-in-background "pasystray")
  (efs/run-in-background "blueman-applet")
  (efs/run-in-background "pavucontrol")
  ;; Set frame transparency
  (set-frame-parameter (selected-frame) 'alpha '(90 . 90))
  (add-to-list 'default-frame-alist '(alpha . (90 . 90)))
  (set-frame-parameter (selected-frame) 'fullscreen 'maximized)
  (add-to-list 'default-frame-alist '(fullscreen . maximized))
  )



;; main packages
(use-package xelb
  :demand t
  :straight (:host github :repo "ch11ng/xelb"))
(use-package exwm
  :demand t
  :custom
  (mouse-autoselect-window nil)
  (focus-follows-mouse t)
  (exwm-workspace-warp-cursor t)
  (exwm-workspace-number 4)
  (exwm-systemtray-height 32)
  (exwm-randr-workspace-monitor-plist '(0 "eDP-1"))
  (exwm-input-global-keys
   `(
     ;; Bind "s-r" to exit char-mode and fullscreen mode.
     ([?\s-r] . exwm-reset)
     ;; Move between windows
     ([s-left] . windmove-left)
     ([s-right] . windmove-right)
     ([s-up] . windmove-up)
     ([s-down] . windmove-down)

     ;; Bind "s-&" to launch applications ('M-&' also works if the output
     ;; buffer does not bother you).
     ([?\s-&] . (lambda (command) (interactive (list (read-shell-command "$ ")))
		              (start-process-shell-command command nil command)))
     ;; Bind "s-w" to switch workspace interactively.
     ([?\s-w] . exwm-workspace-switch)
     ;; Bind "s-0" to "s-9" to switch to a workspace by its index.
     ,@(mapcar (lambda (i) `(,(kbd (format "s-%d" i)) . (lambda () (interactive) (exwm-workspace-switch-create ,i)))) (number-sequence 0 9))
     ;; Bind "s-<f2>" to "slock", a simple X display locker.
     ([s-f2] . (lambda () (interactive) (start-process "" nil "/usr/bin/slock")))))
  (exwm-input-prefix-keys
   '(?\C-x
     ?\C-h
     ?\M-x
     ?\M-`
     ?\M-&
     ?\M-:
     ?\C-\M-j  ;; Buffer list
     ?\C-\M-k  ;; Browser list
     ?\C-\     ;; Ctrl+Space
     ?\C-\;))
  (exwm-input-simulation-keys
   '(([?\C-b] . [left]) ; movement
     ([?\M-b] . [C-left])
     ([?\C-f] . [right])
     ([?\M-f] . [C-right])
     ([?\C-p] . [up])
     ([?\C-n] . [down])
     ([?\C-a] . [home])
     ([?\C-e] . [end])
     ([?\M-v] . [prior])
     ([?\C-v] . [next])
     ([?\C-d] . [delete])
     ([?\C-k] . [S-end delete])
     ([?\C-w] . [?\C-x]) ; cut/paste
     ([?\M-w] . [?\C-c])
     ([?\C-y] . [?\C-v])
     ([?\C-s] . [?\C-f]))) ; search
  :bind
  (:map exwm-mode-map ("C-q" . exwm-input-send-next-key))
  :config
  (defun dc/exwm-update-buffer-name () (exwm-workspace-rename-buffer exwm-class-name))
  (require 'exwm-systemtray)
  (require 'exwm-randr)

  (exwm-systemtray-enable)
  (exwm-randr-enable)
  (efs/set-wallpaper)
  (define-key)
  (exwm-input-set-key (kbd "<s-return>") 'multi-vterm)
  (exwm-input-set-key (kbd "s-f") 'exwm-layout-toggle-fullscreen)
  (start-process-shell-command "xmodmap" nil "xmodmap $HOME/.Xmodmap")
  (start-process-shell-command "xrdb" nil "xrdb $HOME/.Xresources")
  (start-process-shell-command "toggle-touchpad" "*Messages*" "toggle-touchpad off")
  (start-process-shell-command "set key rate" "*Messages*" "xset r rate 300 40")
  (exwm-enable)
  :hook
  (exwm-update-class-hook . dc/exwm-update-buffer-name)
  (exwm-floating-setup-hook . (lambda () (exwm-layout-hide-mode-line))))

;; Make workspace 1 be the one where we land at startup
(exwm-workspace-switch-create 1)

(use-package desktop-environment
  :after exwm
  :config (desktop-environment-mode)
  :custom
  (desktop-environment-brightness-small-increment "2%+")
  (desktop-environment-brightness-small-decrement "2%-")
  (desktop-environment-brightness-normal-increment "5%+")
  (desktop-environment-brightness-normal-decrement "5%-"))

(when dc/helm
  (use-package emacs-helm
    :straight (:host github :repo "emacs-helm/helm-exwm")))

;; TODO: create a key combo to send back and forward and tab keys when viewing
;; buffers named Firefox.


(provide 'dc-exwm)

;;; dc-exwm.el ends here
