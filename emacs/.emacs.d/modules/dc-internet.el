;;; dc-internet.el --- traversing the web tree -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;; Tramp stuff I guess?

(use-feature tramp
  :config
  (setenv "SHELL" "/bin/bash")
  (tramp-set-completion-function "ssh" '((tramp-parse-sconfig "/etc/ssh/ssh_config")
                                         (tramp-parse-sconfig "~/.ssh/config"))))

(provide 'dc-internet)

;;; dc-internet.el ends here
