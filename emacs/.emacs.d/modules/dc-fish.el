;;; dc-fish.el --- fish shell customizations -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

(use-package fish-mode
  :straight (:host github :repo "wwwjfy/emacs-fish"))
(use-package fish-completion
  :straight (:host gitlab :repo "ambrevar/emacs-fish-completion")
  :config
  (when (and (executable-find "fish")
             (require 'fish-completion nil t))
    (global-fish-completion-mode)))

(provide 'dc-fish)

;;; dc-fish.el ends here
