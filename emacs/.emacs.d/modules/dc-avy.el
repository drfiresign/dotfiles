;;; dc-avy.el --- Avy configuration -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

(use-package avy :straight (:host github :repo "abo-abo/avy")
  :defer t
  :custom
  (avy-keys '( ?a ?s ?d ?f ?j ?l ?\;
               ?c ?v ?, ?/
               ?q ?e ?r ?u ?o ?p
               ))
  (avy-style 'de-bruijn)
  (avy-timeout-seconds 0.3)
  :bind
  ("C-'" . avy-goto-char-timer)
  ("C-c a r" . avy-resume)
  ("C-c a a" . avy-goto-line)
  ("C-c a s" . avy-goto-symbol-1)
  ("C-c a e" . avy-goto-end-of-line)
  ("C-c a f" . avy-goto-char-in-line)
  ("C-c a w" . avy-goto-word-0)
  ("M-g l" . avy-goto-line)
  ;; lines
  ("C-c a c l" . avy-comment-line)
  ("C-c a k l" . avy-kill-whole-line)
  ("C-c a y l" . avy-copy-line)
  ;; regions
  ("C-c a c r" . avy-comment-region)
  ("C-c a k r" . avy-kill-region)
  ("C-c a y r" . avy-copy-region)
  (:map isearch-mode-map
        ("M-SPC" . avy-isearch)
        ("C-a" . avy-isearch))

  :config ; via: https://karthinks.com/software/avy-can-do-anything
  (defun avy-show-dispatch-help ()
    (let* ((len (length "avy-action-"))
           (fw (frame-width))
           (raw-strings (mapcar
                         (lambda (x)
                           (format "%2s: %-19s"
                                   (propertize
                                    (char-to-string (car x))
                                    'face 'aw-key-face)
                                   (substring (symbol-name (cdr x)) len)))
                         avy-dispatch-alist))
           (max-len (1+ (apply #'max (mapcar #'length raw-strings))))
           (strings-len (length raw-strings))
           (per-row (floor fw max-len))
           display-strings)
      (cl-loop for string in raw-strings
               for N from 1 to strings-len do
               (push (concat string " ") display-strings)
               (when (= (mod N per-row) 0) (push "\n" display-strings)))
      (message "%s" (apply #'concat (nreverse display-strings)))))
  ;; Kill text
  (defun avy-action-kill-whole-line (pt)
    (save-excursion
      (goto-char pt)
      (kill-whole-line))
    (select-window
     (cdr
      (ring-ref avy-ring 0)))
    t)

  ;; Comment region
  (defun avy-action-comment-whole-line (pt)
    (save-excursion
      (goto-char pt)
      (let ((beg (line-beginning-position))
            (end (line-end-position)))
        (comment-or-uncomment-region beg end)))
    (select-window
     (cdr
      (ring-ref avy-ring 0)))
    t)

  ;; Copy text
  (defun avy-action-copy-whole-line (pt)
    (save-excursion
      (goto-char pt)
      (cl-destructuring-bind (start . end)
          (bounds-of-thing-at-point 'line)
        (copy-region-as-kill start end)))
    (select-window
     (cdr
      (ring-ref avy-ring 0)))
    t)

  ;; Yank text
  (defun avy-action-yank-whole-line (pt)
    (avy-action-copy-whole-line pt)
    (save-excursion (yank))
    t)

  ;; Transpose/Move text
  (defun avy-action-teleport-whole-line (pt)
    (avy-action-kill-whole-line pt)
    (save-excursion (yank)) t)

  ;; Mark text
  (defun avy-action-mark-to-char (pt)
    (activate-mark)
    (goto-char pt))

  (defun avy-action-embark (pt)
    (save-excursion
      (goto-char pt)
      (embark-act))
    (select-window
     (cdr (ring-ref avy-ring 0)))
    t)

  (defun dictionary-search-dwim (&optional arg)
    "Search for definition of word at point. If region is active,
search for contents of region instead. If called with a prefix
argument, query for word to search."
    (interactive "P")
    (if arg
        (dictionary-search nil)
      (if (use-region-p)
          (dictionary-search (buffer-substring-no-properties
                              (region-beginning)
                              (region-end)))
        (if (thing-at-point 'word)
            (dictionary-lookup-definition)
          (dictionary-search-dwim '(4))))))

  (defun avy-action-define (pt)
    (save-excursion
      (goto-char pt)
      (dictionary-search-dwim))
    (select-window
     (cdr (ring-ref avy-ring 0)))
    t)

  (setf
   (alist-get ?w avy-dispatch-alist) 'avy-action-copy
   (alist-get ?W avy-dispatch-alist) 'avy-action-copy-whole-line
   (alist-get ?# avy-dispatch-alist) 'avy-action-comment-whole-line
   (alist-get ?. avy-dispatch-alist) 'avy-action-embark
   (alist-get ?: avy-dispatch-alist) 'avy-action-ispell
   ;; (alist-get ?k avy-dispatch-alist) 'avy-action-kill-move
   (alist-get ?k avy-dispatch-alist) 'avy-action-kill-stay
   (alist-get ?K avy-dispatch-alist) 'avy-action-kill-whole-line
   (alist-get ?m avy-dispatch-alist) 'avy-action-mark
   (alist-get ?  avy-dispatch-alist) 'avy-action-mark-to-char
   (alist-get ?t avy-dispatch-alist) 'avy-action-teleport
   (alist-get ?T avy-dispatch-alist) 'avy-action-teleport-whole-line
   (alist-get ?y avy-dispatch-alist) 'avy-action-yank
   (alist-get ?Y avy-dispatch-alist) 'avy-action-yank-whole-line
   (alist-get ?z avy-dispatch-alist) 'avy-action-zap-to-char
   (alist-get ?= avy-dispatch-alist) 'dictionary-search-dwim)


  (defun avy-comment-region (arg)
    "Select two lines and comment or uncomment the region between them.

The window scope is determined by `avy-all-windows' or
`avy-all-windows-alt' when ARG is non-nil."
    (interactive "P")
    (let ((initial-window (selected-window)))
      (avy-with avy-kill-region
        (let* ((beg (save-selected-window
                      (list (avy--line arg) (selected-window))))
               (end (list (avy--line arg) (selected-window))))
          (cond
           ((not (numberp (car beg)))
            (user-error "Fail to select the beginning of region"))
           ((not (numberp (car end)))
            (user-error "Fail to select the end of region"))
           ;; Restrict operation to same window. It's better if it can be
           ;; different windows but same buffer; however, then the cloned
           ;; buffers with different narrowed regions might cause problem.
           ((not (equal (cdr beg) (cdr end)))
            (user-error "Selected points are not in the same window"))
           ((< (car beg) (car end))
            (save-excursion
              (comment-or-uncomment-region
               (car beg)
               (progn (goto-char (car end)) (forward-visible-line 1) (point)))))
           (t
            (save-excursion
              (comment-or-uncomment-region
               (progn (goto-char (car beg)) (forward-visible-line 1) (point))
               (car end)))))))
      (select-window initial-window)))
  (defun avy-comment-line (arg)
    "Select line and comment or uncomment the whole line.

With a numerical prefix ARG, comment or uncomment ARG line(s)
starting from the selected line. If ARG is negative, comment or
uncomment backward.

If ARG is zero, comment or uncomment the selected line but
exclude the trailing newline.

\\[universal-argument] 3 \\[avy-comment-uncomment-line]
un/comment three lines starting from the selected line.

\\[universal-argument] -3 \\[avy-comment-uncomment-line]
un/comment three lines backward including the selected line."
    (interactive "P")
    (let ((initial-window (selected-window)))
      (avy-with avy-kill-whole-line
        (let* ((start (avy--line)))
          (if (not (numberp start))
              (user-error "Fail to select the line to comment or uncomment")
            (save-excursion (goto-char start)
                            (comment-line arg)))))
      (select-window initial-window)))
  )

;; NOTE[dcascio|2024-07-08] Consider using this in the future.
;; Probably requires going back to generic Avy commands?
(use-package casual-avy
  :disabled t
  :ensure t
  :bind ("M-g" . casual-avy-tmenu))

(provide 'dc-avy)

;;; dc-avy.el ends here
