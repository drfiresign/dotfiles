;;; dc-clojure.el --- parentheses -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;; clojure

;; (use-package clojure-mode
;;   :demand t
;;   :hook
;;   (clojure-mode-hook
;;    .
;;    (lambda () (setq inferior-lisp-program "lein repl")
;;      (font-lock-add-keywords nil '(("(\\(facts?\\)" (1 font-lock-keyword-face)) ("(\\(background?\\)" (1 font-lock-keyword-face))))
;;      (define-clojure-indent (fact 1))
;;      (define-clojure-indent (facts 1))
;;      (rainbow-delimiters-mode))))
;; (use-package clojure-mode-extra-font-locking
;;   :demand t)
;; (use-package clj-refactor)
;; (use-package cider
;;   :demand t
;;   :requires (clojure-mode clojure-mode-extra-font-locking)
;;   :custom
;;   (cider-repl-pop-to-buffer-on-connect t)
;;   (cider-show-error-buffer t)
;;   (cider-auto-select-error-buffer t)
;;   (cider-repl-history-file "~/.emacs.d/cider-history")
;;   (cider-repl-wrap-history t)
;;   :init
;;   (add-to-list 'auto-mode-alist '("\\.edn$" . clojure-mode))
;;   (add-to-list 'auto-mode-alist '("\\.boot$" . clojure-mode))
;;   (add-to-list 'auto-mode-alist '("\\.cljs.*$" . clojure-mode))
;;   (add-to-list 'auto-mode-alist '("lein-env" . enh-ruby-mode))
;;   :blackout t)

(provide 'dc-clojure)
;;; dc-clojure.el ends here
