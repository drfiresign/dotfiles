;;; dc-freebsd.el --- freebsd specifics -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;;

;;; Code:

(setq magit-git-executable "/usr/local/bin/git")

(provide 'dc-freebsd)
;;; dc-freebsd.el ends here
