;;; selenium-server.el --- Manage an instance of selenium-server -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;; Version: 0.1
;; Package-Requires: (selenium-server)
;; Homepage: n/a
;; Keywords: testing automation selenium selenium-server qa quality assurance quality-assurance


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; This package helps manage an instance of selenium-server running on your
;; system. Currently it only supports a preinstalled instance of selenium-server
;; on macOS.

;;; Resources:

;; - https://spin.atomicobject.com/2016/05/27/write-emacs-package/
;; - https://masteringemacs.org/article/comint-writing-command-interpreter

;;; TODO:

;; - [ ] check for selenium-server
;; - [ ] check for os-type
;; - [ ] offer to install selenium-server based on os-type
;; - [ ] autoload dc/selenium-server-start
;; - [ ] define selenium-server major mode
;; - [ ] define selenium-server key map for major mode
;; - [ ] determine what keycommands will do
;; - [ ] generate regular expressions to navigate the debug log
;; - [ ] publish!

;;; Code:

(defun dc/selenium-p ()
  "Return selenium-server buffer if it is already running, nil if not."
  (let* ((buffer (get-buffer "*selenium-server*")))
    (if buffer
        buffer
      nil)))

(defun dc/selenium-run (&optional background)
  "Run the selenium server, in the background if BACKGROUND."
  (let* ((server (make-comint-in-buffer "selenium-server" nil "selenium-server" nil "-debug")))
    (with-current-buffer server
      (make-local-variable 'buffer-read-only)
      (setq buffer-read-only t))
    (unless background
      (switch-to-buffer-other-window server))))

(defun dc/selenium-restart ()
  "Restart the running selenium-server."
  (interactive)
  (if (dc/selenium-p)
      (dc/selenium-server-stop t))
  (dc/selenium-run))

(defun dc/selenium-server-start ()
  "Start up selenium-server in a read-only buffer."
  (interactive)
  (let* ((instance (dc/selenium-p)))
    (if instance
        (when (y-or-n-p "*selenium-server* is already running. Do you want to restart it? ")
          (dc/selenium-restart))
      (if (equal current-prefix-arg nil) ; not called with C-u
          (dc/selenium-run)
        (dc/selenium-run t))))) ; run in background

(defun dc/selenium-server-stop (&optional kill)
  "Stop (forcefully if KILL) selenium-server if running."
  (interactive)
  (if (dc/selenium-p)
      (let* ((buffer (get-buffer (dc/selenium-p)))
             (process (get-buffer-process buffer))
             (window (get-buffer-window buffer)))
        (when kill
          (set-process-query-on-exit-flag process nil))
        (when (and (kill-buffer buffer) window)
          (delete-window window))
        )))

(provide 'selenium-server)

;;; selenium-server.el ends here
