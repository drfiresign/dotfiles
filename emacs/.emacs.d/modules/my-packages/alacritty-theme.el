;;; alacritty-theme.el --- Modify Alacritty Theme -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;; Version: 0.1
;; Package-Requires: ((modus-themes) (emacs 28.0))
;; Homepage: n/a
;; Keywords: theme, alacritty, modus


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; commentary

;;; Code:

(require 'doom-themes)

(-let*)

(json-encode
 `( :colors
    ( :primary ,fg-bg-plist
      :normal ,basic-plist
      :bright ,basic-plist
      :dim ,basic-plist )
    :line_indicator ,fg-bg-plist
    :selection (:text :background )
    :search ( :matches ,fg-bg-plist
              :focused_match ,fg-bg-plist
              :bar ,fg-bg-plist)
    :hints ( :start ,fg-bg-plist
             :end ,fg-bg-plist)))

(defvar alacritty-json-blob
  (-let* (((black red green yellow blue magenta cyan white)
           (mapcar #'face-foreground vterm-color-palette))
          (basic-plist `( :black ,black :red ,red :green ,green :yellow ,yellow
                          :blue ,blue :magenta ,magenta :cyan ,cyan :white ,white))
          (fg-bg-plist '( :foreground "#000000" :background "#ffffff")))
    )
  "JSON String to send off for processing")


(-let* (((black red green yellow blue magenta cyan white)
         (mapcar #'face-foreground vterm-color-palette))
        ((fg bg) '("#000000" "#ffffff"))
        (primary "colors.primary")
        (ansi-color-list ("%sblack=")))
  (format "colors.primary= ")

  `( :colors
     ( :primary ,fg-bg-plist
       :normal ,basic-plist
       :bright ,basic-plist
       :dim ,basic-plist )
     :line_indicator ,fg-bg-plist
     :selection (:text :background )
     :search ( :matches ,fg-bg-plist
               :focused_match ,fg-bg-plist
               :bar ,fg-bg-plist)
     :hints ( :start ,fg-bg-plist
              :end ,fg-bg-plist)))

(provide 'alacritty-theme)

;;; alacritty-theme.el ends here
