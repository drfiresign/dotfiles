;;; term-tab.el --- tabbed terminals -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;; Version: 0.1
;; Package-Requires: (vterm multi-vterm)
;; Homepage:
;; Keywords: keywords


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; commentary

;;; Code:

(defun new-term-tab ()
  ;; switch to last-term-tab,
  ;; create if it doesn't exist,
  ;; with C-u open new term-tab,
  ;; set tab as last-term-tab.
  )

(provide 'term-tab)

;;; term-tab.el ends here
