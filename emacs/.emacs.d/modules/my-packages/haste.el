;;; haste.el --- Send buffers and regions to a haste-server -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;; Version: 0.1
;; Package-Requires: (dependencies)
;; Homepage: homepage
;; Keywords: keywords

;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; The goal here is to create a few functions that can communicate with a
;; haste-server. Users will be able to send regions of text to a specific
;; instance of haste and will get back a url to share. Am I overcomplicating this? It would substantially easier to just


;; testing haste

;;; Code:

(require 'json)

(defconst haste/server-address '"https://paste.squarespace.net")

(defun haste/send-region (beg end &optional region)
  "Send the currently selected region to haste-server"
  (interactive)
  (let ((str (if region
                 (funcall region-extract-function nil)
               ;; (filter-buffer-substring beg end)
               ))))
  ;; This is copied from kill-ring-save.
  (if (called-interactively-p 'interactive)
      (indicate-copied-region))
  )

(provide 'haste)

;;; haste.el ends here
