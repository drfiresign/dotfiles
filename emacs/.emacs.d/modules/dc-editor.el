;;; dc-editor.el --- general text editing / manipulation fuctions -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; This module covers things like text / cursor movement. Anything you might do
;; to manipulate text inside a buffer.

;;; Code:

(use-package pulsar
  :straight (:host github :repo "protesilaos/pulsar")
  :bind
  ("H-'" . pulsar-pulse-line)
  ("H-\"" . pulsar-highlight-dwim)
  :custom
  (pulsar-face 'pulsar-red)
  (pulsar-iterations 100)
  (pulsar-delay 0.01))

(use-package subword
  :config (global-subword-mode 1))

(use-package unfill
  :straight (:host github :repo "purcell/unfill")
  :bind
  ([remap fill-paragraph] . unfill-toggle)
  ("C-M-S-q" . unfill-paragraph))

(use-package iedit
  :bind
  ("C-M-:" . iedit-mode))                           ; multi symbol editing in a buffer

(use-package move-text
  :bind
  ("C-M-S-p" . move-text-up)
  ("C-M-S-n" . move-text-down))

(use-package zop-to-char)

(use-package aggressive-indent
  :init
  (global-aggressive-indent-mode 1)
  :blackout t)
(add-to-list 'aggressive-indent-excluded-modes '(org-mode fsharp-mode python-mode)) ; add exclude modes here

(use-package shrink-whitespace
  :straight (:host gitlab :repo "jcpetkovich/shrink-whitespace.el")
  :config
  (defun dc/shrink-whitespace (arg)
    (interactive "P")
    (pcase (car arg)
      (4 (dotimes (i 4)
           (shrink-whitespace nil)))
      (16 (shrink-whitespace 1))
      (_ (shrink-whitespace nil))))
  :bind (:map ctl-x-map
              ("C-o" . dc/shrink-whitespace)))

(use-package multiple-cursors
  :bind
  ("C->" . 'mc/mark-next-like-this)
  ("C-c C->" . 'mc/skip-to-next-like-this)
  ("C-<" . 'mc/mark-previous-like-this)
  ("C-c C-<" . 'mc/skip-to-previous-like-this))

(setq-default
 x-underline-at-descent-line t
 flycheck-indication-mode 'left-fringe
 indicate-unused-lines 'right
 indicate-buffer-boundaries 'right
 whitespace-line-column 80
 whitespace-style
 '(face
   trailing
   tabs
   lines-char
   newline
   empty
   indentation
   ;; big-indent
   space-after-tab
   space-before-tab
   tab-mark
   ))
(global-whitespace-mode +1)

(provide 'dc-editor)

;;; dc-editor.el ends here
