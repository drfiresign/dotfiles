;;; dc-themes.el --- theme customization (ui & modeline) -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;; We override this with consult-theme which is loaded first
;; (define-key help-map (kbd "t") 'load-theme)

(use-package standard-themes :straight (:host github :repo "protesilaos/standard-themes"))

;; modus is built into Emacs 28, but we always want the latest version.
(use-package modus-themes
  :straight (:host gitlab :repo "protesilaos/modus-themes" :branch "main")
  ;; :disabled t
  :custom
  (modus-themes-bold-constructs t)
  (modus-themes-org-blocks 'tinted-background)
  (modus-themes-completions '((matches . (intense background))
                              (selection . (intense accented))
                              (popup . (intense accented))))
  (modus-themes-region '(accented bg-only))
  (modus-themes-fringes '(subtle))
  (modus-themes-hl-line '(accented))
  (modus-themes-links '(neutral))
  (modus-themes-mode-line nil)
  (modus-themes-mixed-fonts t)
  (modus-themes-variable-pitch-ui t)
  (modus-themes-paren-match '(bold))
  (modus-themes-subtle-line-numbers t)
  (modus-themes-syntax '(;; faint
                         yellow-coments
                         green-strings
                         alt-syntax
                         ))
  (modus-themes-lang-checkers '(straight-underline background))
  (modus-themes-region '(accent bg-only))
  :config
  (add-to-list 'org-src-block-faces '("fsharp" modus-themes-nuanced-red) t))

(use-package ef-themes :straight (:host github :repo "protesilaos/ef-themes")
  :custom
  (ef-themes-variable-pitch-ui t)
  (ef-themes-mixed-fonts t))

(use-package doom-themes)
(use-package kanagawa-themes :straight (:host github :repo "Fabiokleis/kanagawa-emacs")
  :ensure t)

(defvar dc/themes
  '(("Standard" standard-dark standard-light)
    ("Built-ins"  tango wombat)

    ("Kanagawa" kanagawa-dragon kanagawa-wave kanagawa-lotus)
    ("Modus" modus-vivendi modus-operandi)

    ("ef: Bio, Cyprus, Cherie, Frost" ef-bio ef-cyprus ef-cherie ef-frost)
    ("ef: Deuteranopia" ef-deuteranopia-dark ef-deuteranopia-light)
    ("ef: Dream, Reverie" ef-dream ef-reverie)
    ("ef: Duo" ef-duo-dark ef-duo-light)
    ("ef: Elea" ef-elea-dark ef-elea-light)
    ("ef: Light and Dark" ef-dark ef-light)
    ("ef: Maris" ef-maris-dark ef-maris-light)
    ("ef: Melissa" ef-melissa-dark ef-melissa-light)
    ("ef: Night and Day" ef-night ef-day)
    ("ef: Rosa, Arbutus" ef-rosa ef-arbutus)
    ("ef: Seasons" ef-spring ef-summer ef-winter ef-autumn)
    ("ef: Transformation" ef-symbiosis ef-kassio)
    ("ef: Trio" ef-trio-dark ef-trio-light)
    ("ef: Tritanopia" ef-tritanopia-dark ef-tritanopia-light)
    ("ef: Owl" ef-owl ef-eagle)

    ;; doom-themes
    ;; ("Arcario" doom-acario-dark doom-acario-light)
    ;; ("Ayu" doom-ayu-mirage doom-ayu-light)
    ;; ("Badger" doom-badger doom-xcode)
    ("Doom Dark+Miramare" doom-miramare doom-dark+)
    ;; ("Doom One" doom-one doom-one-light)
    ;; ("Dracula" doom-dracula)
    ;; ("Espresso w/ Flatwhite" doom-sourcerer doom-flatwhite)
    ("Gruvbox" doom-gruvbox doom-gruvbox-light)
    ;; ("Homage" doom-homage-black doom-homage-white)
    ("Material" doom-material-dark doom-material)
    ;; ("Monokai Machine/Spectrum/Ristretto" doom-monokai-spectrum doom-monokai-ristretto doom-monokai-machine)
    ("Monokai Pro" doom-monokai-pro doom-monokai-classic)
    ("Nord" doom-nord doom-nord-light)
    ;; ("Octagon/Challenger"  doom-monokai-octagon doom-challenger-deep)
    ;; ("Opera" doom-opera doom-opera-light)
    ("Plain" doom-plain-dark doom-plain)
    ;; ("Purple" doom-shades-of-purple doom-fairy-floss)
    ("Solarized" doom-solarized-dark-high-contrast doom-solarized-light)
    ("Tomorrow" doom-tomorrow-night doom-tomorrow-day)
    ("Zenburn" doom-zenburn doom-rouge doom-meltbus))
  "Dark and Light themed pairs in the form: (STRING-NAME DARK-THEME LIGHT-THEME).
User can also create longer chains of themes to cycle
through: (STRING-NAME THEME-ONE THEME-TWO THEME-THREE ETC...).")

(defcustom dc/theme-fallback 'tango
  "Fallback theme to use if selected theme is not present.

The recommended option is to set one of the built-in themes. This
should be a symbol and not a string.")

(defun dc/apply-theme ()
  "Apply the current theme and set cursor color."
  (interactive)
  (load-theme dc/start-theme t))

(defun dc/load-theme-advice (oldfun &rest args)
  "Always no confirm load themes."
  (let ((theme (car-safe args)))
    (setq dc/start-theme theme)
    (mapc #'disable-theme custom-enabled-themes)
    (apply oldfun (list theme 'noconfirm))
    ;; custom frame properties that need to be reloaded
    (set-cursor-color dc/cursor-color)))

(advice-add 'load-theme :around #'dc/load-theme-advice)

(defun dc/search-set (item set)
  (cl-loop for i in set
           if (member item i)
           return i))

(defun dc/find-theme-set (name)
  (or (dc/search-set name dc/themes)
      (dc/find-theme-set "Red Hat Mono")))

(defun dc/set-theme (theme-list)
  "Create the theme toggle for given THEME-LIST."
  (load-theme (or (cadr (member (car-safe custom-enabled-themes) theme-list))
                  (cadr theme-list))
              t))

(defun dc/theme-toggle-variant (&optional arg)
  "Toggle current theme-set dark/light or select a new one if ARG."
  (interactive "P")
  (let ((set (dc/find-theme-set dc/start-theme)))
    (pcase (car arg)
      (4 (dc/set-theme (reverse set)))
      (_ (dc/set-theme set)))))

(defun dc/theme-switch ()
  "Change current theme-set."
  (interactive)
  (let* ((current-theme-set (dc/find-theme-set dc/start-theme))
         (current-theme-string (car-safe current-theme-set))
         (theme-strings (mapcar #'car-safe dc/themes))
         (new-theme (completing-read "New theme: "
                                     theme-strings
                                     nil nil nil nil
                                     current-theme-string))
         (new-theme-set (dc/find-theme-set new-theme)))
    (setq dc/start-theme (cadr new-theme-set))
    (dc/theme-toggle-variant)))

(global-set-key (kbd "<f6>") 'dc/theme-toggle-variant)
(global-set-key (kbd "C-<f6>") 'dc/theme-switch)

(provide 'dc-themes)
;;; dc-themes.el ends here
