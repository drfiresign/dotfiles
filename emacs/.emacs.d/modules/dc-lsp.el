;;; dc-lsp.el --- lsp-mode
;;
;;; Commentary:
;; This isn't currently in much use, but is here for archival purposes.
;;; Code:

(use-package lsp-mode
  :straight (:host github :repo "emacs-lsp/lsp-mode")
  :defer t
  :commands (lsp lsp-deferred)
  :custom
  ;; (lsp-ansible-ansible-path )
  (lsp-auto-guess-root t)
  (lsp-completion-provider :none)
  (lsp-disabled-clients '(mspyls))
  (lsp-eldoc-render-all t)
  (lsp-enable-symbol-highlighting nil)
  (lsp-go-hover-kind "SynopsisDocumentation")
  (lsp-go-links-in-hover t)
  (lsp-headerline-breadcrumb-icons-enable nil)
  (lsp-keep-workspace-alive nil)
  (lsp-keymap-prefix "C-x l")
  (lsp-lens-enable t)
  (lsp-prefer-flymake nil)
  (lsp-pylsp-plugins-flake8-ignore '["D10" "E501"])
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-signature-auto-activate nil)
  :config
  (lsp-mode)
  ;; (lsp-register-client )
  :hook
  ((conf-toml-mode
    f-sharp
    go-mode
    go-ts-mode
    haskell-mode
    haskell-literate-mode
    js-mode-hook
    json-mode
    python-mode
    rust-mode
    rustic-mode
    rust-ts-mode
    scala-mode
    sh-mode
    xml-mode
    ansible) . lsp-deferred)
  :bind
  (:map lsp-browser-mode-map
        ("n" . widget-forward)
        ("p" . widget-backward)
        ("TAB" . widget-button-press)))

(use-package lsp-ui :blackout t
  :defer t
  :straight (:host github :repo "emacs-lsp/lsp-ui")
  :hook
  (go-mode . lsp-ui-mode)
  :custom
  (lsp-ui-sideline-enable nil)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-sideline-show-diagnostics t)
  (lsp-ui-sideline-show-code-actions t)
  (lsp-ui-sideline-code-actions-prefix ">>> ")
  (lsp-ui-sideline-show-code-actions nil)
  (lsp-ui-peek-enable t)
  (lsp-ui-doc-enable nil)
  (lsp-ui-doc-position 'top)
  (lsp-ui-doc-show-with-mouse t)
  (lsp-ui-doc-frame-mode nil)
  (lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  :bind
  (:map lsp-ui-mode-map
        ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
        ([remap xref-find-references] . lsp-ui-peek-find-references))
  ;; (:map lsp-ui-doc-mode-map
  ;;       ("M-SPC" . lsp-ui-doc-focus-frame))
  ;; (:map lsp-ui-doc-frame-mode-map
  ;;       ("M-SPC" . lsp-ui-doc-unfocus-frame))
  (:map lsp-ui-imenu-mode-map
        ;; Use bindings that are closer to occur-mode...
        ("n" . next-line)
        ("p" . previous-line)
        ("o" . lsp-ui-imenu--view)
        ("g" . lsp-ui-imenu--refresh)
        ("<return>" . lsp-ui-imenu--visit)))

;; (use-package lsp-pyright
;;   :ensure t
;;   :hook (python-mode . (lambda ()
;;                          (require 'lsp-pyright)
;;                          (lsp-deferred))))
                                        ; or lsp-deferred

;; (use-package lsp-metals)


(provide 'dc-lsp)
;;; dc-lsp.el ends here
