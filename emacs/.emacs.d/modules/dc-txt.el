;;; dc-txt.el --- truisms -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;; Version: 0.1
;; Homepage: n/a
;; Keywords: n/a

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; jenny holzer truisms, iain m. banks ship minds, & brian eno oblique strategies
;;
;; Depends on the txt-collections repo with files of lines I like.

;;; Code:

(require 's nil t)
(defvar-local txt-collections (concat dc/user-home "code/txt-collections/txt.sh"))

(defun dc/txt-collections (collection arg)
  "Call shuf on a txt COLLECTION.
One ARG will print to buffer. Two ARGs will hyphenate it for use as a name."
  (let ((line (--dc/txt-collections collection)))
    (pcase arg
      (16 (insert (downcase (s-replace " " "-" line))))
      (4 (insert line))
      (_ line))))

(defun --dc/txt-collections (collection)
  "Raw command for getting the string output of a COLLECTION."
  (s-trim (shell-command-to-string (format "%s %s" txt-collections collection))))

(defun dc/jenny (&optional arg)
  "Jenny Holzer truisms. ARG will print to buffer."
  (interactive "p")
  (dc/txt-collections "jenny" arg))
(defun dc/ships (&optional arg)
  "Iain M. Banks Culture Ships Mind names. ARG will print to buffer."
  (interactive "p")
  (dc/txt-collections "ships" arg))
(defun dc/oblique (&optional arg)
  "Brian Eno oblique strategies. ARG will print to buffer."
  (interactive "p")
  (dc/txt-collections "oblique" arg))

(provide 'dc-txt)

;;; dc-txt.el ends here
