;;; dc-dashboard.el --- dashboard -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;; dashboard init
;; TODO[dcascio|2024-07-08] REPLACE DASHBOARD WITH THIS https://github.com/ichernyshovvv/enlight/

(use-package page-break-lines
  :blackout t
  :demand t
  :config
  (global-page-break-lines-mode t))

(use-package dashboard
  :demand t
  :custom
  ;; (tab-bar-new-tab-choice (lambda () (if -boardp -boardp (get-buffer "*scratch*"))))
  (initial-buffer-choice (lambda () (let ((d-b (get-buffer "*dashboard*")))
                                 (if (bufferp d-b) d-b
                                   (get-buffer "*scratch*")))))
  (dashboard-center-content t)
  (dashboard-banner-logo-title (dc/wrap-quotes (dc/jenny)))
  (dashboard-startup-banner  "~/.emacs.d/meow_coffee.png") ; "~/.emacs.d/party_skull.png"
  (dashboard-set-init-info t)
  (dashboard-set-navigator t)
  (dashboard-set-heading-icons t)
  (dashboard-set-file-icons t)
  (dashboard-items '(;; (agenda . 5)
                     (recents   . 10)
                     (projects  . 5)
                     (bookmarks . 5)))
  (dashboard-footer-messages '("\"Somebody needs to go back and get a shitload of dimes!\""))
  :config
  ;; (advice-add 'dashboard-refresh-buffer
  ;;             :before #'(lambda (&rest args) (setq dashboard-banner-logo-title (dc/jenny))))
  )

(dashboard-setup-startup-hook)

(provide 'dc-dashboard)

;;; dc-dashboard.el ends here
