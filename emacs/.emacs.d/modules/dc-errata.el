;;; errata.el --- random stuff 'n things -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; random things and whatnot

;;; Code:

(use-package xkcd
  :defer t
  :init
  (progn
    (setq xkcd-cache-dir (concat user-emacs-directory ".xkcd/"))
    (when (not (file-directory-p xkcd-cache-dir))
      (make-directory xkcd-cache-dir)))
  :bind (:map xkcd-mode-map
              ("n" . xkcd-next)
              ("p" . xkcd-prev)))

(provide 'errata)

;;; errata.el ends here
