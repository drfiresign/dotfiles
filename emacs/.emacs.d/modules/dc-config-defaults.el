;;; dc-config-defaults.el --- config variables -*- lexical-binding: t -*-
;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; this file controls some of the variable settings that I like to
;; change regularly.

;;; Code:

(require 's)

(defcustom dc/completion-system 'movec
  "Completion system for Emacs.
Possible values are `ivy', `helm', `ido', and `movec'."
  :type 'string)

(defcustom dc/smartparens nil
  "Non-nil to use `smartparens' for list navigation.
Otherwise use default `puni'."
  :type 'boolean)

;; theming

(defcustom color-theme-enable t         ; credit to raxod502/radian
  "Non-nil means to load the default color theme.
Set this to nil if you wish to load a different color theme in
your local configuration."
  :type 'boolean)

(defcustom dc/start-theme
  ;; 'doom-badger
  ;; 'doom-gruvbox
  ;; 'doom-tomorrow-night
  ;; 'ef-dream
  'kanagawa-dragon
  ;; 'modus-vivendi
  ;; 'standard-dark
  "Starting theme for Emacs. Use nil for Emacs default.
For list of themes, see variable `dc/themes'."
  :type 'string)

(defcustom dc/which-key nil
  "Non-nil to use `which-key' for key discovery."
  :type 'boolean)

(defcustom dc/doom-modeline t
  "Non-nil to use `doom-modeline' style modeline."
  :type 'boolean)

(defcustom dc/cursor-color '"indian red"
  "Cursor color for Emacs."
  :type 'string)

(defcustom dc/transparency-value 95
  "Default transparency value."
  :type 'integer)

(defcustom dc/transparency nil
  "Non-nil to enable frame transparency."
  :type 'boolean)

;; fonts
(defvar-local dc/-base-font
    ;; "BlexMono Nerd Font"
    "CodeNewRoman Nerd Font Mono"
  ;; "FiraCode Nerd Font"
  ;; "GohuFont 11 Nerd Font"
  ;; "GeistMono Nerd Font Mono"
  ;; "Input Mono"
  ;; "Iosevka Comfy"
  ;; "InconsolataLGC Nerd Font"
  ;; "InconsolataGo Nerd Font"
  ;; "JetBrainsMono Nerd Font"
  ;; "Liga SFMono Nerd Font"
  ;; "MonaspiceNe Nerd Font"
  ;; "Monoisome"
  ;; "Overpass Nerd Font"
  ;; "Recursive Mono Linear Static"
  ;; "RobotoMono Nerd Font"
  ;; "SauceCodePro Nerd Fontn"
  ;; "SpaceMono Nerd Font Mono"
  ;; "Terminess Nerd Font Mono"
  ;; "UbuntuMono Nerd Font Mono"
  ;; "VictorMono Nerd Font"
  "Base font for Emacs.")

(defcustom dc/default-variable-pitch-font
  ;; "BlexMono Nerd Font Propo"
  "CodeNewRoman Nerd Font Propo"
  ;; "FiraCode Nerd Font Propo Retina"
  ;; "GohuFont 11 Nerd Font"
  ;; "GeistMono Nerd Font Propo"
  ;; "Iosevka Comfy Duo"
  ;; "JetBrainsMono Nerd Font Propo"
  ;; "MonaspiceNe Nerd Font Propo"
  ;; "InconsolataGo Nerd Font Propo"
  ;; "InconsolataLGC Nerd Font Propo"
  ;; "Recursive Mono Linear Static"
  ;; "RobotoMono Nerd Font Propo"
  ;; "SauceCodePro Nerd Font Propo"
  ;; "SpaceMono Nerd Font Propo"
  ;; "Terminess Nerd Font Propo"
  "Starting font for modeline and other variable pitch faces."
  :type 'string)

(defcustom dc/default-font dc/-base-font
  "Starting font for Emacs."
  :type 'string)

(defcustom dc/default-font-size 10
  "Starting font size for Emacs."
  :type 'integer)

(defcustom dc/nerd-fonts t
  "Non-nil to use Nerd Fonts."
  :type 'boolean)

(defcustom dc/default-term 'vterm
  "Terminal mode to use in Emacs.
Must be from options: `ansi-term', `shell', `eshell', and
`vterm'. Currently, only `vterm' has been implememted."
  :type 'symbol)

;; org-mode
(defcustom dc/org-work-file "~/org/sre.org"
  "Handy variable for quick access via function calls.")

(defcustom dc/organization-task-id "32134E86-8210-4741-99A2-1CE507EBCCDB"
  "The org-id for the generic ORGANIZATION default clock task.")

(defcustom dc/user-home
  (let* ((home-dir (if (eq system-type 'darwin) "/Users/" "/home/")))
    (file-name-as-directory (concat home-dir (user-login-name))))
  "User $HOME directory.")

(defcustom dc/rg-path (executable-find "rg")
  "Location of Ripgrep binary.")

(defcustom dc/rg-ignore-glob
  '("--glob=!.git" "--glob=!vendor" "--glob=!TAGS")
  "List of file globs which `rg' should ignore.")

(defcustom dc/rg-display-pref
  '("--null" "--line-buffered" "--color=never" "--max-columns=1000"
    "--path-separator /" "--smart-case" "--hidden" "--no-heading"
    "--line-number")
  "List of `rg' arguments which generally cover output display.")

(defcustom dc/rg-args-list
  (nconc (list dc/rg-path)
         dc/rg-display-pref
         dc/rg-ignore-glob)
  "Combined list of all preferred `rg' arguments.")

(defcustom dc/rg-args-string (s-join " " dc/rg-args-list)
  "String containing all preferred `rg' arguments.")

(defcustom dc/locate-path (executable-find "locate")
  "Location of `man' binary.")

(defcustom dc/locate-args-list `(,dc/locate-path "--ignore-case" "--regex")
  "String to use for any Locate searches.")

(defcustom dc/locate-args-string (s-join " " dc/locate-args-list)
  "String containing all preferred `locate' arguments.")

(defcustom dc/man-path (executable-find "man")
  "Location of `man' binary.")

(defcustom dc/man-args-list `(,dc/man-path "-k")
  "Combined list of all preferred `man' arguments.")

(defcustom dc/man-args-string (s-join " " dc/man-args-list)
  "String containing all perferred `man' arguments.")

(provide 'dc-config-defaults)

;;; dc-config-defaults.el ends here
