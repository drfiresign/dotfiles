;;; dc-linux.el --- linux config -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;;; Commentary:

;; commentary

;;; Code:
(defvar dc/user-home
  (file-name-as-directory (concat "/home/" (user-login-name)))
  "Linux User $HOME directory.")

(setq-default ispell-program-name "/usr/bin/enchant-2"
              ispell-local-dictionary "en_US"
              ispell-local-dictionary-alist '(("en_US" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "en_US") nil utf-8)))

(provide 'dc-linux)

;;; dc-linux.el ends here
