;;; dc-perl.el --- Perl -*- lexical-binding: t -*-

;; Author: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;;; cperl-mode is preferred to perl-mode
(defalias 'perl-mode 'cperl-mode)

(use-package perl-doc :straight (:host github :repo "HaraldJoerg/emacs-perl-doc"))

(provide 'dc-perl)

;;; dc-perl.el ends here
