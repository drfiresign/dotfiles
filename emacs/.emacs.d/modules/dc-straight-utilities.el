;;; dc-straight-utilities.el --- utility functions for straight package management -*- lexical-binding: t; -*-
;;
;;; Commentary:
;;
;;; Code:

(straight-use-package '(blackout
  :type git :host github :repo "raxod502/blackout"
  :demand t
  :config
  (dolist (modes '(eldoc-mode))
    (blackout modes))))

;; Package `el-patch' provides a way to override the definition of an
;; internal function from another package by providing an s-expression
;; based diff which can later be validated to ensure that the upstream
;; definition has not changed.
(straight-use-package '(el-patch
  :host github :repo "raxod502/el-patch"
  :demand t))

;;; prevent Emacs-provided Org from being loaded

;; doing this now means that if any packages that are installed
;; depend on Org, they will not accidentally cause the Emacs-provided
;; (outdated and duplicated) version of Org to be loaded.
(straight-use-package 'org)
(straight-use-package 'eglot)

;;; utilities

(defmacro radian-defhook (name arglist hook docstring &rest body) ; from raxod502/radian
  "Define a function called NAME and add it to a hook.
ARGLIST is as in `defun'.  HOOK is the hook to which to add the
function.  DOCSTRING and BODY are as in `defun'."
  (declare (indent 2)
           (doc-string 4))
  (unless (string-match-p "-hook$" (symbol-name hook))
    (error "Symbol `%S' is not a hook" hook))
  (unless (stringp docstring)
    (error "Radian-defhook: no docstring provided"))
  `(progn
     (defun ,name ,arglist
       ,(format "%s\n\nThis function is for use in `%S'."
                docstring hook)
       ,@body)
     (add-hook ',hook ',name)))

(provide 'dc-straight-utilities)
;;; dc-straight-utilities.el ends here
