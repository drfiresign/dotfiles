;;; dc-movec.el --- summary -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;;; Code:

(use-package marginalia
  :ensure t
  :init
  (marginalia-mode)
  :custom
  ;; NOTE[dcascio|2024-03-13] This is the MAX field width, but marginalia is smart enough to
  ;; truncate when window is smaller. 350 is the max width (minus 2) of an emacs window on my
  ;; ultrawide monitor at a 14 pt font-size (unless the font itself is smaller than normal).
  (marginalia-field-width 350)
  (marginalia-align 'right)
  :bind
  (:map minibuffer-mode-map
        ("M-m" . marginalia-cycle)))

(use-package vertico
  :ensure t
  :straight (:files ("*" (:exclude ".git") "extensions/*"))
  :init
  (vertico-mode 1)
  ;; (vertico-multiform-mode 1)
  ;; (vertico-flat-mode -1)
  ;; (vertico-unobtrusive-mode 1)
  :custom
  (vertico-resize nil) ; Grow and shrink the Vertico minibuffer
  (vertico-cycle t)
  (vertico-count 15)
  (vertico-multiform-categories '((consult-outline buffer)))
  (vertico-multiform-commands '((consult-imenu indexed)
                                ("\\`execute-extended-command" unobtrusive
                                 (vertico-flat-annotate . t)
                                 (marginalia-annotator-registry (command marginalia-annotate-binding)))))
  (completion-in-region-function
   (lambda (&rest args)
     (apply (if vertico-mode
                #'consult-completion-in-region
              #'completion--in-region)
            args)))
  :bind
  (:map vertico-map
        ;; ("C-i" . minibuffer-completion-help)
        ("?" . minibuffer-completion-help)
        ("M-RET" . minibuffer-force-complete-and-exit)
        ("M-TAB" . minibuffer-complete)
        ;; ("M-V" . vertico-multiform-vertical)
        ;; ("M-G" . vertico-multiform-grid)
        ;; ("M-F" . vertico-multiform-flat)
        ;; ("M-U" . vertico-multiform-unobtrusive)
        ;; ("C-<escape>" . dc/vertico-flat-toggle)
        ("S-SPC" . +vertico-restrict-to-matches))
  ;; (:map vertico-flat-map
  ;;       ("C-f" . vertico-next)
  ;;       ("C-b" . vertico-previous))
  :config
  (defun dc/vertico-flat-toggle ()
    (interactive)
    (if vertico-flat-mode
        (vertico-multiform-vertical)
      (vertico-flat-mode)))
  (defun +vertico-restrict-to-matches ()
    "From the Vertico Wiki, restrict list to current filter and clear filter."
    (interactive)
    (let ((inhibit-read-only t))
      (goto-char (point-max))
      (insert " ")
      (add-text-properties (minibuffer-prompt-end) (point-max)
                           '(invisible t read-only t cursor-intangible t rear-nonsticky t))))
  (use-feature vertico-suspend)
  (use-feature vertico-directory
    :after vertico
    :ensure nil
    :bind (:map vertico-map
                ("C-l" . vertico-directory-up)
                ("RET" . vertico-directory-enter)
                ("<backspace>" . vertico-directory-delete-char)
                ("M-<backspace>" . vertico-directory-delete-word)
                ("C-w" . vertico-directory-delete-word))
    :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))
  (use-feature vertico-quick
    :bind (:map vertico-map
                ("M-q" . vertico-quick-insert)
                ("C-q" . vertico-quick-exit)))
  (use-feature vertico-repeat
    :bind
    ("C-M-z" . vertico-repeat)))

(use-package orderless
  :ensure t
  :config
  ;;; This config is taken wholesale from the Consult wiki pages:
  ;;; https://github.com/minad/consult/wiki#minads-orderless-configuration
  (defvar +orderless-dispatch-alist
    '((?% . char-fold-to-regexp)
      (?! . orderless-without-literal)
      (?`. orderless-initialism)
      (?= . orderless-literal)
      (?~ . orderless-flex)))

  ;; Recognizes the following patterns:
  ;; * ~flex flex~
  ;; * =literal literal=
  ;; * %char-fold char-fold%
  ;; * `initialism initialism`
  ;; * !without-literal without-literal!
  ;; * .ext (file extension)
  ;; * regexp$ (regexp matching at end)
  (defun +orderless-dispatch (pattern index _total)
    (cond
     ;; Ensure that $ works with Consult commands, which add disambiguation suffixes
     ((string-suffix-p "$" pattern)
      `(orderless-regexp . ,(concat (substring pattern 0 -1) "[\x100000-\x10FFFD]*$")))
     ;; File extensions
     ((and
       ;; Completing filename or eshell
       (or minibuffer-completing-file-name
           (derived-mode-p 'eshell-mode))
       ;; File extension
       (string-match-p "\\`\\.." pattern))
      `(orderless-regexp . ,(concat "\\." (substring pattern 1) "[\x100000-\x10FFFD]*$")))
     ;; Ignore single !
     ((string= "!" pattern) `(orderless-literal . ""))
     ;; Prefix and suffix
     ((if-let (x (assq (aref pattern 0) +orderless-dispatch-alist))
          (cons (cdr x) (substring pattern 1))
        (when-let (x (assq (aref pattern (1- (length pattern))) +orderless-dispatch-alist))
          (cons (cdr x) (substring pattern 0 -1)))))))

  ;; Define orderless style with initialism by default
  (orderless-define-completion-style +orderless-with-initialism
    (orderless-matching-styles '(orderless-initialism orderless-literal orderless-regexp)))

  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles . (partial-completion orderless)))
                                   ;; enable initialism by default for symbols
                                   (command (styles . (+orderless-with-initialism)))
                                   (variable (styles . (+orderless-with-initialism)))
                                   (symbol (styles . (+orderless-with-initialism)))
                                   ))
  (orderless-component-separator #'orderless-escapable-split-on-space) ;; allow escaping space with backslash!
  (orderless-style-dispatchers '(+orderless-dispatch))
  )

(use-package corfu
                                        ;:defines corfu-map
  ;; :disabled t
  :straight (:files ("*" (:exclude ".git") "extensions/*"))
  :ensure t
  :custom
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-auto nil)                 ;; Enable auto completion
  (corfu-count 14)
  (corfu-echo-documentation t)
  (corfu-min-width 80)
  (corfu-max-width corfu-min-width)
  (corfu-scroll-margin 4)
  (corfu-commit-predicate nil)   ;; Do not commit selected candidates on next input
  (corfu-quit-at-boundary nil)     ;; Automatically quit at word boundary
  (corfu-quit-no-match nil)        ;; Automatically quit if there is no match
  (corfu-preview-current nil)
  ;; :hook
  ;; ((common-lisp-mode conf-mode emacs-lisp-mode go-mode javascript-mode lisp-mode perl-mode python-mode) . corfu-mode)
  ;; Recommended: Enable Corfu globally.
  ;; This is recommended since dabbrev can be used globally (M-/).
  :init
  (defun corfu-move-to-minibuffer ()
    (interactive)
    (let ((completion-extra-properties corfu--extra)
          completion-cycle-threshold completion-cycling)
      (apply #'consult-completion-in-region completion-in-region--data)))
                                        ;(define-key corfu-map "\M-m" #'corfu-move-to-minibuffer)
  :config
  (use-feature corfu-info)
  (use-feature corfu-quick)
  (use-feature corfu-popupinfo)
  (global-corfu-mode)
  :bind ( :map corfu-map
          ("C-n" . corfu-next)
          ("C-p" . corfu-previous)
          ("M-d" . corfu-info-documentation)
          ("M-l" . corfu-info-location)))

(straight-use-package
 '(corfu-terminal
   :type git
   :repo "https://codeberg.org/akib/emacs-corfu-terminal.git"))

(unless (display-graphic-p)
  (corfu-terminal-mode +1))

(use-package cape
  ;; :disabled t
  :ensure t
  ;; Bind dedicated completion commands
  :bind (("C-c c p" . completion-at-point) ;; capf
         ;; ("M-i" . cape-dabbrev)
         ;; ("C-c c t" . complete-tag)        ;; etags
         ;; `cape-abbrev': Complete abbreviation (add-global-abbrev, add-mode-abbrev).
         ;; `cape-dabbrev': Complete word from current buffers.
         ;; `cape-dict': Complete word from dictionary file.
         ;; `cape-elisp-block': Complete Elisp in Org or Markdown code block.
         ;; `cape-elisp-symbol': Complete Elisp symbol.
         ;; `cape-emoji': Complete Emoji.
         ;; `cape-file': Complete file name.
         ;; `cape-history': Complete from Eshell, Comint or minibuffer history.
         ;; `cape-keyword': Complete programming language keyword.
         ;; `cape-line': Complete entire line from file.
         ;; `cape-rfc1345': Complete Unicode char using RFC 1345 mnemonics.
         ;; `cape-sgml': Complete Unicode char from SGML entity, e.g., &alpha.
         ;; `cape-tex': Complete Unicode char from TeX command, e.g. \hbar.

         ("C-c c a" . cape-abbrev)
         ("C-c c d" . cape-dabbrev)
         ("C-c c w" . cape-dict)
         ("C-c c b" . cape-elisp-block)
         ("C-c c s" . cape-elisp-symbol)
         ("C-c c e" . cape-emoji)
         ("C-c c f" . cape-file)
         ("C-c c h" . cape-history)
         ("C-c c k" . cape-keyword)
         ("C-c c l" . cape-line)
         ("C-c c \\" . cape-tex)
         ("C-c c &" . cape-sgml)
         ("C-c c r" . cape-rfc1345))
  :custom
  (cape-dict-file "/usr/share/dict/words")
  (cape-dabbrev-min-length 2)
  :config
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (dolist (complete '(cape-elisp-symbol
                      cape-file
                      cape-keyword
                      cape-dabbrev))
    (add-to-list 'completion-at-point-functions complete)))

(use-package kind-icon
  :ensure t
  :after corfu
  :custom
  (kind-icon-default-face 'corfu-default) ; to compute blended backgrounds correctly
  (kind-icon-blend-background t)
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package embark
  :ensure t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-," . embark-dwim)        ;; good alternative: C-,
   ("C-c C-c" . embark-collect)
   ("C-c C-e" . embark-export))
  (:map help-map
        ("b" . embark-bindings))
  (:map embark-url-map
        ("f" . osx-browse-url-firefox)
        ("c" . osx-browse-url-chrome)
        ("s" . osx-browse-url-safari))
  (:map embark-general-map
        (";" . comment-or-uncomment-region))
  :custom
  ;; Optionally replace the key help with a completing-read interface
  (prefix-help-command 'embark-prefix-help-command)
  (embark-verbose-indicator-display-action '(display-buffer-at-bottom
                                             (window-height . fit-window-to-buffer)))

  (embark-indicators '(embark-highlight-indicator embark-isearch-highlight-indicator embark-minimal-indicator))
  (embark-prompter 'embark-keymap-prompter)
  (embark-quit-after-action '((point-to-register . nil)
                              (flycheck-explain-error-at-point . nil)
                              (kill-buffer . nil)
                              (t . t)))
  :config
  (defvar-keymap embark-consult-flycheck-map
    :doc "Keymap for `consult-flycheck-error' list."
    :parent embark-general-map
    "h" #'flycheck-explain-error-at-point)
  (add-to-list 'embark-keymap-alist '(consult-flycheck-error . embark-consult-flycheck-map))

  (defvar-keymap embark-consult-location-map
    :doc "Keymap for performing actions on registers."
    :parent embark-general-map
    "r" #'point-to-register)
  (add-to-list 'embark-keymap-alist '(consult-location . embark-consult-location-map))

  (defvar-keymap embark-straight-map
    :doc "Keymap for operating straight.el actions on use-package configured packages."
    :parent embark-library-map
    "u" #'straight-visit-package-website
    "i" #'straight-use-package
    "c" #'straight-check-package
    "F" #'straight-pull-package
    "f" #'straight-fetch-package
    "p" #'straight-push-package
    "n" #'straight-normalize-package
    "m" #'straight-merge-package)
  (add-to-list 'embark-keymap-alist '(library . embark-straight-map))

  ;; provided by the wiki
  (defun +embark-live-vertico ()
    "Shrink Vertico minibuffer when `embark-live' is active."
    (when-let (win (and (string-prefix-p "*Embark Live" (buffer-name))
                        (active-minibuffer-window)))
      (with-selected-window win
        (when (and (bound-and-true-p vertico--input)
                   (fboundp 'vertico-multiform-unobtrusive))
          (vertico-multiform-unobtrusive)))))

  (add-hook 'embark-collect-mode-hook #'+embark-live-vertico))

(use-package consult
  :ensure t
  :defines
  (consult-buffer-sources consult-customize)
  :commands (dc/vertico-next
             dc/vertico-previous)

  :bind (;; C-c c bindings (mode-specific-map)
         ("C-c c h" . consult-history)
         ("C-c c m" . consult-mode-command)
         ("C-c c K" . consult-kmacro)
         ("C-c o A" . consult-org-agenda)

         ;; C-x bindings (ctl-x-map)
         ("C-x M-:" . consult-complex-command) ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)          ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame) ;; orig. switch-to-buffer-other-frame
         ("C-x C-r" . consult-recent-file)

         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-@" . consult-register-store) ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ("M-g r" . consult-register)

         ;; Other custom bindings
         ("M-y" . consult-yank-pop)     ;; orig. yank-pop

         ;; M-g bindings (goto-map)
         ;; ("M-g f" . consult-flymake)               ;; We install and set consult-flymake in config below
         ("M-g b" . consult-bookmark)
         ("M-g e" . consult-compile-error)
         ("M-g g" . consult-goto-line)   ;; orig. goto-line
         ("M-g M-g" . consult-goto-line) ;; orig. goto-line
         ("M-g o" . consult-outline)     ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)

         ;; M-s bindings (search-map)
         ("M-s f" . consult-find)
         ("M-s F" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s M-s" . dc/consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-symbol-at-point)
         ("M-s m" . consult-line-multi) ;; or plain multi-occur
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ("M-s s" . consult-eglot-symbols)

         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         (:map isearch-mode-map
               ("M-e" . consult-isearch-history)   ;; orig. isearch-edit-string
               ("M-s e" . consult-isearch-history) ;; orig. isearch-edit-string
               ("M-s l" . consult-line) ;; needed by consult-line to detect isearch
               ("M-s L" . consult-line-multi)) ;; needed by consult-line to detect isearch
         (:map help-map
               ("M" . consult-man)
               ("I" . consult-info)
               ("L" . consult-locate)
               ("t" . consult-theme))
         (:map consult-narrow-map
               ("<?" . consult-narrow-help)))
  :hook
  (completion-list-mode . consult-preview-at-point-mode)
  :custom
  (consult-narrow-key "<")
  ;; (consult-widen-key ">") ;; setting this returns from a narrowing into a `nil' focus with no candidates
  (consult-locate-args dc/locate-args-string)
  (consult-ripgrep-args dc/rg-args-string)
  (consult-man-args dc/man-args-string)
  (completion-in-region-function (lambda (&rest args)
                                   (apply (if vertico-mode
                                              #'consult-completion-in-region
                                            #'completion--in-region)
                                          args)))
  (consult-project-root-function (when (boundp projectile-project-root)
                                   'projectile-project-root))
  (register-preview-delay 0)                                 ; improve register loading
  (register-preview-function #'consult-register-format)
  (xref-show-xrefs-function #'consult-xref)
  (xref-show-definitions-function #'consult-xref)
  :init

  (define-minor-mode consult-ripgrep-mode
    "Consult ripgrep minor mode."
    :group 'search
    :interactive nil
    :keymap (let ((map (make-sparse-keymap)))
              (define-key map (kbd "<f2>") 'minibuffer-keyboard-quit)
              map)

    (if consult-ripgrep-mode
        (add-hook 'minibuffer-mode-hook #'consult-ripgrep-mode nil t)
      (remove-hook 'minibuffer-mode-hook #'consult-ripgrep-mode)))

  (defun dc/consult-ripgrep (&optional arg)
    "A wrapper for `consult-ripgrep'. Search for regexp with rg in
`projectile-project-root' with region if active. If provided,
 temporarily set the vertico-count to ARG."
    (interactive "P")
    (let* ((consult-ripgrep-mode t)
           (initial (if (use-region-p)
                        (buffer-substring (region-beginning) (region-end))
                      (thing-at-point 'symbol)))
           (dir (or (when (boundp projectile-project-root)
                      (projectile-project-root))
                    (file-name-directory (buffer-file-name)))))
      (let ((consult-ripgrep-args
             (if (= vertico-count 1)
                 (s-replace "--no-heading" "" consult-ripgrep-args)
               consult-ripgrep-args)))
        (consult-ripgrep dir initial))))

  (defun consult-line-symbol-at-point ()
    (interactive)
    (consult-line (thing-at-point 'symbol)))

  (advice-add #'register-preview :override #'consult-register-window)
  (advice-add 'find-function :before #'xref-push-marker-stack)

  :config
  ;; add org source
  (autoload 'org-buffer-list "org")
  (defvar org-buffer-source
    (list :name     "Org Buffer"
          :category 'buffer
          :narrow   ?o
          :face     'consult-buffer
          :history  'buffer-name-history
          :state    #'consult--buffer-state
          :new
          (lambda (name)
            (with-current-buffer (get-buffer-create name)
              (insert "#+title: " name "\n\n")
              (org-mode)
              (consult--buffer-action (current-buffer))))
          :items
          (lambda ()
            (mapcar #'buffer-name
                    (seq-filter
                     (lambda (x)
                       (eq (buffer-local-value 'major-mode x) 'org-mode))
                     (buffer-list))))))
  (add-to-list 'consult-buffer-sources 'org-buffer-source 'append)

  ;; configure consult previews
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep
   consult-git-grep
   consult-grep
   ;; consult-lsp-symbols
   consult-bookmark
   consult-recent-file
   consult-xref
   ;; sources
   consult--source-bookmark
   consult--source-buffer
   consult--source-project-buffer
   consult--source-project-recent-file
   consult--source-recent-file
   ;; persp-consult-source
   :preview-key "M-.")

  (consult-customize consult--source-buffer :hidden t :default nil)
  (add-to-list 'consult-buffer-sources persp-consult-source)
  (add-to-list 'consult-buffer-filter "^\\*")

  (recentf-mode))
;; consult plugins
(use-package consult-flycheck
  :ensure t
  :straight (:host github :repo "minad/consult-flycheck")
  :bind ("M-g f" . consult-flycheck))
(use-package consult-dir
  :ensure t
  :bind (("C-x C-d" . consult-dir)
         :map vertico-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file)))

(use-package consult-projectile
  :ensure t
  :straight (consult-projectile :type git :host gitlab :repo "OlMon/consult-projectile" :branch "master")
  :bind (("C-c p j" . consult-projectile)
         ("C-c p e" . consult-projectile-recentf)
         ("C-c p b" . consult-projectile-switch-to-buffer)
         ("C-c p f" . consult-projectile-find-file)
         ("s-j" . consult-projectile)
         ("s-e" . consult-projectile-recentf)
         ("s-b" . consult-projectile-switch-to-buffer)
         ("s-p f" . consult-projectile-find-file))
  :custom
  (projectile-switch-project-action 'consult-projectile)
  :config
  (consult-customize
   consult-projectile--source-projectile-dir
   consult-projectile--source-projectile-file
   consult-projectile--source-projectile-project
   consult-projectile--source-projectile-recentf
   :preview-key "M-."))

(use-package consult-eglot :straight (:host github :repo "mohkale/consult-eglot")
  :bind
  (:map eglot-mode-map
        ("M-g s" . consult-eglot-symbols)))

(use-package consult-todo
  :straight (:host github :repo "liuyinz/consult-todo")
  :after hl-todo
  :demand t
  :bind
  ("C-c t" . consult-todo)
  ("C-c T" . consult-todo-project))

(use-package bookmark-view
  :disabled t
  :straight (:host github :repo "minad/bookmark-view")
  :defines consult-buffer-sources
  :custom
  ;; Modify bookmark source, such that views are hidden
  (consult--source-bookmark
   (plist-put
    consult--source-bookmark :items
    (lambda ()
      (bookmark-maybe-load-default-file)
      (mapcar #'car
              (seq-remove (lambda (x)
                            (eq #'bookmark-view-handler
                                (alist-get 'handler (cdr x))))
                          bookmark-alist)))))
  :config
  ;;; Configuration to create new bookmark-view source is from consult docs.
  ;; Configure new bookmark-view source
  (add-to-list 'consult-buffer-sources
               (list :name     "View"
                     :narrow   ?v
                     :category 'bookmark
                     :face     'font-lock-keyword-face
                     :history  'bookmark-view-history
                     :action   #'consult--bookmark-action
                     :items    #'bookmark-view-names)
               'append))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t
  :after (embark consult)
  :demand t
  ;; only necessary if you have the hook below
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package recursion-indicator
  :blackout t
  :demand t
  :config
  (recursion-indicator-mode))

(use-package mini-popup
  :disabled t
  :straight (:host github :repo "minad/mini-popup")
  :init
  ;; Configure a height function (Example for Vertico)
  (defun mini-popup-height-resize ()
    (* (1+ (min vertico--total vertico-count)) (default-line-height)))
  (defun mini-popup-height-fixed ()
    (* (1+ (if vertico--input vertico-count 0)) (default-line-height)))
  (setq mini-popup--height-function #'mini-popup-height-fixed)

  ;; Disable the minibuffer resizing of Vertico (HACK)
  (advice-add #'vertico--resize-window :around
              (lambda (&rest args)
                (unless mini-popup-mode
                  (apply args))))

  ;; Ensure that the popup is updated after refresh (Consult-specific)
  (add-hook 'consult--completion-refresh-hook
            (lambda (&rest _) (mini-popup--setup)) 99))

(provide 'dc-movec)

;;; dc-movec.el ends here
