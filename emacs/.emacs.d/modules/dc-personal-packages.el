;;; dc-personal-packages.el --- import my personal packages from my-packages/ -*- lexical-binding: t -*-

;; Author: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

(add-to-list 'load-path "~/.emacs.d/modules/my-packages/" t)

;; (require 'selenium-server)

(provide 'dc-personal-packages)

;;; dc-personal-packages.el ends here
