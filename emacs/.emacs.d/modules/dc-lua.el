;;; dc-lua.el --- lua-mode -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

(use-package lua-mode
  :straight (:host github :repo "immerrr/lua-mode")
  :config
  (add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
  (add-to-list 'interpreter-mode-alist '("lua" . lua-mode)))

(provide 'dc-lua)

;;; dc-lua.el ends here
