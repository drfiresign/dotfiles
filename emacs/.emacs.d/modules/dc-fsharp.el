;;; dc-fsharp.el --- f-sharp -*- lexical-binding: t -*-

;;; Commentary:

;; commentary

;;; Code:

(use-package fsharp-mode
  :mode "\\.fs[iylx]?$"
  :custom
  (fill-column 100)
  (fsharp-ac-intellisense-enabled t)
  (inferior-fsharp-program "dotnet fsi --readline-")
  :config
  (setq-default fsharp-indent-offset 2)
  (require 'eglot-fsharp)
  :hook
  (fsharp-mode . (lambda ()
                   (electric-indent-local-mode -1)
                   (highlight-indentation-mode 1)
                   (setq require-final-newline nil
                         dabbrev-check-all-buffers nil
                         ;; warning-minimum-level :error
                         )
                   ;; TODO[dcascio|2022-08-25] Remove when this is resolved
                   ;; https://github.com/fsharp/FsAutoComplete/issues/1000
                   (-let [dr "DOTNET_ROOT"]
                     (setenv dr (exec-path-from-shell-getenv dr)))
                   ;; (lsp-deferred)
                   )))
(use-package ob-fsharp)
(provide 'dc-fsharp)

;;; dc-fsharp.el ends here
