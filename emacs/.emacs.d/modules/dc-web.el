;;; dc-web.el --- internet tooling -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

(global-set-key [mouse-2] 'browse-url-at-mouse)

;; (use-package emacs-w3m)

(provide 'dc-web)

;;; dc-web.el ends here
