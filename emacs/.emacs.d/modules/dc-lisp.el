;;; dc-lisp.el --- lispy lisps -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;;; lisp -- common lisp / clisp / sbcl

;; the one, the only... now available inside Emacs.
(use-package sicp
  :straight (:host github :repo "webframp/sicp-info"))

;; suggested by roswell installation guide
;; (load (expand-file-name "~/.roswell/helper.el"))

(use-package slime
  ;; :demand t
  :custom
  (inferior-lisp-program "ros -Q run")
  (slime-net-coding-system 'utf-8-unix)
  (slime-contribs '(slime-fancy
                    slime-quicklisp
                    slime-asdf))
  :init
  (add-to-list 'slime-contribs 'slime-repl))

;;; Scheme

;; (straight-use-package 'xscheme)

(use-package geiser
  ;; :requires (scheme-mode lispy)
  :demand t
  :custom
  (geiser-active-implementations '(mit)))

(add-hook 'scheme-mode-hook 'geiser-mode)

;; ;;; racket -- not installed
;; (use-package racket-mode
;;   :custom
;;   (racket-program '/Applications/Racket v7.2/bin/racket)
;;   :hook
;;   (racket-mode-hook . racket-unicode-input-method-enable)
;;   (racket-repl-mode-hook . racket-unicode-input-method-enable))


(provide 'dc-lisp)

;;; dc-lisp.el ends here
