;;; dc-python.el --- python things; -*- lexical-binding: t; mode: emacs-lisp; -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; TODO: This needs an overhaul. First I need a python project.

;;; Code:

;; (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode))
(use-package python-mode
  :defer t
  :custom
  (py-shell-name "python3")
  (python-shell-interpreter "python3.12")
  (python-interpreter "python3.12")
  (python-shell-completion-native-enable nil)
  :config
  ;; (if (treesit-language-available-p 'python)
  ;;     (progn
  ;;       (dolist (checker '(python-flake8
  ;;                          python-pycompile
  ;;                          python-pyright
  ;;                          python-mypy
  ;;                          python-pylint))
  ;;         (flycheck-add-mode checker 'python-ts-mode)
  ;;         )))
  )

(use-package ruff-format
  :straight (:host github :repo "scop/emacs-ruff-format"))

(use-package pipenv
  :blackout t
  :hook (python-mode . pipenv-mode)
  :init
  (setq pipenv-projectile-after-switch-function #'pipenv-projectile-after-switch-extended)
  )

;; (use-package lsp-python-ms :blackout t
;;   :ensure t
;;   :custom
;;   (lsp-python-ms-auto-install-server t)
;;   (lsp-python-ms-python-executable-cmd "python3")
;;   :hook
;;   (python-mode . (lambda () (require 'lsp-python-ms) (lsp-deferred))))
                                        ; or lsp

(provide 'dc-python)

;;; dc-python.el ends here
