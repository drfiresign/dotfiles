;;; package: --- custom keybindings -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;; TODO[dcascio|2021-08-27] Relocate all these into the bind-keys macro. See dc-mac.el

;; --- unset things I hate

(defgroup dc/keybindings nil
  "Custom keybindings for Dylan."
  :group 'keybindings)

(defcustom dc/disable-keybindings
  '(;; I hit these accidentally, or they interfere with another command I like.
    "C-z" "C-x C-z" "C-x C-l" "C-x C-u"
    ;; MacOS NS related commands
    "s-h" "s-t" "s-m" "s-q" "s-w" "s-n" "s-," "s-o" "s-~" "s-g"
    ;; Text scaling keys
    "C-=" "C--" "<f11>"
    )
  "Keybindings which should be disabled upon startup."
  :group 'dc/keybindings
  :type 'list)

(dolist (binding dc/disable-keybindings)
  (global-unset-key (kbd binding)))

(defadvice keyboard-escape-quit
    (around keyboard-escape-quit-dont-close-windows activate)
  (let ((buffer-quit-function (lambda () ())))
    ad-do-it))

(use-package kkp
  :ensure t
  :config
  ;; (setq kkp-alt-modifier 'alt) ;; use this if you want to map the Alt keyboard modifier to Alt in Emacs (and not to Meta)
  (global-kkp-mode +1))

(use-package emacs
  :bind
  ;; ("C-c e" . dc/visit-emacs-config)
  ("<escape>" . keyboard-escape-quit)
  ("C-x x t" . toggle-truncate-lines)
  ("C-x x w" . toggle-word-wrap)
  ("C-x x e" . toggle-debug-on-error)
  ("M-g #" . global-display-line-numbers-mode)
  ;; ("C-x K" . kill-this-buffer)
  ("C-<tab>" . next-buffer)
  ("C-S-<tab>" . previous-buffer)
  ("C-x o" . dc/other-buffer)
  ("C-x O" . dc/other-other-buffer)
  ("C-x <deletechar>" . kill-sentence)
  ("C-x x d" . delete-pair)
  ("C-\\" . delete-horizontal-space-but-one)
  ([remap zap-to-char] . zop-up-to-char)
  ("M-\\" . delete-horizontal-space)
  ([remap ns-open-file-using-panel] . find-file)
  ;; ("M-O" . dc/previous-window)
  ("C-x C-#" . dc/comment-string)
  ("M-<up>" . beginning-of-defun)
  ("M-<down>" . end-of-defun)
  ("M-{" . beginning-of-defun)
  ("M-}" . end-of-defun)
  ("M-Q" . fill-paragraph)
  ("M-i" . dabbrev-completion)
  ("M-/" . undo)

  ("C-c r r" . reverse-region)
  ("C-c r e" . eval-region)
  ("C-c r s" . ispell-region)
  ("C-c r i" . indent-region)
  ("C-c r m" . mc/mark-all-in-region)
  ("C-c r x" . shell-command-on-region)
  ("C-c r <tab>" . canonically-space-region)

  ("C-x >" . scroll-left)  ; swap the counter-intuitive
  ("C-x <" . scroll-right) ; left/right window scrolling

  ;; ;; super commands
  ("s-d" . dired-jump-other-window)
  ;; ("s-1" . delete-other-windows)

  ;; ("s-2" . dc/split-window-below-and-switch)
  ;; ("s-3" . dc/split-window-right-and-switch)
  ;; ("s-0" . delete-window)

  ;; ("C-M-+" . dc/split-window-below-and-switch)
  ;; ("C-+" . dc/split-window-right-and-switch)
  ;; ("C-_" . delete-window)

  ;; ("s-4" . ctl-x-4-prefix)
  ;; ("s-5" . ctl-x-5-prefix)
  ;; ("s-z" . repeat)

  ;; windmove commands
  ("H-h" . yabai-window-left)
  ("H-j" . yabai-window-down)
  ("H-k" . yabai-window-up)
  ("H-l" . yabai-window-right)

  ("H-<left>" . yabai-window-left)
  ("H-<down>" . yabai-window-down)
  ("H-<up>" . yabai-window-up)
  ("H-<right>" . yabai-window-right)

  ;; scrolling
  ("M-s-v" . scroll-down-line)
  ("C-s-v" . scroll-up-line)

  ;; transparency
  ;; ("M-0" . dc/toggle-transparency)

  ;; region commands
  ;; when region is active, make `capitalize-word' and friends act on it
  ("M-c" . capitalize-dwim)
  ("M-l" . downcase-dwim)
  ("M-u" . upcase-dwim)
  ("C-M-<prior>" . beginning-of-defun)
  ("C-M-<next>" . end-of-defun)

  ;; remapping
  ([remap move-beginning-of-line] . smarter-move-beginning-of-line)

  ;; frames
  ("C-x 5 O" . (lambda () (interactive) (other-frame -1)))
  ("C-`" . suspend-frame)

  ;; misc?
  ("<help>" . overwrite-mode)
  ("s-m" . kmacro-keymap)

  (:map help-map
        ("m" . describe-mode)
        ("M" . man)
        ("I" . info)
        ("L" . locate)
        ("t" . load-theme)
        ("s" . describe-symbol))
  :config
  (dc/create-insert-signed-comments "todo" "TODO" "C-c C-t")
  (dc/create-insert-signed-comments "note" "NOTE" "C-c C-n")
  (dc/create-insert-signed-comments "question" "???" "C-c C-?"))

(defvar dc/repeat-map
  (-let [map (make-sparse-keymap)]
    (define-key map "o" 'dc/other-buffer)
    (define-key map "O" 'dc/other-other-buffer)
    map)
  "My personal repeat-map.")
(put 'dc/other-buffer 'repeat-map 'dc/repeat-map)


(provide 'dc-keybindings)

;;; dc-keybindings.el ends here
