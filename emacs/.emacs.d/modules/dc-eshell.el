;;; dc-eshell.el --- Eshell behaviors for emacs -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; great thanks to howard abrams for his talk on eshell.

;;; Code:

(require 'em-term)
(require 'eshell)

(setq eshell-destroy-buffer-when-process-dies t)

(add-to-list 'eshell-visual-commands "top") ; programs which need special displays
(add-to-list 'eshell-visual-options '("git" "--help")) ; commands which trigger curses / pager
(add-to-list 'eshell-visual-subcommands ; sub-commands which trigger curses / pager
             '("git" "log" "diff" "show"))

(use-package eshell-prompt-extras)

(with-eval-after-load "esh-opt"
  ;; (require 'virtualenvwrapper)
  ;; (venv-initialize-eshell)
  (autoload 'epe-theme-pipeline "eshell-prompt-extras")
  (autoload 'epe-theme-lambda "eshell-prompt-extras")
  (autoload 'epe-theme-dakrone "eshell-prompt-extras")
  (autoload 'epe-theme-multiline-with-status "eshell-prompt-extras")
  (setq eshell-highlight-prompt t
        eshell-prompt-function 'epe-theme-lambda))

;; (use-package xterm-color)
;; (add-hook 'eshell-before-prompt-hook (lambda () (setq xterm-color-preserve-properties t)))

;; (use-package eshell
;;   :custom
;;   (eshell-preoutput-filter-functions 'xterm-color-filter)
;;   (eshell-output-filter-functions (remove 'eshell-handle-ansi-color eshell-output-filter-functions))
;;   :init
;;   (setq eshell-output-filter-functions (remove 'eshell-handle-ansi-color eshell-output-filter-functions))
;;   (setenv "TERM" "xterm-256color"))


(provide 'dc-eshell)
;;; dc-eshell.el ends here
