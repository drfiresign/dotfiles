;;; dc-terminal.el --- vterm and env configuration -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;;; Commentary:

;; Configuration for vterm and anything which takes advantage of the shell.

;;; Code:

(add-hook 'compilation-filter-hook 'ansi-color-compilation-filter)

(use-package eterm-256color
  :ensure t
  :config
  (add-hook 'vterm-mode-hook #'eterm-256color-mode))

(add-to-list 'display-buffer-alist
             `(,(rx (| "\\*e?shell\\*"
                       "\\*v?term\\*"))
               display-buffer-in-direction
               (direction . bottom)
               (window . root)
               (window-height . 0.3)))

(use-package vterm
  :straight (:host github :repo "akermu/emacs-libvterm" :repo-dir (concat user-emacs-directory "straight/repos/vterm"))
  :custom
  (vterm-install t)
  (vterm-shell 'fish)
  (vterm-eval-cmds
   '(("find-file" find-file) ; default cmds
     ("message" message)
     ("vterm-clear-scrollback" vterm-clear-scrollback)
     ("man" man); custom cmds
     ("ffo" find-file-other-window)
     ("ffro" find-file-read-only)
     ("dired" dired)))
  (vterm-kill-buffer-on-exit t)
  (vterm-always-compile-module t)
  (vterm-max-scrollback 100000)
  ;; (vterm-buffer-name-string "vterm %s")
  ;; (vterm-term-environment-variable "eterm-color")
  (vterm-use-vterm-prompt-detection-method t)
  (vterm-copy-exclude-prompt t)
  :config
  (defun dc/vterm-window-width ()
    (setq-local vterm-min-window-width (window-body-width)))
  (defun turn-off-chrome ()
    (hl-line-mode -1)
    (display-line-numbers-mode -1))
  :bind
  ;; ("<f1>" . vterm)
  ;; (:map vterm-mode-map
  ;;       ("<f1>" . vterm))
  (:map vterm-mode-map
        ("M-<backspace>" . vterm-send-meta-backspace))
  :hook
  (vterm-mode . turn-off-chrome)
  ;; (vterm-mode . dc/vterm-window-width)
  (vterm-redraw-display-hook . dc/vterm-window-width))

(use-package multi-vterm
  :after projectile
  :ensure t
  :straight (multi-vterm :host github :repo "suonlight/multi-vterm")
  :bind
  ("C-c v m" . multi-vterm)
  ("C-c v n" . multi-vterm-next)
  ("C-c v p" . multi-vterm-prev)
  ;; ("C-c v v" . multi-vterm-dedicated-toggle)
  ("C-M-s-`" . multi-vterm-dedicated-toggle)
  ("C-c v v" . multi-vterm-project)
  ;; ("C-H-s-SPC" . multi-vterm-dedicated-toggle)
  ("C-M-`" . multi-vterm-project)
  ;; ("C-H-s-p SPC" . multi-vterm-project)
  :config
  ;; (setq multi-vterm-dedicated-window-height 40)
  (setq multi-vterm-dedicated-window-height-percent 30)
  )

(use-package bash-completion
  :straight (:host github :repo "szermatt/emacs-bash-completion")
  :config
  (autoload 'bash-completion-dynamic-complete
    "bash-completion"
    "BASH completion hook")
  (add-hook 'shell-dynamic-complete-functions
            'bash-completion-dynamic-complete))

;; (use-package piper :straight (:type git :host gitlab :repo "howardabrams/emacs-piper"))

(provide 'dc-terminal)

;;; dc-terminal.el ends here
