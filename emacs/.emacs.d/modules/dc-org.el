;;; dc-org.el --- org-mode -*- mode: emacs-lisp; mode: rainbow; lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

(use-package restclient
  :straight (:host github :repo "pashky/restclient.el"))
(use-package ob-restclient
  :straight (:host github :repo "alf/ob-restclient.el"))

(use-package org
  :custom
  (org-fontify-quote-and-verse-blocks t)
  (org-startup-indented 'nil)
  (org-edit-src-content-indentation 0)
  (org-babel-load-languages
   '((awk . t) (C . t) (emacs-lisp . t) (org . t)
     (restclient . t) (python . t) (jq . t)
     (shell . t) (fsharp . t)))
  (org-babel-python-command "python3")
  (org-directory "~/org")
  (org-default-notes-file "~/org/tasks.org")
  (org-agenda-files `(;; ,dc/org-work-file
                      ,org-default-notes-file
                      ))
  (org-todo-keywords
   '((sequence "IN PROGRESS(i)" "TO DO(t)" "HOLD(x)" "BACKLOG(b)" "|" "DONE(d!)" "CANCELLED(c!)")))
  (org-todo-keyword-faces
   '(("TO DO" . org-todo) ;; On deck, next actionable item
     ("DONE" . org-done) ;; Completed
     ("IN PROGRESS" . org-cite) ;; In consideration
     ("HOLD" . org-dispatcher-highlight) ;; Blocked
     ("BACKLOG" . org-column)
     ("CANCELLED" . (:inherit org-warning :strike-through t :weight bold))))

  ;; org-clock :: http://doc.norang.ca/org-mode.html#Clocking
  (org-drawers '("PROPERTIES" "LOGBOOK"))
  (org-clock-history-length 8)
  (org-clock-in-resume t)
  (org-clock-persist t)
  (org-clock-persist-query-resume nil)
  (org-clock-out-remove-zero-time-clocks t)
  (org-clock-auto-clock-resolution 'when-no-clock-is-running)
  (org-clock-mode-line-total 'current)

  (org-capture-templates
   '(("C" "add new task and clock in" entry (file+olp "~/org/sre.org" "tasks")
      "** %?"
      :clock-in t :clock-keep t :empty-lines 1)
     ("c" "add note to clocked task" item (clock)
      "%U :: %?"
      :clock-keep t :clock-resume t :empty-lines 1)
     ("t" "task: work" entry (file+olp "~/org/sre.org" "tasks")
      "** BACKLOG %?"
      :empty-lines 1)
     ("p" "task: personal" entry (file "")
      "** BACKLOG %?"
      :prepend t :empty-lines 1)
     ("n" "notes: misc (refile)" entry (file "~/org/refile.org")
      "* %?"
      :empty-lines 1)
     ("o" "oncall log" entry (file+olp+datetree "~/org/oncall.org")
      "* %?\n"
      :prepend t)
     ("a" "achievements" entry (file+olp+datetree "~/org/sre.org" "achievements" )
      "** %^{your achievement}\n%?\n- impact :: %^{impact of your achievement}"
      :empty-lines 1)
     ("g" "gratitude" entry (file+olp+datetree "~/org/gratitude.org")
      ""
      :kill-buffer t :empty-lines 1)
     ("r" "reading material" entry (file+olp "~/org/sre.org" "errata" "reading material")
      "*** %^{Source}"
      :empty-lines 1)
     ("f" "file: add task" entry (file+headline "" "tasks")
      "** %f: %^{Description}\n:PROPERTIES:\n:FILEPATH: %F\n:LINK: %a\n:END:\n%?" ; "** %?\n  See: %a"
      :empty-lines 1)
     ("F" "file: add note to clocked task" item (clock)
      (function
       (lambda () (let* ((timestamp (format-time-string "%Y-%M-%d %a %k:%M"))
                    (file-name (buffer-file-name))
                    (file-base (file-name-nondirectory file-name))
                    (line-number (line-number-at-pos (region-beginning)))
                    (lang (replace-regexp-in-string "-mode" "" (format "%s" major-mode)))
                    (capture-region (buffer-substring-no-properties (mark) (- (point) 1))))
               (format "- [%s] :: [[file:%s::%s][%s]]\n  #+begin_src %s\n  %s\n  #+end_src\n\n  "
                       timestamp file-name line-number file-base lang capture-region))))
      :empty-lines 1)
     ("?" "questions to answer" entry (file+olp "~/org/sre.org" "questions")
      "** %?")
     ("m" "music" entry (file "~/org/music.org")
      "* %^{artist: album/title}\n%t\n%?"
      :prepend t :empty-lines 1)))

  (org-confirm-babel-evaluate nil) ;; Don't ask before src block eval

  ;; refiling defaults
  (org-refile-targets '((nil :maxlevel . 3) (org-agenda-files :maxlevel . 2)))
  (org-refile-allow-creating-parent-nodes 'confirm)
  (org-refile-use-outline-path 'file) ;; let me refile via the full outline path of a header
  (org-outline-path-complete-in-steps nil)
  (org-refile-active-region-within-subtree t)

  ;; org-agenda
  (org-agenda-window-setup 'current-window)
  (org-agenda-text-search-extra-files '(agenda-archives)) ; not sure what this is either
  (org-agenda-search-headline-for-time nil) ; unsure what this does yet
  (org-agenda-timegrid-use-ampm nil)
  (org-agenda-span 14)
  (org-agenda-custom-commands ;; specific agenda views for specific states
   '(("h" . "Habits")
     ("hd" "Daily Habits"
      (agenda "")
      ((org-agenda-show-log t)
       (org-agenda-log-mode-items '(state))
       (org-agenda-skip-function '(org-agenda-skip-entry-if 'notregexp ":DAILY:"))))
     ("hw" "Weekly Habits"
      (agenda "")
      ((org-agenda-show-log t)
       (org-agenda-log-mode-items '(state))
       (org-agenda-skip-function '(org-agenda-skip-entry-if 'notregexp ":WEEKLY:"))))
     ))
  :hook
  ((org-mode . rainbow-mode)
   ;; (org-mode . auto-fill-mode)
   (org-mode . visual-line-mode)
   ;; (org-mode . org-indent-mode)
   )
  :bind
  (("C-c o a" . org-agenda)
   ("C-c o c" . org-capture)
   ("C-c o t" . org-clock-goto)
   ("C-c o l" . org-store-link)
   ;; ("C-c o j" . dc/find-org-default-notes)
   ("C-c o w" . dc/find-org-work-file)
   ;; (:map org-mode-map
   ;;       ("C-}" . org-down-element)
   ;;       ("C-{" . org-up-element)
   ;;       ("C-]" . org-forward-element)
   ;;       ("C-[" . org-backward-element))
   )
  :config
  (org-clock-persistence-insinuate)
  (add-hook 'completion-at-point-functions 'pcomplete-completions-at-point nil t)
  (defun dc/find-org-work-file () (interactive) (find-file dc/org-work-file))
  (defun dc/find-org-default-notes () (interactive) (find-file org-default-notes-file))
  (defun dc/clock-in-organization-task-as-default ()
    "Clock into the default org task when opening Emacs."
    (interactive)
    (org-with-point-at (org-id-find dc/organization-task-id 'marker)
      (org-clock-in '(16))))
  (defvar org-created-property-name "CREATED"
    "The name of the org-mode property that stores the creation date of an entry.")
  (defun dc/org-set-created-property (&optional active NAME)
    "Set a property on the entry giving the creation time.

By default the property is called CREATED. If given the `NAME'
argument will be used instead. If the property already exists, it
will not be modified."
    (interactive)
    (let* ((created (or NAME org-created-property-name))
           (fmt (if active "<%s>" "[%s]"))
           (now (format fmt (format-time-string "%Y%m%d" ;; " %a %k:%M"
                                                ))))
      (unless (or (org-entry-get (point) created nil)
                  (eq major-mode 'org-journal-mode))
        (org-set-property created now)))
    )
  ;; (add-hook 'org-capture-before-finalize-hook #'dc/org-set-created-property)
  )

;; (use-package org-notmuch)
;; (require 'ob-js)
;; org-super-agenda
;; examples: https://github.com/alphapapa/org-super-agenda/blob/master/examples.org

;; helm-org
(if (eq dc/completion-system "helm")
    (use-package helm-org :init
      (add-to-list 'helm-completing-read-handlers-alist '(org-capture . helm-org-completing-read-tags))
      (add-to-list 'helm-completing-read-handlers-alist '(org-set-tags . helm-org-completing-read-tags))))

;; org exports

(use-package mermaid-mode
  :custom
  (ob-mermaid-cli-path "/usr/local/bin/mmdc"))

(use-package htmlize)
;; (use-package ox-twbs)
(use-package ox-gfm)
(use-package ox-rst)
;; (use-package ox-slack)
;; (use-package ox-pandoc)
;; (use-package org-pandoc-import :straight (:host github :repo "tecosaur/org-pandoc-import" :files ("*.el" "filters" "preprocessors")))

(setq org-export-backends '(html md odt rst gfm)
      org-export-with-toc nil)

(use-package ob-async ;; via: https://emacs.stackexchange.com/questions/2952/display-errors-and-warnings-in-an-org-mode-code-block
  :defer t
  :config
  (defvar org-babel-eval-verbose t
    "A non-nil value makes `org-babel-eval' display")

  (defun org-babel-eval (cmd body)
    "Run CMD on BODY.
If CMD succeeds then return its results, otherwise display
STDERR with `org-babel-eval-error-notify'."
    (let ((err-buff (get-buffer-create " *Org-Babel Error*")) exit-code)
      (with-current-buffer err-buff (erase-buffer))
      (with-temp-buffer
        (insert body)
        (setq exit-code
              (org-babel--shell-command-on-region
               (point-min) (point-max) cmd err-buff))
        (if (or (not (numberp exit-code)) (> exit-code 0)
                (and org-babel-eval-verbose (> (buffer-size err-buff) 0))) ; new condition
            (progn
              (with-current-buffer err-buff
                (org-babel-eval-error-notify exit-code (buffer-string)))
              nil)
          (buffer-string))))))

(provide 'dc-org)

;;; dc-org.el ends here
