;;; dc-search.el --- search for things -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Searching or sorting through code.

;;; Code:

(use-package ripgrep
  :bind (:map ripgrep-search-mode-map ("q" . delete-window)))

(use-feature re-builder
  :custom
  (reb-re-syntax 'string)
  :bind
  ("M-s R" . re-builder))

(use-package wgrep)
(use-package visual-regexp
  :disabled t)
(use-package visual-regexp-steroids :straight (:host github :repo "benma/visual-regexp-steroids.el")
  :disabled t
  :custom
  (vr/command-python "/usr/local/opt/python@3.10/bin/python3 /Users/dcascio/.straight.el/straight/build/visual-regexp-steroids/regexp.py")
  :bind
  ([remap query-replace] . vr/query-replace)
  ([remap query-replace-regexp] . vr/query-replace)
  ("M-s r" . vr/replace)
  ("C-c m" . vr/mc-mark)
  ("C-M-r" . vr/isearch-backward)
  ("C-M-s" . vr/isearch-forward))

(use-package anzu
  :blackout t
  :straight (:host github :repo "emacsorphanage/anzu")
  :init (global-anzu-mode +1)
  :custom (vr/default-replace-preview t)
  :bind
  ([remap isearch-query-replace] . anzu-isearch-query-replace)
  ([remap isearch-query-replace-regexp] . anzu-isearch-query-replace-regexp)
  ([remap query-replace]. anzu-query-replace)
  ([remap query-replace-regexp]. anzu-query-replace-regexp))

;; (use-package hyperbole)
(use-package link-hint
  :ensure t
  :bind
  ("C-c l o" . link-hint-open-link)
  ("C-c l f" . (lambda () (interactive)
                 (let ((browse-url-browser-function 'osx-browse-url-firefox))
                   (link-hint-open-link))))
  ("C-c l c" . (lambda () (interactive)
                 (let ((browse-url-browser-function 'osx-browse-url-chrome))
                   (link-hint-open-link))))
  ("C-c l w" . link-hint-copy-link))

(provide 'dc-search)

;;; dc-search.el ends here
