;;; dc-cleanup.el --- cleanup utilities -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;; -------------------- straight cleanup ------------------------
;; prune the build cache for straight.el; this will prevent it from
;; growing too large. do this after the final hook3 to prevent packages
;; installed there from being pruned.
(straight-prune-build-cache)

;; occasionally prune the build directory as well. for similar reasons
;; as above, we need to do this after local configuration.
(unless (bound-and-true-p radian--currently-profiling-p)
  (when (= 0 (random 100))
    (straight-prune-build-directory)))

;; enable color theme as late as is humanly possible. this reduces
;; frame flashing and other artifacts during startup.
(when color-theme-enable (dc/apply-theme))
(when dc/transparency (transparency dc/transparency-value))

(provide 'dc-cleanup)

;;; dc-cleanup.el ends here
