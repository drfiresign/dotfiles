;;; dc-snippets.el --- text snippets and templates -*- lexical-binding: t -*-

;;; Commentary:

;; commentary

;;; Code:

(use-package tempel :straight (:host github :repo "minad/tempel")
  :bind (("M-+" . tempel-complete)  ;; alternative tempel-expand
         ("M-*" . tempel-insert))
  :config
  ;; taken from the readme
  (defun tempel-include (elt)
    (when (eq (car-safe elt) 'inc)
      (if-let (template (alist-get (cadr elt) (tempel--templates)))
          (cons 'l template)
        (message "Template %s not found" (cadr elt))
        nil)))
  (add-to-list 'tempel-user-elements #'tempel-include)
  :init
  (defun tempel-setup-capf ()
    ;; Add the Tempel Capf to `completion-at-point-functions'.
    ;; `tempel-expand' only triggers on exact matches. Alternatively use
    ;; `tempel-complete' if you want to see all matches, but then you
    ;; should also configure `tempel-trigger-prefix', such that Tempel
    ;; does not trigger too often when you don't expect it. NOTE: We add
    ;; `tempel-expand' *before* the main programming mode Capf, such
    ;; that it will be tried first.
    (setq-local completion-at-point-functions
                (cons #'tempel-expand
                      completion-at-point-functions)))
  (global-tempel-abbrev-mode)
  :hook
  ((text-mode . tempel-setup-capf)
   (prog-mode . tempel-setup-capf)))

(provide 'dc-snippets)

;;; dc-snippets.el ends here
