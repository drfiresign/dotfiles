;;; dc-regular-m-x.el --- non-fancy M-x -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Nothing really fancy here

;;; Code:

(use-package smex
  :bind
  ("M-x" . smex)
  ("M-X" . smex-major-mode-commands)
  ("C-c C-c M-x" . execute-extended-command)
  :config
  (smex-initialize))

(use-feature fido-mode)
(use-feature ido
  :init
  ;; (use-package ido-complete-space-or-hyphen
  ;;   :straight (:host github :repo "doitian/ido-complete-space-or-hyphen")
  ;;   :demand t
  ;;   :config (ido-complete-space-or-hyphen-mode t))
  :custom ((ido-use-faces nil)
           (ido-everywhere t)
           (ido-enable-flex-matching t)
           (ido-use-filename-at-point 'guess)
           (ido-create-new-buffer 'always)
           (ido-ignore-buffers '("\\` " "magit:" "\*straight-process" "\*Compile-Log" "\*help" "\*\(ido \)?completions"))
           (ido-ignore-extensions t))
  :bind
  (:map ido-common-completion-map
        ("C-n" . ido-next-match)
        ("C-p" . ido-prev-match))
  )
(use-package flx-ido
  :init
  (ido-mode 1)
  (ido-everywhere 1)
  (flx-ido-mode 1)
  (setq ido-enable-flex-matching t
        ido-use-faces nil))

(if (require 'vcomplete nil 'noerror)
    (vcomplete-mode))

(provide 'dc-regular-m-x)

;;; dc-regular-m-x.el ends here
