;;; dc-go.el --- go-mode config -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;;; Commentary:

;; go-mode. to go places!

;;; Code:
(use-package go-mode
  :after flycheck
  :straight (:host github :repo "dominikh/go-mode.el")
  :custom (gofmt-command "goimports")
  :hook (before-save . gofmt-before-save)
  :bind ("C-M-?" . godoc-at-point)
  :init
  (dolist (checker '(go-vet
		     go-build
		     go-test
		     go-errcheck
		     go-unconvert
		     go-staticcheck
		     go-gocritic))
    (flycheck-add-mode checker 'go-mode)))

(use-feature go-ts-mode
  :after flycheck
  :custom
  (go-ts-mode-indent-offset 4)
  (gofmt-command "goimports")
  :init
  (add-to-list 'major-mode-remap-alist '(go-ts-mode . go-mode))
  (dolist (checker '(go-vet
		     go-build
		     go-test
		     go-errcheck
		     go-unconvert
		     go-staticcheck
		     go-gocritic))
    (flycheck-add-mode checker 'go-ts-mode))
  :hook (before-save . gofmt-before-save))

;; (use-package go-add-tags
;;   :straight (:host github :repo "emacsorphanage/go-add-tags")
;;   :bind (:map go-mode-map ("C-c t" . go-add-tags))
;;   :custom (go-add-tags-style . 'lower-camel-case))

(provide 'dc-go)

;;; dc-go.el ends here
