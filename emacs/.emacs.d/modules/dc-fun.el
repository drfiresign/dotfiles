;;; dc-fun.el --- Fun distractions -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

(use-package oblique
  :straight (:host github :repo "zzkt/oblique-strategies" :branch "endless" :files ("oblique.el" "strategies/*"))
  :custom (oblique-edition "oblique-strategies-condensed.txt"))

(provide 'dc-fun)

;;; dc-fun.el ends here
