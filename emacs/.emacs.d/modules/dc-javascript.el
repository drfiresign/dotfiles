;;; dc-javascript.el --- javascript, web stuff -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;;; web-mode
(use-package web-mode)

(defun dc/web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2))

(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.rasi?\\'" . web-mode))

(add-hook 'web-mode-hook  'dc/web-mode-hook)

;;; javascript

;; Package `js2-mode' provides a better major mode for JavaScript. It
;; builds on the major mode `js-mode' which comes with Emacs.
(use-package js2-mode
  ;; The `js2-mode' package does not come with `auto-mode-alist' autoloads.
  :mode (("\\.js\\'" . js2-mode)
         ("\\.jsx\\'" . js2-jsx-mode))
  :interpreter ("node" . js2-mode)
  :custom ((js2-skip-preprocessor-directives t) ;; Treat shebang lines (e.g. for node) correctly.
           (js2-strict-missing-semi-warning nil) ;; disable semicolon warnings
           (js-indent-level 2))
  :blackout ((js2-mode . "JavaScript")
             (js2-jsx-mode . "JSX")))

(use-package js2-refactor
  :config
  (js2r-add-keybindings-with-prefix "C-c C-r")
  (add-hook 'js-mode-hook 'js2-refactor-mode))

;;; TypeScript
;; https://www.typescriptlang.org/

(use-package typescript-mode
  :custom
  (typescript-indent-level 2))

;; vue.js
(use-package vue-mode
  :custom
  (mmm-submode-decoration-level 1))

(provide 'dc-javascript)

;;; dc-javascript.el ends here
