;;; dc-fonts.el --- fonts -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; do some fonts! here we create functions for selecting and updating fonts and
;; font sizes. this also selects the `dc-config-defaults' chosen font at startup.

;;; Code:

;; (define-key help-map (kbd "C-f") 'dc/font-switch)
(global-set-key (kbd "<f9>") 'dc/font-switch)

(defvar dc/raw-fonts

  `((,(intern "0xProto") "0xProto Nerd Font Mono" nil "0xProto Nerd Font Propo")
    (,(intern "3270") "3270 Nerd Font Mono" nil "3270 Nerd Font Propo")
    (agave "agave Nerd Font Mono" nil "agave Nerd Font Mono Propo")
    (anonymice-pro "AnonymicePro Nerd Font Mono" nil "AnonymicePro Nerd Font Propo")
    (aurulent-sans-m "AurulentSansM Nerd Font Mono" nil "AurulentSansM Nerd Font Propo")
    (b612 nil "B612 Mono" "B612")
    (big-blue-term "BigBlueTerm437 Nerd Font Mono" nil "BigBlueTerm437 Nerd Font Propo")
    (big-blue-term-plus "BigBlueTermPlus Nerd Font Mono" nil "BigBlueTermPlus Nerd Font Propo")
    (bitstrom-wera "BitstromWera Nerd Font Mono" nil "BitstromWera Nerd Font Propo")
    (blex "BlexMono Nerd Font" "IBM Plex Mono" "BlexMono Nerd Font Propo")
    (caskaydia "CaskaydiaCove Nerd Font Mono" nil "CaskaydiaCove Nerd Font Propo")
    (code-new-roman "CodeNewRoman Nerd Font Mono" nil "CodeNewRoman Nerd Font Propo")
    (comic-shanns "ComicShannsMono Nerd Font Mono" nil "ComicShannsMono Nerd Font Propo")
    (commit "CommitMono Nerd Font Mono" nil "CommitMono Nerd Font Propo")
    (cousine "Cousine Nerd Font Mono" nil "Cousine Nerd Font Propo")
    (d2-coding-ligature "D2CodingLigature Nerd Font Mono" nil "D2CodingLigature Nerd Font Propo")
    (daddy-time "DaddyTimeMono Nerd Font Mono" nil "DaddyTimeMono Nerd Font Propo")
    (deja-vu-sans "DejaVuSansM Nerd Font Mono" nil "DejaVuSansM Nerd Font Propo")
    ;; (departure "DepartureMono Nerd Font Mono" nil "DepartureMono Nerd Font Propo") ; this isn't in the nix package?
    (droid-sans "DroidSansM Nerd Font Mono" nil "DroidSansM Nerd Font Propo")
    (envy "EnvyCodeR Nerd Font Mono" "Envy Code R" "EnvyCodeR Nerd Font Propo")
    (fantasque "FantasqueSansM Nerd Font" nil "FantasqueSansM Nerd Font Propo")
    (fira-code "FiraCode Nerd Font" nil "FiraCode Nerd Font Propo")
    (fira-mono "FiraMono Nerd Font" nil "FiraMono Nerd Font Propo")
    (geist "GeistMono Nerd Font Mono" nil "GeistMono Nerd Font Propo")
    (go-mono "GoMono Nerd Font" nil "GoMono Nerd Font Propo")
    (gohu-uni11-mono "GohuFont uni11 Nerd Font" nil "GohuFont uni11 Nerd Font Propo")
    (gohu-uni14-mono "GohuFont uni14 Nerd Font" nil "GohuFont uni14 Nerd Font Propo")
    (gohu11-mono "GohuFont 11 Nerd Font" nil "GohuFont 11 Nerd Font Propo")
    (gohu14-mono "GohuFont 14 Nerd Font" nil "GohuFont 14 Nerd Font Propo")
    (hack "Hack Nerd Font" "Hack" "Hack Nerd Font Propo")
    (hasklug "Hasklug Nerd Font" nil "Hasklug Nerd Font Propo")
    (hurmit "Hurmit Nerd Font Mono" nil "Hurmit Nerd Font Propo")
    (im-writing "IMWritingMono Nerd Font" nil "IMWritingQuat Nerd Font Propo")
    (inconsolata "Inconsolata Nerd Font Mono" nil "Inconsolata Nerd Font Propo")
    (inconsolata-go "InconsolataGo Nerd Font" nil "InconsolataGo Nerd Font Propo") ; inconsolata w/ cyrllic
    (input nil "Input Mono" "Input Sans")
    (input-compressed nil "Input Mono Compressed" "Input Sans Compressed")
    (input-narrow nil "Input Mono Narrow" "Input Sans Narrow")
    (intone "IntoneMono Nerd Font Mono" nil "IntoneMono Nerd Font Propo")
    (iosevka "Iosevka Nerd Font" nil "Iosevka Nerd Font Propo")
    (iosevka-comfy nil "Iosevka Comfy" "Iosevka Comfy Duo")
    (iosevka-comfy-motion nil "Iosevka Comfy Motion" "Iosevka Comfy Motion Duo")
    (iosevka-term "IosevkaTerm Nerd Font Mono" nil "IosevkaTerm Nerd Font Propo")
    (iosevka-term-slab "IosevkaTermSlab Nerd Font Mono" nil "IosevkaTermSlab Nerd Font Propo")
    (jetbrains "JetBrainsMono Nerd Font" "JetBrains Mono" "JetBrainsMono Nerd Font Propo")
    (jetbrains-no-ligature "JetBrainsMonoNL Nerd Font" "JetBrains Mono" "JetBrainsMonoNL Nerd Font Propo")
    (latin-modern nil "Latin Modern Mono" "Latin Modern Mono Prop")
    (latin-modern-light nil "Latin Modern Mono Light" "Latin Modern Mono Prop Light")
    (latin-modern-caps nil "Latin Modern Mono Caps" "Latin Modern Mono Prop Caps")
    (latin-modern-light-condensed nil "Latin Modern Mono Light Cond" nil)
    (lekton "Lekton Nerd Font" nil "Lekton Nerd Font Propo")
    (literation "LiterationMono Nerd Font" nil "LiterationSans Nerd Font Propo")
    (lilex "Lilex Nerd Font Mono" nil "Lilex Nerd Font Propo")
    (martian "MartianMono Nerd Font Mono" nil "MartianMono Nerd Font Propo")
    (meslo-line-gap-large "MesloLGL Nerd Font Mono" nil "MesloLGLDZ Nerd Font Propo")
    (meslo-line-gap-large-dot-zero "MesloLGLDZ Nerd Font Mono" nil "MesloLGLDZ Nerd Font Propo")
    (meslo-line-gap-medium "MesloLGM Nerd Font Mono" nil "MesloLGM Nerd Font Propo")
    (meslo-line-gap-medium-dot-zero "MesloLGMDZ Nerd Font Mono" nil "MesloLGMDZ Nerd Font Propo")
    (meslo-line-gap-small "MesloLGS Nerd Font Mono" nil "MesloLGS Nerd Font Propo")
    (meslo-line-gap-small-dot-zero "MesloLGSDZ Nerd Font Mono" nil "MesloLGSDZ Nerd Font Propo")
    (monaspace-argon "MonaspiceAr Nerd Font Mono" nil "MonaspiceAr Nerd Font Propo")
    (monaspace-krypton "MonaspiceKr Nerd Font Mono" nil "MonaspiceKr Nerd Font Propo")
    (monaspace-neon "MonaspiceNe Nerd Font Mono" nil "MonaspiceNe Nerd Font Propo")
    (monaspace-radon "MonaspiceRn Nerd Font Mono" nil "MonaspiceRn Nerd Font Propo")
    (monaspace-xenon "MonaspiceXe Nerd Font Mono" nil "MonaspiceXe Nerd Font Propo")
    (monofur "Monofur Nerd Font" nil "Monofur Nerd Font Propo")
    (monoid "Monoid Nerd Font" nil "Monoid Nerd Font Propo")
    (monoisome "Monoisome" "Monoisome" nil)
    (mononoki "mononoki Nerd Font Mono" "mononoki" "mononoki Nerd Font Propo")
    (noto-sans "NotoSansM NFM" "Noto Sans Mono" "NotoSansM NFP")
    (overpass "OverpassM Nerd Font Mono" nil "OverpassM Nerd Font Propo")
    (pro-font "ProFont IIx Nerd Font Mono" nil "ProFont IIx Nerd Font Propo")
    (pro-font-windows "ProFontWindows Nerd Font Mono" nil "ProFontWindows Nerd Font Propo")
    (proggy-clean "ProggyClean Nerd Font Mono" nil "ProggyClean Nerd Font Propo")
    (pt-mono nil "PT Mono" nil)
    (recursive-casual "RecMonoCasual Nerd Font Mono" nil "RecMonoCasual Nerd Font Propo")
    (recursive-duotone "RecMonoDuotone Nerd Font Mono" nil "RecMonoDuotone Nerd Font Propo")
    (recursive-linear "RecMonoLinear Nerd Font Mono" nil "RecMonoLinear Nerd Font Propo")
    (recursive-semi-casual "RecMonoSmCasual Nerd Font Mono" nil "RecMonoSmCasual Nerd Font Propo")
    (red-hat nil "Red Hat Mono" nil)
    (roboto "RobotoMono Nerd Font Mono" nil "RobotoMono Nerd Font Propo")
    (shure-tech "ShureTechMono Nerd Font Mono" nil "ShureTechMono Nerd Font Propo")
    (sauce-code-pro "SauceCodePro Nerd Font" nil "SauceCodePro Nerd Font Propo")
    (space "SpaceMono Nerd Font Mono" nil "SpaceMono Nerd Font Propo")
    (sf-mono "Liga SFMono Nerd Font" nil nil)
    (terminess "Terminess Nerd Font Mono" nil "Terminess Nerd Font Propo")
    (ubuntu "UbuntuMono Nerd Font Mono" nil "UbuntuMono Nerd Font Propo")
    (ubuntu-sans "UbuntuSansMono Nerd Font Mono" nil "UbuntuSansMono Nerd Font Propo")
    (unifont nil "Unifont" nil)
    (victor-mono "VictorMono Nerd Font Mono" "Victor Mono" "VictorMono Nerd Font Propo")
    (zed "ZedMono Nerd Font Mono" nil "ZedMono Nerd Font Propo")))

(defvar dc/current-font dc/default-font)
(defvar dc/font-fallback "Monoid Nerd Font")

(use-package ligature :straight (:host github :repo "mickeynp/ligature.el")
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                       "\\\\" "://"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))


(defun dc/find-font-set (name &optional set)
  (or (dc/search-set name (or set dc/raw-fonts))
      (dc/find-font-set dc/font-fallback)))

(setq dc/current-font-size dc/default-font-size
      dc/current-font dc/default-font
      dc/font-change-increment 1.1
      cursor-in-non-selected-windows 'nil
      ;; new frames use my font and size
      ;; default-frame-alist '((font . dc/current-font)
      ;;                       ;; (width . dc/init-win-w)
      ;;                       ;; (height . dc/init-win-h)
      ;;                       )
      )

(defun dc/font-code (&optional font size)
  "Return a string representing the current FONT with SIZE (like \"IBM Plex Mono-14\")."
  (let ((f (or font dc/current-font))
        (s (number-to-string (or size dc/current-font-size))))
    (concat f "-" s)))

(defun dc/set-font--config (&optional font size variable-font)
  "Set `dc/default-font' to FONT and `dc/current-font-size' to SIZE using VARIABLE-FONT.
Set that for the current frame, and also make it the default for other, future frames."
  (let* ((font-size (or size dc/current-font-size))
         (font-name (or font dc/current-font))
         (font-code (dc/font-code font-name font-size))
         (vari-font-code (if variable-font
                             (dc/font-code variable-font font-size)
                           font-code)))
    (setq dc/current-font font-name
          dc/current-font-size font-size)
    (add-to-list 'default-frame-alist `(font . ,font-code) nil 'dc/compare-frame-fn)
    (set-frame-font font-code t t t)
    (set-face-font 'variable-pitch vari-font-code)
    (set-alacritty-font-size font-size)))

(defun dc/get-font-condensed (font)
  (list (car font) (or (cadr font) (caddr font)) (or (cadddr font))))

(defun dc/font-switch (&optional font size variable-font)
  "Change current font-set."
  (interactive)
  (let* ((current-font-set (dc/find-font-set dc/default-font))
         (current-font-string (car current-font-set))
         (valid-fonts (-non-nil (cl-mapcar #'dc/get-font-condensed dc/raw-fonts)))
         (new-font (or font (completing-read "New font: "
                                             (cl-mapcar #'(lambda (x) (symbol-name (car x))) valid-fonts)
                                             nil nil nil nil current-font-string)))
         (new-size (or size (completing-read "New size: "
                                             (cl-mapcar #'number-to-string (number-sequence 8 20))
                                             nil 'confirm (number-to-string dc/current-font-size) nil
                                             (window-font-width))))
         (new-set (dc/find-font-set (intern new-font) valid-fonts)))
    (dc/set-font--config (cadr new-set) (string-to-number new-size) (caddr new-set))
    (redraw-display)))

(defun dc/reset-to-default-font ()
  "Reset to our default fonts at current size."
  (interactive)
  (let* ((font-name '"Iosevka Comfy Motion")
         (font-string (concat font-name "-" (number-to-string dc/current-font-size))))
    (set-frame-font `(font . ,font-string))
    (set-face-font 'variable-pitch (concat font-name " Duo"))))

(defun dc/reset-font-size ()
  "Change font size back to `dc/default-font-size'."
  (interactive)
  (setq dc/current-font-size dc/default-font-size)
  (if (equal dc/current-font "saxMono")
      (dc/set-font--config nil 13)
    (dc/set-font--config)))

(defun dc/increase-font-size ()
  "Increase the current font size by a factor of `dc/font-change-increment'."
  (interactive)
  (setq dc/current-font-size
        (ceiling (* dc/current-font-size dc/font-change-increment)))
  (dc/set-font--config))

(defun dc/decrease-font-size ()
  "Decrease the current font size by a factor of `dc/font-change-increment', down to a minimum size of 1."
  (interactive)
  (setq dc/current-font-size
        (max 1 (floor
                (/ dc/current-font-size
                   dc/font-change-increment))))
  (dc/set-font--config))

(dc/reset-font-size)

;; separate font for modeline
(defun dc/modeline-font (&optional f)
  "Set a separate FONT for the modeline."
  (interactive)
  (setq dc/modeline-changed-p t)
  (let* ((font (unless f
                 (completing-read "Font: " (fontset-list)))))
    (set-face-attribute 'mode-line nil :family font :height 1.0)
    (set-face-attribute 'mode-line-inactive nil :family font :height 1.0)))

(provide 'dc-fonts)

;;; dc-fonts.el ends here
