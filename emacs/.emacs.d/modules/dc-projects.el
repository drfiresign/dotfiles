;;; dc-projects.el --- project management -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Project management.

;;; Code:

(use-package editorconfig
  :disabled t
  :defer t
  :init
  (editorconfig-mode 1)
  :blackout t)

(use-package harpoon.el
  :straight (:host github :repo "otavioschwanck/harpoon.el")
  :custom
  (harpoon-cache-file "~/.local/harpoon/")
  :bind
  ("C-c h f" . harpoon-toggle-file)
  ("C-c h <return>" . harpoon-add-file)
  ("C-c h h" . harpoon-toggle-quick-menu)
  ("C-c h c" . harpoon-clear)
  ("C-c h 1" . harpoon-go-to-1)
  ("C-c h 2" . harpoon-go-to-2)
  ("C-c h 3" . harpoon-go-to-3)
  ("C-c h 4" . harpoon-go-to-4)
  ("C-c h 5" . harpoon-go-to-5)
  ("C-c h 6" . harpoon-go-to-6)
  ("C-c h 7" . harpoon-go-to-7)
  ("C-c h 8" . harpoon-go-to-8)
  ("C-c h 9" . harpoon-go-to-9))

(use-package projectile
  :defer t
  :custom
  ;; disable project.el keymap
  ;; (project-prefix-map nil)
  ;; (project-other-frame-map nil)
  ;; (project-other-window-map nil)
  (projectile-indexing-method 'alien)
  (projectile-sort-order 'recently-active)
  (projectile-completion-system 'auto)
  (completion-ignored-extensions '(".DS_Store" ".eln"))
  (projectile-switch-project-action #'projectile-dired)
  ;; (projectile-require-project-root 'prompt)
  (projectile-project-search-path '("~/code/" "~/code/work/"))
  (projectile-globally-ignored-file-suffixes '("~" "#"))
  (projectile-enable-caching t)
  (projectile-mode-line '(:eval (if (and (projectile-project-p)
                                         (not (file-remote-p default-directory)))
                                    (format " Projectile[%s]" (projectile-project-name)) "")))
  ;; IGNORE ME!
  (projectile-globally-ignored-files
   '(".DS_Store" "TAGS" "package-lock.json" ".~undo-tree~" ".projectile"
     ".ignore"
     ))
  (projectile-globally-ignored-directories
   '(".idea" ".ensime_cache" ".eunit" ".git" ".hg" ".fslckout"
     "_FOSSIL_" ".bzr" "_darcs" ".tox" ".svn" ".stack-work"
     "node_modules" "straight" ".doom.tmp" ".xkcd" "eln-cache"
     "emacs.d/thumbs" "elpy" ".cache" "vendor"))
  (projectile-globally-ignored-buffers
   '("Dired" "*[hH]elp[^*]+*" "\*[tT][aA][gG][sS]\*"))
  :bind
  ("C-c p" . projectile-command-map)
  ("s-p" . projectile-command-map)
  ("s-[" . projectile-previous-project-buffer)
  ("s-]" . projectile-next-project-buffer)
  ;; ("s-q" . projectile-switch-open-project)
  :init
  (projectile-mode +1)
  :config
  (let ((rg (executable-find "rg")))
    (when rg
      (progn
        (defconst dc/projectile-rg-arguments
          '("--line-number"                     ; line numbers
            "--smart-case"
            "--follow"                          ; follow symlinks
            "--mmap"                            ; apply memory map optimization when possible
            "--hidden"
            "--glob=\!.git"
            "--glob=\!org/org/")
          "Default rg arguments used in the functions in `projectile' package.")
        (defconst dc/projectile-rg-command
          (mapconcat 'identity
                     (append `(,rg) ; used unaliased version of `rg': \rg
                             '("--files" ; get file names matching the regex '' (all files)
                               "--null") ; output null separated results,
                             dc/projectile-rg-arguments
                             )
                     " ")
          "Command used by projectile to get the files in a project with ripgrep.")

        (defun dc/advice-projectile-use-rg (vcs)
          "Always use `rg' for getting a list of all files in the project."
          (pcase vcs
            ('git dc/projectile-rg-command)
            ('hg projectile-hg-command)
            ('fossil projectile-fossil-command)
            ('bzr projectile-bzr-command)
            ('darcs projectile-darcs-command)
            ('svn projectile-svn-command)
            (_ projectile-generic-command)))

        (advice-add 'projectile-get-ext-command :override #'dc/advice-projectile-use-rg))))
  ;; debugging the above:
  ;; (advice-remove 'projectile-get-ext-command #'dc/advice-projectile-use-rg)
  :blackout t)

;; --- ibuffer
(use-feature ibuffer
  ;; :bind
  ;; ("M-i" . ibuffer)
  ;; ("M-I" . ibuffer-bs-show)
  :custom
  (ibuffer-default-directory "~/code")
  (ibuffer-default-sorting-mode "not special")
  (ibuffer-never-show-predicates '("\*helm" "magit" "*help" "*Warnings"))
  (ibuffer-saved-filter-groups nil))

(use-package all-the-icons-ibuffer :straight (:host github :repo "seagle0128/all-the-icons-ibuffer")
  :init (all-the-icons-ibuffer-mode 1))

(use-package perspective-el :straight (:host github :repo "nex3/perspective-el")
  ;; :disabled t
  :bind
  ;; ("C-x x I" . persp-ibuffer)
  ("C-x B" . persp-ibuffer)
  ;; ("s-<return>" . persp-switch)
  ("C-x x o" . persp-switch-last)
  ("s-o" . persp-switch-last)
  ("s-s" . persp-switch)
  ("s-{" . persp-prev)
  ("s-}" . persp-next)
  ("C-M-S-]" . persp-prev)
  ("C-M-S-[" . persp-next)
  ;; ("s-K" . dc/kill-project-and-buffers)
  ;; :hook (kill-emacs . persp-state-save)
  :custom
  (persp-mode-prefix-key (kbd "C-x x"))
  (persp-state-default-file "~/.emacs.d/perspectives/last")
  (persp-show-modestring 'header)
  (persp-modestring-short nil)
  (persp-suppress-no-prefix-key-warning t)
  (persp-sort 'created)
  :init
  (persp-mode 1)
  (use-package persp-projectile
    :straight (:host github :repo "bbatsov/persp-projectile")
    :bind
    ("C-c p p" . projectile-persp-switch-project))
  (defun dc/kill-project-and-buffers ()
    "Kill current project and all associated buffers."
    (interactive)
    (cl-loop for buffer in (persp-buffers (persp-curr))
             do (kill-buffer buffer))
    (persp-kill (persp-name (persp-curr)))))

(use-package ztree-dif
  :straight (:host github :repo "fourier/ztree")
  :bind (:map ztreediff-mode-map
              ("n" . ztree-next-line)
              ("p" . ztree-previous-line)))

(use-package treemacs
  ;; :disabled t
  :bind ("<f5>" . dc/treemacs-toggle)
  ;; :custom (treemacs-is-never-other-window t)
  ;; :hook (treemacs-mode . treemacs-project-follow-mode)
  :config
  (defun dc/treemacs-toggle ()
    "Toggle treemacs buffer."
    (interactive)
    (if (eq major-mode 'treemacs-mode)
        (treemacs-quit)
      (treemacs))))
(use-package treemacs-magit
  :after (treemacs magit))
(use-package treemacs-projectile
  :after (treemacs projectile)
  :bind (:map treemacs-mode-map
              ("j" . treemacs-find-file))
  )
(use-package treemacs-persp
  :after (treemacs perspective-el))
(provide 'dc-projects)

;;; dc-projects.el ends here
