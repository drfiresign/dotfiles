;;; dc-ui.el --- user interface -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;; pretty symbols
(global-prettify-symbols-mode t)
;; (push '(("#+BEGIN_SRC"     . "λ")
;;         ("#+END_SRC"       . "λ")) prettify-symbols-alist)

;; blending color functions from:
;; https://oremacs.com/2015/04/28/blending-faces/
(defun colir-join (r g b)
  "Build a color from R G B.
Inverse of `color-values'."
  (format "#%02x%02x%02x"
          (ash r -8)
          (ash g -8)
          (ash b -8)))

(defun colir-blend (c1 c2 &optional ALPHA)
  "Blend the two colors C1 and C2 with ALPHA.
C1 and C2 are in the format of `color-values'.
ALPHA is a number between 0.0 and 1.0 which corresponds to the
influence of C1 on the result."
  (setq alpha (or ALPHA 0.5))
  (apply #'colir-join
         (cl-mapcar
          (lambda (x y)
            (round (+ (* x alpha) (* y (- 1 alpha)))))
          c1 c2)))

;; (defun highlight-symbol-update-hook ()
;;   "Update the background and foreground colors for highlight-symbol mode."
;;   (set-face-attribute 'highlight-symbol-face nil
;;                       :background (foreground-color-at-point)
;;                       :foreground (background-color-at-point)
;;                       ))

;; ;;; stolen from http://xenodium.com/emacs-highlight-symbol-mode/
;; (use-package highlight-symbol
;;   :blackout t
;;   :custom ((highlight-symbol-idle-delay nil)
;;            (highlight-symbol-on-navigation-p t))
;;   :hook (((org-mode prog-mode) . highlight-symbol-mode)
;;          ((org-mode prog-mode yaml-mode) .  highlight-symbol-nav-mode)))

(use-package lin
  :straight (:host github :repo "protesilaos/lin")
  :init
  (setq lin-face 'lin-green)
  :config
  (lin-global-mode 1))

;; delimiter and paren colors
(use-package rainbow-delimiters
  :init (rainbow-delimiters-mode)
  :hook (prog-mode . rainbow-delimiters-mode)
  :blackout t)

(use-package rainbow-mode
  :straight (:host github :repo "emacsmirror/rainbow-mode")
  :hook ((fundamental-mode toml-ts-mode) . rainbow-mode)
  :blackout t)

(use-package nerd-icons
  :ensure t)
(use-package nerd-icons-completion
  :ensure t
  :after marginalia
  :config
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))
(use-package nerd-icons-corfu
  :ensure t
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))
(use-package nerd-icons-dired
  :ensure t
  :hook
  (dired-mode . nerd-icons-dired-mode))

(use-package all-the-icons)
(use-package all-the-icons-completion
  :disabled t
  :straight (:host github :repo "iyefrat/all-the-icons-completion")
  :hook
  (marginalia-mode . all-the-icons-completion-marginalia-setup)
  :init
  ;; (add-hook 'marginalia-mode-hook #'all-the-icons-completion-marginalia-setup)
  (all-the-icons-completion-mode)
  :custom
  (all-the-icons-scale-factor 1))

;; (global-hl-line-mode +1)
;; fringe for icons
(fringe-mode '(10 . 10))

(provide 'dc-ui)

;;; dc-ui.el ends here
