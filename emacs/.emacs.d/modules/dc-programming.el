;;; dc-programming.el --- Programming mode settings -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Features providing "IDE-like" behavior such as help menus, documentation
;; browsers, snippets, etc.

;;; Code:

(use-package reformatter
  :straight (:host github :repo "purcell/emacs-reformatter"))

(use-package treesit-auto
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(use-package flycheck-rust :straight (:host github :repo "flycheck/flycheck-rust"))
(use-package flycheck
  :blackout t
  :custom (flycheck-indication-mode 'left-fringe)
  :hook (after-init . global-flycheck-mode)
  :bind
  ("C-x `" . flycheck-next-error)
  ("C-x ~" . flycheck-previous-error)
  :config
  (flycheck-define-checker go-gocritic
    "A Go style checker using gocritic.

See URL: `https://go-critic.com/'"
    :command ("gocritic" "check" source)
    :error-patterns
    ((error line-start (file-name) ":" line ":" column ":" (message) line-end))
    :modes (go-mode go-ts-mode)
    :next-checkers ((warning . go-build)))
  (unless (member 'rustic-clippy flycheck-checkers)
    (push 'rustic-clippy flycheck-checkers))
  (unless (member 'go-gocritic flycheck-checkers)
    (push 'go-gocritic flycheck-checkers)))


(use-package expand-region
  ;; :bind
  ;; (("C-+" . er/expand-region)
  ;;  ("C-_" . er/contract-region))
  )

(use-package yafolding
  :disabled t
  ;; :demand t
  :init (add-hook 'prog-mode-hook 'yafolding-mode)
  :blackout t)

;; (use-package dap-mode :blackout t
;;   :defer t
;;   :straight (:host github :repo "emacs-lsp/dap-mode"))

(require 'dc-eglot)
;; (require 'dc-lsp)

(use-package direnv
  :custom
  (direnv-show-paths-in-summary nil)
  (direnv-always-show-summary nil)
  :config
  (direnv-mode))

(use-package hl-todo
  :straight (:host github :repo "tarsius/hl-todo")
  :init
  (global-hl-todo-mode 1))

(provide 'dc-programming)

;;; dc-programming.el ends here
