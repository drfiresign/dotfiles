;;; dc-smartparens.el --- Smartparens configuration -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;; Version: version
;; Package-Requires: (dependencies)
;; Homepage: homepage
;; Keywords: keywords



;;; Commentary:

;; Maybe transitioning away from this, but it's long enough to warrant a
;; separate file.

;;; Code:

(use-package smartparens
  :straight (:host github :repo "Fuco1/smartparens")
  :defer t
  :config
  (require 'smartparens-config)
  (smartparens-global-mode)
  (show-smartparens-global-mode)
  (add-to-list 'sp-ignore-modes-list #'org-mode)
  (add-to-list 'sp-ignore-modes-list #'org-agenda-mode)
  (add-to-list 'sp-ignore-modes-list #'vterm-mode)
  (add-hook 'prog-mode-hook #'turn-on-smartparens-strict-mode)
  (setq show-paren-delay 0)

  (defun dc/sp-select-mark (direction arg)
    "Documentation ARG."
    (let ((next (eq direction 'next))
          (prev (eq direction 'prev)))
      (cond
       (next (if arg (sp-select-next-thing-exchange)
               (sp-select-next-thing)))
       (prev (if arg (sp-select-previous-thing)
               (sp-select-previous-thing-exchange))))))

  (defun dc/sp-select-prev (&optional arg)
    (interactive "P")
    (dc/sp-select-mark 'prev arg))
  (defun dc/sp-select-next (&optional arg)
    (interactive "P")
    (dc/sp-select-mark 'next arg))

  :custom
  (sp-hybrid-kill-excessive-whitespace t)
  :bind
  ("C-x x s" . smartparens-strict-mode)
  (:map smartparens-mode-map
        ("C-M-f" . sp-forward-sexp)
        ("C-M-b" . sp-backward-sexp)

        ("C-M-n" . sp-next-sexp)
        ("C-M-p" . sp-previous-sexp)

        ("C-M-u" . sp-down-sexp)
        ("C-M-d" . sp-up-sexp)

        ("C-S-u" . sp-backward-down-sexp)
        ("C-S-d" . sp-backward-up-sexp)

        ("C-M-a" . sp-beginning-of-sexp)
        ("C-M-e" . sp-end-of-sexp)

        ("C-S-f" . sp-forward-symbol)

        ("C-x C-." . dc/sp-select-next)
        ("C-x C-," . dc/sp-select-prev)

        ("C-k" . sp-kill-sexp)
        ("M-k"   . sp-backward-kill-sexp)
        ("C-M-k"   . sp-kill-hybrid-sexp)
        ("C-M-w" . sp-copy-sexp)
        ("C-M-y" . sp-delete-symbol)

        ("C-<backspace>" . backward-kill-word)
        ("M-<backspace>" . sp-backward-kill-word)
        ("C-M-<backspace>" . sp-kill-whole-line)
        ([remap kill-region] . sp-kill-region)
        ("C-x C-t" . sp-transpose-hybrid-sexp)
        ("C-M-t" . sp-transpose-sexp)

        ("C-<right>" . sp-forward-slurp-sexp)
        ("M-<right>" . sp-forward-barf-sexp)
        ("C-<left>"  . sp-backward-slurp-sexp)
        ("M-<left>"  . sp-backward-barf-sexp)
        ("C-M-[" . sp-backward-unwrap-sexp)
        ("C-M-]" . sp-unwrap-sexp))
  :blackout t)

(provide 'dc-smartparens)

;;; dc-smartparens.el ends here
