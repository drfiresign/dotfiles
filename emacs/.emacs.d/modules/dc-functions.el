;;; dc-functions.el --- various utility functions -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

(require 'dash)

(defun delete-horizontal-space-but-one ()
  "Delete all but a single space on a line."
  (interactive)
  (delete-horizontal-space)
  (insert " "))

(defun dc/replace-non-breaking-space ()
  "Replace all non-breaking space characters in buffer with regular space characters."
  (interactive)
  (save-excursion
    (point-min)
    (ignore-errors
      (while (search-forward "\x00A0")
        (replace-match "\x0020")))))

;; config shortcut
(defun dc/visit-emacs-config ()
  "Quickly visit your Emacs configuration file."
  (interactive)
  (find-file "~/.emacs.d/init.el"))


(defun dc/kill-current-buffer ()
  "Kill the current buffer the point is active in.
Switch to the previous buffer in replacement."
  (interactive)
  (kill-buffer (current-buffer)))

(defun dc/other-buffer (arg)
  "With one window open, switch to last buffer.
With more than one, cycle around buffers."
  (interactive "p")
  (if (= 1 (count-windows))
      (switch-to-buffer (other-buffer))
    (other-window arg)))

(defun dc/other-other-buffer (arg)
  "With one window open, switch to last buffer.
With more than one, cycle around buffers."
  (interactive "p")
  (other-window -1))

(defun dc/random-choice (items)
  "Return random choice from list of ITEMS."
  (let* ((size (length items))
         (index (random size)))
    (nth index items)))

(defun dc/wrap-quotes (string)
  "Wrap a given STRING with quotes."
  (concat "\"" string "\""))

(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

;; TODO: open with eww, using this page: https://github.com/emacsmirror/emacswiki.org/blob/fe96edb732a7ae18bb42baaaac077d0f9ea1b310/oneonone.el#L1622
;;       and figure out how to extract this read only cursor mode from oneonone.el
;;       I don't want to actually use oneonone because it removes windows in favor of frames, and I love windows.

;; (defun dc/update-cursor ()
;;   "Update the cursor shape to indicate you're viewing a read-only buffer."
;;   (when ()))

(defun transparency (value)
  "Set the transparency of the frame window to VALUE.  0=transparent/100=opaque."
  (interactive "nTransparency Value 0 - 100 opaque:")
  (set-frame-parameter (selected-frame) 'alpha value))

(defun dc/toggle-transparency ()
  "Toggle transparency between 100% and 60% with <C-0>."
  (interactive)
  (let* ((current-alpha (frame-parameter (selected-frame) 'alpha)))
    (if (eq current-alpha (or 100 nil))
        (transparency dc/transparency-value)
      (transparency 100))))

(defun dc/kill-advice (old-fun beg end &rest args)
  "Advice to use pulse.el and highlight the kill-ring-save'd region."
  ;; Taken from https://blog.meain.io/2020/emacs-highlight-yanked/
  (apply old-fun beg end args)
  (pulse-momentary-highlight-region beg end))

(advice-add 'kill-ring-save :around #'dc/kill-advice)

(defun dc/previous-window ()
  "Move back one window"
  (interactive)
  (other-window -1))


(defun dc/insert-signed-comment (&optional extra arg)
  "Insert signed comment with optional EXTRA text."
  (comment-dwim arg)
  (-let* ((whoami (user-login-name))
          (dt (format-time-string "%Y-%m-%d" (current-time)))
          (also (or extra ""))
          (sig (format "%s[%s|%s] " also whoami dt)))
    (insert sig)
    (end-of-line)))

(defmacro dc/create-insert-signed-comments (name keyword keybind)
  "Create function `dc/insert-signed-NAME' to insert KEYWORD as a comment with KEYBIND."
  ;; make keyword highlighted in page.
  (declare (indent 1))
  `(global-set-key
    (kbd ,keybind)
    (defun ,(intern (concat "dc/insert-signed-" (downcase name))) (&optional arg)
      ,(concat "Insert " (upcase name) " comment.")
      (interactive "*P")
      (dc/insert-signed-comment ,(concat (upcase keyword)) arg))))

(defun project-root (project)
  (car (project-roots project)))

;; via: https://masteringemacs.org/article/fixing-mark-commands-transient-mark-mode
(defun push-mark-no-activate ()
  "Pushes `point' to `mark-ring' and does not activate the region
   Equivalent to \\[set-mark-command] when \\[transient-mark-mode] is disabled"
  (interactive)
  (push-mark (point) t nil)
  (message "Pushed mark to ring"))
(defun exchange-point-and-mark-no-activate ()
  "Identical to \\[exchange-point-and-mark] but will not activate the region."
  (interactive)
  (exchange-point-and-mark)
  (deactivate-mark nil))

;; (global-set-key (kbd "C-`") 'push-mark-no-activate)
;; (global-set-key (kbd "M-`") 'exchange-point-and-mark-no-activate)

(defun file-name-at-point (insert-in-buffer)
  "Prompt the user for a file path using `find-file' completion and
insert the absolute path to the chosen file.

With ADD-TO-KILL-RING, copy the filename to the kill-ring."
  (interactive "P")
  (let ((action (if insert-in-buffer 'insert 'kill-new))
        (path (expand-file-name (read-file-name "file path: "))))
    (apply action (list path))))

(global-set-key (kbd "C-c f") 'file-name-at-point)

;; rebalance windows, credit hrs
(defun dc/split-window-below-and-switch ()
  "Split the window horizontally, then switch to the new pane."
  (interactive)
  (split-window-below)
  (other-window 1))

(defun dc/split-window-right-and-switch ()
  "Split the window vertically, then switch to the new pane."
  (interactive)
  (split-window-right)
  (other-window 1))

(global-set-key (kbd "C-x 2") 'dc/split-window-below-and-switch)
(global-set-key (kbd "C-x 3") 'dc/split-window-right-and-switch)

(defun dc/dired-view-file-other-window ()
  "In Dired, examine a file in view mode, returning to Dired when done.
When file is a directory, show it in this buffer if it is inserted.
Otherwise, display it in another buffer."
  (interactive)
  (let ((file (dired-get-file-for-visit)))
    (if (file-directory-p file)
        (or (and (cdr dired-subdir-alist)
                 (dired-goto-subdir file))
            (dired file))
      (view-file-other-window file))))

(defun dc/restow ()
  "Run the command to restow the contents of the current dotfile directory.
Only useful when viewing a shell file in my dotfiles repo."
  (interactive)
  (if (string-equal (projectile-project-root)
                    (concat dc/user-home "dotfiles/"))
      (cl-flet ((match-dir (dir root-dir)
                  (cadr (s-match (s-join "/" (list dir "\\([^/]+\\)/"))
                                 root-dir))))
        (let* ((proj-root (projectile-project-root))
               (cmd-root default-directory)
               (stow-proj (match-dir "dotfiles" cmd-root)
                          ;; (cadr (s-match "dotfiles/\\([^/]+\\)/" cmd-root))
                          )
               (default-directory proj-root))
          (if (string-equal stow-proj "errata") ;; custom-scripts are stored elsewhere
              (let* ((stow-nested-proj (match-dir "errata" cmd-root))
                     (default-directory (concat proj-root "errata")))
                (message "%s; %s" stow-nested-proj default-directory)
                (call-process "stow" nil
                              (get-buffer-create "*err*")
                              nil "-R" stow-nested-proj "-t"
                              (concat dc/user-home "bin"))
                )
            (call-process "stow" nil 0 nil "-R" stow-proj))))
    (error "This command can only be used in the dotfiles repo")))

(defun user-bin-p (bin)
  "Check for BIN in `/usr/bin/'."
  (let ((user-bin (concat "/usr/bin/" bin))
        (bin-bin (concat "/bin/" bin)))
    (cond
     ((file-exists-p user-bin) user-bin)
     ((file-exists-p bin-bin) bin-bin))))


;; TODO[dcascio|2022-02-14] This behaves oddly. Window opens, doesn't connect to
;; yabai, won't close on disconnect or `exit' command.

;; (defun dc/open-alacritty ()
;;   "Open a new alacritty instance in the current working directory."
;;   (call-process "/usr/local/bin/alacritty" nil 0 nil
;;                 "--working-directory" default-directory))

;; (defun dc/gh-visit-file ()
;;   "Visit the current file and line in the browser at Github.
;; Useful for sharing a particular line with a coworker."
;;   (interactive)
;;   (if (string-equal (projectile-project-root)
;;                     (concat dc/user-home "code/work/")))
;;   )

(defun make-display-buffer-matcher-function (major-modes)
  (lambda (buffer-name action)
    (with-current-buffer buffer-name (apply #'derived-mode-p major-modes))))

(defun dc/switch-window-split ()
  "Toggle two windows to be vertically split if horizontal and vice versa."
  (interactive)
  (unless (= (count-windows) 2) (error "Can only toggle a split with two windows."))
  (let ((split-vertically-p (window-combined-p)))
    (delete-window)
    (if split-vertically-p
        (split-window-horizontally)
      (split-window-vertically))
    (switch-to-buffer nil)))

(defun dc/override-keybinding-in-buffer (key command)
  "Override KEY with COMMAND in buffer."
  (interactive "KSet key buffer-locally: \nCSet key %s buffer-locally to command: ")
  (let ((oldmap (current-local-map))
        (newmap (make-sparse-keymap)))
    (when oldmap
      (set-keymap-parent newmap oldmap))
    (define-key newmap key command)
    (use-local-map newmap)))

(defun read-temp-markdown-buffer (&optional prompt init-text emacsclient-p frame-handle)
  "Create a temporary markdown-mode buffer with initial PROMPT.

If INIT-TEXT if it is not nil, it is __decoded from base64__.
This makes passing in arbitrary text strings easier from a
terminal.

If EMACSCLIENT-P is not nil, this function assumes a new frame
named FRAME-HANDLE was created. Accordingly, it will be deleted
when the session is complete.

\\<org-capture-mode-map>
Use \\[org-capture-finalize] to close buffer and save contents to `kill-ring'.
Use \\[org-capture-kill] to kill buffer and discard contents."
  (interactive)
  (let ((current-buffer (current-buffer))
        (temp-buffer (get-buffer-create "*temp-markdown-buffer*"))
        (help-text (format "<!--- %s -->\n\n" (concat "Press C-c C-c to continue, "
                                                      "C-c C-k to cancel"))))
    (pop-to-buffer temp-buffer)
    (when emacsclient-p (select-frame-by-name frame-handle))
    (erase-buffer)
    (markdown-mode)
    (insert (or prompt help-text))
    (goto-char (point-max))
    (defvar-local min-point (point)
      "Local var for min point. Ensures given prompt is not returned,
regardless of length.")
    (when init-text (insert (base64-decode-string init-text)))
    (dc/override-keybinding-in-buffer
     (kbd "C-c C-c")
     `(lambda ()
        (interactive)
        (copy-region-as-kill ,min-point (point-max))
        (if ,emacsclient-p (delete-frame)
          (switch-to-buffer ,current-buffer))
        (kill-buffer ,temp-buffer)))
    (dc/override-keybinding-in-buffer
     (kbd "C-c C-k")
     (lambda ()
       (interactive)
       (if emacsclient-p (delete-frame))
       (kill-buffer temp-buffer)))))

(defun slack-markdown-buffer (&optional text)
  "Open a markdown frame to edit markdown TEXT."
  (interactive)
  (condition-case nil
      (progn
        (read-temp-markdown-buffer nil text t))
    (error (delete-frame))
    (quit (delete-frame))))

(defun slack-markdown-buffer-emacsclient (&optional text)
  "Open an `emacsclient' frame to edit TEXT in a markdown buffer.

This function is designed to be opened from a terminal through
`emacsclient'. The invocation of `emacsclient' should specify
args `--create-frame' and `--frame-parameters' to set the `name'
parameter to `*slack-msg*'. For more frame parameters see Info
node `Window Frame Parameters'."
  (condition-case nil
      (read-temp-markdown-buffer nil text t "*slack-msg*")
    (quit
     (kill-buffer)
     (delete-frame))))

(defun dc/file-to-github-url ()
  "Translate a given file path to a github.com URL."
  (interactive)
  (let* ((file-path (file-name-split (buffer-file-name)))
         (file-path-components (member "github.com" file-path)))
    (if file-path-components
        (let* ((repo-org (cadr file-path-components))
               (repo-name (caddr file-path-components))
               (file-in-repo (apply #'f-join (cdr (member repo-name file-path-components))))
               (start-line-number (line-number-at-pos (if (region-active-p) (region-beginning) (point))))
               (end-r (region-end))
               (end-line-number (line-number-at-pos
                                 (if (region-active-p)
                                     (- (region-end) (if (= (region-end) (line-beginning-position))
                                                         1 0))
                                   (point))))
               (branch-name (if (boundp 'magit-get-current-branch)
                                (magit-get-current-branch)
                              (car (vc-git-branches))))
               (github-url (format "https://github.com/%s/%s/blob/%s/%s"
                                   repo-org
                                   repo-name
                                   branch-name
                                   file-in-repo)))
          (kill-new (s-join "" (list github-url
                                     (unless (= start-line-number 1)
                                       (format "#L%s" start-line-number))
                                     (unless (= start-line-number end-line-number)
                                       (format "-L%s" end-line-number)))))))))

(defun set-alacritty-font-size (font-size)
  "Set the font size in Alacritty to FONT-SIZE."
  (message "tried to set alacritty font-size to %s" font-size)
  ;; (call-process-shell-command (format "alacritty msg config font.size=%s -w -1" font-size))
  )

;; (transient-define-prefix dc-restart-services ()
;;   "Transient for managing window manager commands."
;;   ["service"
;;    ("y" "yabai" restart-yabai)
;;    ("s" "skhd" restart-skhd)
;;    ("e" "emacs" restart-emacs)
;;    ])

(provide 'dc-functions)

;;; dc-functions.el ends here
