;;; dc-rust.el --- Rust -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Rust

;;; Code:

(use-package rustic
  :custom
  (rustic-lsp-client 'eglot)
  (rustic-format-trigger 'on-save))

(provide 'dc-rust)

;;; dc-rust.el ends here
