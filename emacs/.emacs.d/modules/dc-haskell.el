;;; dc-haskell.el --- haskell language packages -*- lexical-binding: t -*-

;;; Commentary:

;; commentary

;;; Code:

;; (use-package lsp-haskell
;;   :straight (:host github :repo "emacs-lsp/lsp-haskell"))
(use-package haskell-mode
  :straight (:host github :repo "haskell/haskell-mode")
  :requires (lsp lsp-haskell)
  :bind
  (:map haskell-mode-map
        ("C-c C-p" . run-haskell))
  ;; :config (require 'lsp-haskell)
  )

(provide 'dc-haskell)

;;; dc-haskell.el ends here
