;;; dc-modeline.el --- modeline -*- lexical-binding: t -*-

;;; Commentary:

;; commentary

;;; Code:

;; modeline

;; flash mode line on error instead of a visual or audible bell
(setq visible-bell nil
      ring-bell-function 'double-flash-mode-line
      mode-line-compact nil)

(defun double-flash-mode-line ()
  "Flash the mode line two times instead of the screen."
  (let ((flash-sec (/ 1.0 20)))
    (invert-face 'mode-line)
    (run-with-timer flash-sec nil #'invert-face 'mode-line)
    (run-with-timer (* 2 flash-sec) nil #'invert-face 'mode-line)
    (run-with-timer (* 3 flash-sec) nil #'invert-face 'mode-line)))

;; via https://github.com/sunnyhasija/Academic-Doom-Emacs-Config
(use-package doom-modeline
  :when dc/doom-modeline
  :commands
  (doom-modeline-conditional-buffer-encoding doom-modeline-conditional-vcs-length)
  :hook
  (window-setup . doom-modeline-mode)
  (find-file-hook . doom-modeline-conditional-vcs-length)
  (after-save-hook . doom-modeline-conditional-vcs-length)
  (after-change-major-mode-hook . doom-modeline-conditional-buffer-encoding)
  ;; :init
  ;; (display-battery-mode t)
  :custom
  (doom-modeline-checker-simple-format nil)
  (doom-modeline-hud t)
  ;; (doom-modeline-bar-width 5)
  (doom-modeline-buffer-file-name-style 'relative-to-project)
  (doom-modeline-buffer-encoding nil)
  (doom-modeline-checker-simple-format t)
  (doom-modeline-enable-word-count t)
  (doom-modeline-env-version t)
  (doom-modeline-icon t)
  (doom-modeline-indent-info t)
  (doom-modeline-lsp t)
  (doom-modeline-major-mode-color-icon t)
  (doom-modeline-major-mode-icon t)
  ;; (doom-modeline-minor-modes nil)
  ;; (doom-modeline-modal-icon nil)
  (doom-modeline-persp-name t)
  (doom-modeline-display-default-persp-name t)
  (doom-modeline-python-executable "python3")
  (doom-modeline-vcs-max-length 30)
  (doom-modeline-workspace-name t)
  :config
  (defun doom-modeline-conditional-buffer-encoding ()
    (setq-local doom-modeline-buffer-encoding
                (unless (or (eq buffer-file-coding-system 'utf-8-unix)
                            (eq buffer-file-coding-system 'utf-8)))))
  (defun doom-modeline-conditional-vcs-length ()
    (setq-local doom-modeline-vcs-max-length
                (length (magit-get-current-branch))))
  (advice-add #'vc-refresh-state :after #'doom-modeline-conditional-vcs-length)
  ;; (set-face-attribute 'tab-bar-tab nil :inherit 'doom-modeline-panel :foreground nil :background nil)
  )

(provide 'dc-modeline)

;;; dc-modeline.el ends here
