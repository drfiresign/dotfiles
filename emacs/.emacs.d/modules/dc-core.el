;;; dc-core.el --- Core behaviors for emacs -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Sets many of the default options for Emacs as a whole. Covers things like
;; file handling, state mangement, scrolling, history, saving, etc.

;;; Code:

(defvar dc/term nil)
(when (equal (window-system) nil)
  (setq dc/term t))

(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
;; run at full power please
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'set-goal-column 'disabled nil)
;; stop trying to change font with kbd
(put 'text-scale-increase 'disabled t)
(put 'text-scale-decrease 'disabled t)

;; add calling last macro to repeat-map
(put 'kmacro-end-and-call-macro 'repeat-map 'kmacro-keymap)

;;; environment -- variables, clipboard, mouse, window management, terminal

(global-auto-revert-mode) ;; this may cause a problem of
;; having too many files open at once on macos. this causes a problem with a lot
;; of open eglot/lsp-mode projects as the servers are set to watch files in the
;; project directories. not using this revert mode option reduces the number of
;; open files. fix for now is to modify the custom values when eglot is called
;; for the revert function.
(column-number-mode t)
(repeat-mode 1)

(setq-default
 backup-by-copying t                      ; don't clobber symlinks
 completion-ignore-case t                 ; tab completion
 create-lockfiles nil                     ; disable lockfiles
 cursor-type 'box
 ;; cursor-in-non-selected-windows nil
 ;; blink-cursor-delay 0.3
 ;; blink-cursor-interval 0.3
 ;; blink-cursor-blinks 0
 custom-theme-directory (expand-file-name "custom-themes/" user-emacs-directory)
 dabbrev-case-fold-search 'case-fold-search
 dabbrev-upcase-means-case-search 'nil
 default-buffer-file-coding-system 'utf-8
 delete-old-versions t                    ; don't ask about deleting old versions
 display-buffer-base-action '((display-buffer-reuse-window display-buffer-same-window)
                              (reusable-frames . t) ; or 'visible
                              (inhibit-switch-frame . t))
 ediff-keep-variants nil
 ediff-split-window-function #'split-window-horizontally
 ediff-window-setup-function #'ediff-setup-windows-plain
 enable-recursive-minibuffers t           ; does what it says on the tin
 eval-espression-print-length nil
 even-window-sizes nil
 fill-column 90
 fringes-outside-margins nil
 global-auto-revert-non-file-buffers t
 indent-tabs-mode nil                     ; always indent with spaces
 ;; Info-hide-note-references nil
 jsonrpc-default-request-timeout 30       ; wait 30 seconds for response from LSP servers
 kept-new-versions 2                      ; keep 4 latest versions
 kept-old-versions 1                      ; keep 2 previous versions
 kill-ring-max 200                        ; self-explanatory
 large-file-warning-threshold (* 500 1024 1024) ; 50MB
 lexical-binding t
 load-prefer-newer t
 message-log-max 16384                    ; more messages in *messages* buffer
 minibuffer-depth-indicate-mode t
 package-install-upgrade-built-in t
 read-buffer-completion-ignore-case t     ; ''
 ;; read-extended-command-predicate #'command-completion-default-include-p
 read-file-name-completion-ignore-case t  ; ''
 read-process-output-max (* 2048 2048)    ; read large blobs of data (recommended by `lsp-doctor')
 recenter-positions '(middle top bottom)
 require-final-newline nil                ; require files have a newline at the end
 save-interprogram-paste-before-kill t    ; save clipboard contents before replacement
 save-place-mode t                        ; save my point location in all files
 savehist-file "~/.emacs.d/savehist"      ; savehist minibuffer file location
 scroll-conservatively 0                  ; only scroll as far as the point goes
 set-mark-command-repeat-pop t            ; pop mark without repeated C-u prefix
 split-height-threshold 50
 split-width-threshold 100
 sentence-end-double-space nil            ; single space is a sentence
 system-uses-terminfo t
 switch-to-buffer-obey-display-actions t
 switch-to-buffer-in-dedicated-window 'pop
 tab-always-indent t                      ; 'complete
 tab-width 2                              ; too many spaces for a tab. less of these
 tags-add-tables nil                      ; NO TAGS
 tramp-ssh-controlmaster-options ""
 use-dialog-box nil
 use-file-dialog nil
 view-read-only t
 warning-suppress-log-types '((use-package use-feature)))

(use-package exec-path-from-shell
  :demand t
  :custom
  (exec-path-from-shell-shell-name "fish")
  (exec-path-from-shell-variables '("PATH" "MANPATH" "GOPATH" "GOPRIVATE" "GOPROXY" "GONOPROXY"))
  :init
  (exec-path-from-shell-initialize)
  (setenv "LSP_USE_PLISTS" "true"))

(use-feature emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; Alternatively try `consult-completing-read-multiple'.
  (defun crm-indicator (args)
    (cons (concat "[multi,] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
  :config
  (custom-set-faces
   '(region ((t :extend t))))
  ;; (defvar mark-thing-map (define-prefix-command 'mark-thing-map)
  ;;   "Custom, mnemonic, bindings for (some) mark commands.")

  ;; (define-key mark-thing-map (kbd "w") #'mark-word)
  ;; (define-key mark-thing-map (kbd "d") #'mark-defun)
  ;; (define-key mark-thing-map (kbd "f") #'mark-defun) ;; welp...
  ;; (define-key mark-thing-map (kbd "s") #'mark-sexp)
  ;; (define-key mark-thing-map (kbd "p") #'mark-paragraph)
  ;; (define-key mark-thing-map (kbd "e") #'mark-end-of-sentence)
  ;; (define-key mark-thing-map (kbd "h") #'mark-whole-buffer)
  ;; (global-unset-key (kbd "C-x h"))
  ;; (global-set-key (kbd "M-SPC") 'mark-thing-map)
  )

(use-package sensible-defaults
  :demand t
  :straight (:host github :repo "hrs/sensible-defaults.el" :fork (:host github :repo "drfiresign/sensible-defaults.el"))
  :commands (sensible-defaults/use-all-settings
             sensible-defaults/bind-commenting-and-uncommenting
             sensible-defaults/backup-to-temp-directory))

(sensible-defaults/use-all-settings)
(sensible-defaults/bind-commenting-and-uncommenting)
(sensible-defaults/backup-to-temp-directory)

;; files -- saving, finding
(use-package savehist
  :ensure nil
  :hook (after-init . savehist-mode))

;; `super-save' saves a buffer when switching frames
(use-package super-save
  :disabled t
  :config
  (super-save-mode +1)
  (add-to-list 'super-save-hook-triggers 'find-file-hook)
  :custom
  (super-save-auto-save-when-idle t))

;; best file management
(use-package crux
  :demand t
  :bind
  (("C-c r d" . crux-duplicate-and-comment-current-line-or-region)
   ("C-M-o" . crux-smart-open-line-above)
   ("C-M-j" . crux-smart-open-line)
   ("C-c x 4 t" . crux-transpose-windows)
   ("C-c x <backspace>" . crux-kill-line-backwards)
   ("C-c x K" . crux-kill-whole-line)
   ("C-c x b" . crux-switch-to-previous-buffer)
   ("C-c x c" . crux-cleanup-buffer-or-region)
   ("C-c x j" . crux-top-join-line)
   ("C-c x k" . crux-smart-kill-line)
   ("C-c x o" . crux-smart-open-line)
   ("C-c x r b" . crux-rename-buffer-and-file)
   ("C-c x r f" . crux-rename-file-and-buffer)
   ("C-c x x" . crux-create-scratch-buffer)
   ("C-c x s u d o" . crux-sudo-edit))
  :init
  (defun dc/crux-other-window (arg)
    (interactive "P")
    (pcase (car arg)
      (4 (crux-swap-windows 1))
      (16 (crux-swap-windows -1))
      (_ (crux-other-window-or-switch-buffer))))
  :config
  (progn                                  ; add crux commands to repeat
    (defvar crux-repeat-map
      (-let [map (make-sparse-keymap)]
        (define-key map "j" 'crux-top-join-line)
        (define-key map "o" 'crux-smart-open-line)
        (define-key map "O" 'crux-smart-open-line-above)
        map)
      "Repeating map for crux line opening.")
    (put 'crux-top-join-line 'repeat-map 'crux-repeat-map)
    (put 'crux-smart-open-line 'repeat-map 'crux-repeat-map)
    (put 'crux-smart-open-line-above 'repeat-map 'crux-repeat-map)))

;;;; filesystem stuff
;; clean up some things overnight because I leave Emacs running 24/7
(midnight-mode)

(use-package vlf) ;; open 2gb files, I dare you.

;; When deleting a file interactively, move it to the trash instead.
(setq-default delete-by-moving-to-trash t)

(use-package helpful
  :init
  (when (> emacs-major-version 28)
    ;;  NOTE[dcascio|2022-02-03] See Wilfred/elisp-refs#35. This variable is
    ;;  required in elisp-refs, a dependency of helpful. Remove once fixed
    ;;  upstream.
    (defvar read-symbol-positions-list nil))
  :bind (("C-h f" . helpful-callable)
         ("C-h v" . helpful-variable)
         ("C-h k" . helpful-key)
         ("C-h o" . helpful-symbol)
         ("C-h F" . helpful-function)
         ("C-h C" . helpful-command)
         (:map emacs-lisp-mode-map
               ("C-c C-d" . helpful-at-point))
         (:map helpful-mode-map
               ("C-c C-d" . helpful-at-point)))
  :config
  (advice-add #'describe-function :override #'helpful-function)
  (advice-add #'describe-variable :override #'helpful-variable)
  (advice-add #'describe-symbol :override #'helpful-symbol)
  (advice-add #'describe-key :override #'helpful-key)
  (advice-add #'describe-command :override #'helpful-command))

;; (use-package which-key
;;   :when dc/which-key
;;   :config
;;   (defun which-key-prefix-first-then-key-order-alpha (acons bcons)
;;     "Order first by whether A and/or B is a prefix with no prefix
;; coming before a prefix. Within these categories order using
;; `which-key-key-order-alpha'."
;;     (let ((apref? (which-key--group-p (cdr acons)))
;;           (bpref? (which-key--group-p (cdr bcons))))
;;       (if (not (eq apref? bpref?))
;;           (and apref? (not bpref?))
;;         (which-key-key-order-alpha acons bcons))))
;;   :custom
;;   (which-key-sort-uppercase-first nil)
;;   (which-key-sort-order 'which-key-prefix-first-then-key-order-alpha)
;;   (which-key-show-transient-maps t)
;;   :init
;;   (which-key-mode)
;;   (which-key-setup-side-window-bottom))



(provide 'dc-core)
;;; dc-core.el ends here
