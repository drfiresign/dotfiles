;;; dc-kubernetes.el --- kubernetes
;;
;;; Commentary:
;; Kubernetes configuration
;;; Code:

(use-package kele
  :straight t
  :config
  (kele-mode 1)
  (define-key kele-mode-map (kbd "s-k") kele-command-map)
  :custom
  (kele-filtered-fields '((metadata managedFields)
                          (metadata annotations kubectl.kubernetes.io/last-applied-configuration)))
  (kele-get-show-instructions nil))

(use-package kubedoc :straight (:host github :repo "r0bobo/kubedoc.el"))


(provide 'dc-kubernetes)
;;; dc-kubernetes.el ends here
