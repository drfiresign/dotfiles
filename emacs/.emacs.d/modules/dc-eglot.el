;;; dc-eglot.el --- eglot controls
;;
;;; Commentary:
;; Customization for Eglot
;;; Code:

(use-package eglot
  :custom
  (eglot-extend-to-xref nil)
  :hook
  ((
    go-mode
    go-ts-mode
    js-mode
    lua-mode
    lua-ts-mode
    markdown-mode
    nix-mode
    python-mode
    python-ts-mode
    rust-mode
    rust-ts-mode
    rustic-mode
    shell-mode
    ) . eglot-ensure)
  :config
  (setq-default eglot-workspace-configuration
                '((:gopls .
                          ((staticcheck . t)
                           (matcher . "CaseSensitive")))))
  (add-to-list 'eglot-server-programs '(go-ts-mode . ("gopls")))
  (add-to-list 'eglot-server-programs '(python-ts-mode . ("ruff-lsp")))
  (add-to-list 'eglot-server-programs '(python-mode . ("ruff-lsp")))
  (add-to-list 'eglot-server-programs '(nix-mode . ("nixd")))
  )

(use-package flycheck-eglot :straight (:host github :repo "intramurz/flycheck-eglot")
  :after (flycheck eglot)
  :init (global-flycheck-eglot-mode)
  :custom
  (flycheck-eglot-exclusive nil))

(provide 'dc-eglot)
;;; dc-eglot.el ends here
