;;; dc-parens.el --- parentheses -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Parenthesis

;;; Code:

(use-package puni
  :defer t
  :bind
  ("C-}" . puni-barf-forward)
  ("C-{" . puni-barf-backward)
  ("C-)" . puni-slurp-forward)
  ("C-(" . puni-slurp-backward)
  ("C-|" . puni-split)
  ("C-^" . puni-raise)
  ("C-S-SPC" . puni-mark-sexp-at-point)
  ("C-S-s" . puni-mark-sexp-around-point)
  ("M-S-SPC" . puni-mark-list-around-point)
  ([remap transpose-sexps] . puni-transpose)

  :init
  ;; The autoloads of Puni are set up so you can enable `puni-mode` or
  ;; `puni-global-mode` before `puni` is actually loaded. Only after you press
  ;; any key that calls Puni commands, it's loaded.
  (puni-global-mode)
  :hook
  (prog-mode . electric-pair-mode)
  ((term-mode vterm-mode) . puni-disable-puni-mode))

(use-package surround
  :ensure t
  :straight (:host github :repo "mkleehammer/surround")
  :bind-keymap ("M-'" . surround-keymap))

;; (use-package lispy
;;   :defer t
;;   :hook (emacs-lisp-mode . lispy-mode)
;;   :config
;;   (defun conditionally-enable-lispy ()
;;     (when (eq this-command 'eval-expression)
;;       (lispy-mode 1)))
;;   (add-hook 'minibuffer-setup-hook 'conditionally-enable-lispy))

(provide 'dc-parens)
;;; dc-parens.el ends here
