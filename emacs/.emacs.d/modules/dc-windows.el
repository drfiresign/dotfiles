;;; dc-windows.el --- window and frame management -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;; window and buffer management

(follow-mode 1)

(add-to-list 'display-buffer-alist `('(,(rx (| "\\*Help\\*" "\\*helpful" "\\*info\\*"))
                                       (display-buffer-in-atom-window display-buffer-pop-up-window display-buffer-reuse-window))))

(use-package rotate
  :disabled t
  :straight (:type git :host github :repo "daichirata/emacs-rotate"))

(unless dc/exwm
  (use-package golden-ratio.el
    :straight (:host github :repo "roman/golden-ratio.el")
    :config (golden-ratio-mode 1)
    :custom
    (golden-ratio-auto-scale t)
    (golden-ratio-adjust-factor .8)
    (golden-ratio-wide-adjust-factor .8)
    ;; (golden-ratio-max-width 102)
    :bind ("s-\`" . golden-ratio)))

(use-package ace-window
  :custom
  (aw-scope 'frame)
  (aw-keys '(?a ?r ?s ?t ?n ?e ?i ?o ?d ?h))
  (aw-ignored-buffers '("*Calc Trail*" " *LV*"))
  ;; :config (ace-window-display-mode)
  :bind
  ("M-o" . ace-window))

(winner-mode 1)

(provide 'dc-windows)

;;; dc-windows.el ends here
