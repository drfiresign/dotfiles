;;; dc-mail.el --- email -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; commentary

;;; Code:

;; (require 'notmuch)

;; ;; (advice-add 'notmuch-refresh-this-buffer :before 'dc/fetch-mail)

(setq mail-user-agent 'mu4e-user-agent

      mu4e-maildir "/home/dcascio/.mail"
      mu4e-attachment-dir "~/downloads"
      mu4e-sent-folder "/sent"
      mu4e-drafts-folder "/drafts"
      mu4e-trash-folder "/trash"
      mu4e-refile-folder "/archive"

      user-mail-address "dcascio@protonmail.com"
      user-full-name  "Dylan Cascio"
      mu4e-compose-signature "---\n~ d/c"

      mu4e-get-mail-command "mbsync"
      mu4e-update-interval 120
      mu4e-change-filenames-when-moving t

      message-send-mail-function 'smtpmail-send-it
      smtpmail-smtp-server "127.0.0.1"
      smtpmail-smtp-service 1025
      message-kill-buffer-on-exit t
      ;; smtpmail-auth-credentials "~/.authinfo.gpg" ;; Here I assume you encrypted the credentials
      )

(provide 'dc-mail)
