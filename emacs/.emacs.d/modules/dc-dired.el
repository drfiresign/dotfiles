;;; dc-dired.el --- dired customization -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; Do some dired things

;;; Code:

(use-package all-the-icons-dired
  :disabled t
  :straight (:host github :repo "jtbm37/all-the-icons-dired"))
(put 'dired-find-alternate-file 'disabled nil)

(use-package diredfl
  :straight (:host github :repo "purcell/diredfl")
  :hook (dired-mode . diredfl-mode))

(use-package dired-auto-readme :straight (:host github :repo "amno1/dired-auto-readme")
  :config (setq dired-auto-readme-file '("manifest\\W" "readme\\.\\(org\\|rst\\|md\\|markdown\\)")))

(use-feature dired
  :ensure nil
  :commands (dired)
  :defines dired-mode-map
  :bind
  (:map dired-mode-map
        ("C-c C-p" . wdired-change-to-wdired-mode)
        ("M-p" . dired-prev-subdir)
        ("M-n" . dired-next-subdir)
        ("V" . dc/dired-view-file-other-window))
  :custom
  (dired-ls-F-marks-symlinks t)
  (dired-listing-switches  "-l --group-directories-first --almost-all --escape --dired --classify --human-readable --dereference")
  (dired-auto-revert-buffer t)
  (dired-recursive-copies 'always)
  (dired-recursive-deletes 'always)
  (delete-by-moving-to-trash t)
  (dired-dwim-target t)
  :hook
  ((dired-mode . dired-auto-readme-mode)
   (dired-mode . dired-hide-details-mode)
   (dired-mode . hl-line-mode)))

(use-package dired-subtree
  :ensure t
  :after dired
  :bind
  ( :map dired-mode-map
    ("<tab>" . dired-subtree-toggle)
    ("TAB" . dired-subtree-toggle)
    ("<backtab>" . dired-subtree-remove)
    ("S-TAB" . dired-subtree-remove))
  :custom
  (dired-subtree-use-backgrounds nil))

(use-package trashed
  :ensure t
  :commands (trashed)
  :custom
  (trashed-action-confirmer 'y-or-n-p)
  (trashed-use-header-line t)
  (trashed-sort-key '("Date deleted" . t))
  (drashed-date-format "%Y-%m%d %H:%M:%S"))

(provide 'dc-dired)

;;; dc-dired.el ends here
