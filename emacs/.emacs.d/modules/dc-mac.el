;;; dc-mac.el --- macOS specifics -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; there are some things that I've added to the config that have been specific
;; to macOS. They're going to live here now.

;;; Code:

(defun port-p (bin)
  "Check for BIN in macports installed coreutils path."
  (let ((port (concat "/opt/local/libexec/gnubin/" bin)))
    (when (file-exists-p port) port)))

(defun brew-p (bin)
  "check for BIN in homebrew installed coreutils path."
  (let ((brew (concat "/usr/local/bin/" bin)))
    (when (file-exists-p brew) brew)))

(defun find-bin (bin &optional gnu-prefix &rest args)
  "Check for BIN in macports, homebrew, and macos system tools paths."
  (-let ((port (port-p bin))
         (brew (brew-p (if gnu-prefix
                           (concat "g" bin)
                         bin)))
         (system (user-bin-p bin))
         ((arg1 arg2 arg3) args))
    (cond
     (port (if arg1
               (s-join " " (list port arg1))
             port))
     (brew (if arg1
               (s-join " " (list brew (or arg2 arg1)))
             brew))
     (system (if arg1
                 (s-join " " (list system (or arg3 arg2 arg1)))
               system)))))

(defvar dc/user-home (file-name-as-directory (concat "/Users/" (user-login-name)))
  "MacOS User $HOME directory.")
(defvar dc/rg-path (find-bin "rg")
  "Location of Ripgrep binary.")
(setq-local rg-this-dir (list "."))
(defvar dc/rg-ignore-glob (list "--glob=!.git" "--glob=!vendor" "--glob=!TAGS")
  "List of file globs which `rg' should ignore.")
(defvar dc/rg-display-pref (list "--null" "--line-buffered" "--color=never" "--max-columns=1000"
                                 "--path-separator /" "--smart-case" "--hidden" "--no-heading"
                                 "--line-number" "--search-zip")
  "List of `rg' arguments which generally cover output display.")
(defvar dc/rg-args-list (nconc (list dc/rg-path)
                               dc/rg-display-pref
                               dc/rg-ignore-glob
                               ;; rg-this-dir
                               )
  "Combined list of all preferred `rg' arguments.")
(defvar dc/rg-args-string (s-join " " dc/rg-args-list)
  "String containing all preferred `rg' arguments.")
(defvar dc/locate-args (find-bin "locate" t "--ignore-case --existing --regex" nil "-i")
  "String to use for any Locate searches.")
(defvar dc/man-args (find-bin "man" nil "-k"))

(setq-default
 mac-command-modifier 'super
 mac-right-command-modifier 'super
 mac-right-control-modifier 'control
 mac-option-modifier 'meta ; option
 mac-right-option-modifier 'hyper
 mac-function-modifier 'hyper
 mac-ignore-accessibility t        ; ignore mac accessibility features
 mac-pass-command-to-system nil    ; do not pass command key presses to system

 ;; macos binary locations
 magit-git-executable (find-bin "git")
 insert-directory-program (find-bin "ls" t))

(defun dc/swap-macbook-keyboard-modifiers ()
  "Toggle which physical keys map to which modifiers.
This will swap the keys which are used for super and control.
It's intended use is to enable easier use of the built in
keyboard on a macbook."
  (interactive)
  (setq mac-right-control-modifier
        (pcase mac-right-control-modifier
          ('control 'meta)
          (_ 'control)))
  (message "right ctrl (cmd) is set to: %s" mac-right-control-modifier))

(when (find-bin "gmake")
  (add-to-list 'auto-mode-alist '("[Mm]akefile\\'" . makefile-gmake-mode)))

;; `osx-browse' helps ensure that we can use firefox to open stuff.
;; (use-package osx-browse
;;   :straight (:host github :repo "rolandwalker/osx-browse")
;;   :custom
;;   (osx-browse-prefer-browser "org.mozilla.firefox")
;;   (browse-url-dwim-always-confirm-extraction nil)
;;   (browse-url-browser-function 'osx-browse-url)
;;   (browse-url-secondary-browser-function 'browse-url-dwim)
;;   (osx-browse-prefer-background t)
;;   (osx-browse-install-aliases t)
;;   (osx-browse-url-keystrokes nil)
;;   (osx-browse-guess-keystrokes nil)
;;   (ns-use-thin-smoothing t)
;;   :config
;;   (osx-browse-mode 1)
;;   (global-unset-key (kbd "s-f"))
;;   ;; (global-unset-key (kbd "s-c"))
;;   ;; (global-unset-key (kbd "s-x"))
;;   (global-unset-key (kbd "s-s"))
;;   (global-unset-key (kbd "s-b"))
;;   (global-unset-key (kbd "s-i")))

(use-package dash-at-point :straight (:host github :repo "stanaka/dash-at-point")
  :config
  (cl-dolist (mode '((javascript-mode . "javascript")
                     (rustic-mode . "rust")))
    (add-to-list 'dash-at-point-mode-alist mode))
  :bind
  ("C-c d" . dash-at-point)
  ("C-c e" . dash-at-point-with-docset))

;; miscellaneous funcs
(defun --restart (service)
  "Restart a brew managed SERVICE."
  (call-process (format "/usr/local/bin/%s" service) nil nil nil  "--restart-service"))
;; (defun restart-sketchybar () "Restart sketchybar homebrew services daemon." (interactive) (--restart "sketchybar"))
(defun restart-yabai () "Restart yabai homebrew services daemon." (interactive) (--restart "yabai"))
(defun restart-skhd () "Restart skhd homebrew services daemon." (interactive) (--restart "skhd"))

(require 'dc-yabai)

(provide 'dc-mac)
;;; dc-mac.el ends here
