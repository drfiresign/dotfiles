;;; dc-yabai.el --- yabai controls -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio

;;; Commentary:

;; via: https://gist.github.com/ethan-leba/760054f36a2f7c144c6b06ab6458fae6
;; Finally integrated Yabai and skhd and here, we marry window movement to Emacs window movement.

;;; Code:
(defun yabai-move-on-error (move-fn pairs)
  "Attempt to call MOVE-FN. On error, instead call out to yabai
process to move user focus in DIRECTION. DIRECTION is a cons of
the selector and the cardinal direction to look for that
selector."
  (interactive)
  (condition-case nil
      (funcall move-fn)
    (user-error
     (let ((default-directory "~/")
           (cmds (cl-loop for p in pairs
                          collect (format "yabai -m %s --focus %s" (car p) (cdr p)))))
       (call-process-shell-command (string-join cmds " || ") nil 0)))))

(defun yabai-window-left ()
  (interactive)
  (yabai-move-on-error #'windmove-left '((window . west)
                                         (display . west)
                                         (display . east))))

(defun yabai-window-right ()
  (interactive)
  (yabai-move-on-error #'windmove-right '((window . east)
                                          (display . east)
                                          (display . west))))

(defun yabai-window-up ()
  (interactive)
  (yabai-move-on-error #'windmove-up '((window . north))))

(defun yabai-window-down ()
  (interactive)
  (yabai-move-on-error #'windmove-down '((window . south))))

;; These two aren't necessary apparently?
(defun yabai-stack-up ()
  (interactive)
  (yabai-move-on-error #'error '((window . stack.next))))

(defun yabai-stack-up ()
  (interactive)
  (yabai-move-on-error #'error '((window . stack.prev))))

(use-package clippo
  :straight (:host github :repo "anonimitoraf/emacs-clippo"))


(provide 'dc-yabai)
;;; dc-yabai.el ends here
