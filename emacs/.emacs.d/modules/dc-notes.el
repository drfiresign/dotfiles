;;; dc-notes.el --- Configuration for note-taking
;;
;;; Commentary:
;;
;;; Code:

(defvar dc/oncall-alert-names '(""))

(use-package denote
  :straight (:type git :host github :repo "protesilaos/denote")
  :config
  (setq denote-directory (expand-file-name "~/Documents/org/")
        denote-known-keywords '()
        denote-infer-keywords t
        denote-sort-keywords t
        denote-file-type nil
        denote-prompts '(title keywords)
        denote-excluded-directories-regexp nil
        denote-excluded-keywords-regexp nil
        ;; Pick dates, where relevant, with Org's advanced interface:
        denote-date-prompt-use-org-read-date t
        denote-allow-multi-word-keywords t
        denote-date-format nil          ; read doc string

        ;; By default, we do not show the context of links. We just display file
        ;; names. This provides a more informative view. Also see
        ;; `denote-link-backlinks-display-buffer-action' which is a bit
        ;; advanced.
        denote-backlinks-show-context t)
  (defun dc/denote-oncall ()
    (interactive)
    ;; TODO[dcascio|2023-10-13] How do I keep track of entries/history here and make the command work
    (let ((alert (completing-read "Alert: " dc/oncall-alert-names nil nil nil dc/oncall-alert-names)))
      (add-to-list dc/oncall-alert-names alert)
      (denote alert '("oncall") nil "oncall")))
  ;; (with-eval-after-load 'org-capture
  ;;   (setq denote-org-capture-specifiers "%l\n%i\n%?")
  ;;   (add-to-list 'org-capture-templates
  ;;                '("n" "New note (with denote.el)" plain
  ;;                  (file denote-last-path)
  ;;                  #'denote-org-capture
  ;;                  :no-save t
  ;;                  :immediate-finish nil
  ;;                  :kill-buffer t
  ;;                  :jump-to-captured t)))

  ;; Also check the commands `denote-link-after-creating',
  ;; `denote-link-or-create'.  You may want to bind them to keys as well.

  ;; If you want to have Denote commands available via a right click
  ;; context menu, use the following and then enable
  ;; `context-menu-mode'.
  (add-hook 'context-menu-functions #'denote-context-menu)

  :bind
  ("C-c n n" . denote)
  ("C-c n c" . denote-region)  ; "contents" mnemonic
  ("C-c n N" . denote-type)
  ("C-c n d" . denote-date)
  ("C-c n z" . denote-signature) ; "zettelkasten" mnemonic
  ("C-c n s" . denote-subdirectory)
  ("C-c n t" . denote-template)
  ("C-c n r" . denote-rename-file)
  ("C-c n R" . denote-rename-file-using-front-matter)
  (:map org-mode-map
        ("C-c n i" . denote-link) ; "insert" mnemonic
        ("C-c n I" . denote-add-links)
        ("C-c n b" . denote-backlinks)
        ("C-c n f f" . denote-find-link)
        ("C-c n f b" . denote-find-backlink))
  (:map dired-mode-map
        ("C-c C-d C-i" . denote-link-dired-marked-notes)
        ("C-c C-d C-r" . denote-dired-rename-marked-files)
        ("C-c C-d C-R" . denote-dired-rename-marked-files-using-front-matter)))

(use-package consult-notes
  :straight (:type git :host github :repo "mclear-tools/consult-notes")
  :after (consult denote)
  :commands (consult-notes
             consult-notes-search-in-all-notes)
  :config
  (setq consult-notes-file-dir-sources `(("On-Call"  ?o  "~/org/oncall/")
                                         ("Notes"    ?n  ,(denote-directory))))
  ;; Set org-roam integration, denote integration, or org-heading integration e.g.:
  ;; (setq consult-notes-org-headings-files '("~/path/to/file1.org"
  ;;                                          "~/path/to/file2.org"))
  ;; (consult-notes-org-headings-mode)
  (when (locate-library "denote")
    (consult-notes-denote-mode))
  ;; search only for text files in denote dir
  ;; (setq consult-notes-denote-files-function (function denote-directory-text-only-files))
  )

;;; Older attempts at getting my life sorted out:

;; (use-package org-journal
;;   ;; :init
;;   ;; (setq-default org-journal-prefix-key "C-c o ")
;;   :bind
;;   ("C-c o o" . dc/org-journal-oncall)  ; "o" is for oncall
;;   ("C-c o T" . org-journal-new-entry)
;;   ("C-c o t" . dc/org-journal-curr-entry)
;;   :custom
;;   (org-journal-file-header "# -*- mode: org; mode: org-journal; -*-")
;;   (org-journal-dir "~/org/daily-journal/")
;;   (org-journal-file-type 'weekly)
;;   ;; For purposes on oncall shifts, weeks start on Thursday
;;   (org-journal-start-on-weekday 1)
;;   ;; (org-journal-created-property-timestamp-format "%Y%m%d")
;;   ;; (org-journal-file-format "%Y%m%d")
;;   (org-journal-find-file '(lambda (file)
;;                             (interactive)
;;                             (find-file-other-window file)
;;                             (dc/override-keybinding-in-buffer
;;                              (kbd "C-c C-c")
;;                              '(lambda ()
;;                                 (interactive)
;;                                 (save-buffer)
;;                                 (delete-window)))
;;                             (dc/override-keybinding-in-buffer
;;                              (kbd "C-c C-k")
;;                              '(lambda ()
;;                                 (interactive)
;;                                 (revert-buffer t t)
;;                                 (delete-window))))))
;; (defun dc/org-journal-oncall ()
;;   "Open today's journal task file. CREATE a new entry if provided."
;;   (interactive)
;;   (dlet ((org-journal-dir "~/org/oncall/")
;;          (org-journal-start-on-weekday 4))
;;     (org-journal-new-entry nil)))
;; (defun dc/org-journal-curr-entry ()
;;   (interactive)
;;   (org-journal-read-or-display-entry (current-time)))

(provide 'dc-notes)
;;; dc-notes.el ends here
