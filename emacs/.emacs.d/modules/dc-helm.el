;;; dc-helm.el --- summary -*- lexical-binding: t -*-

;; Author: Dylan Cascio
;; Maintainer: Dylan Cascio
;; Version: version
;; Package-Requires: (dependencies)
;; Homepage: homepage
;; Keywords: keywords


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; TODO: check out https://oracleyue.github.io/2018/04/24/init-helm/

;;; Code:

;; Via: https://www.manueluberti.eu/emacs/2020/02/22/ripgrepping-with-helm/

(use-package helm-flx
  :init (helm-flx-mode +1))

(use-package helm
  :demand t
  :blackout t
  :bind
  (("C-x C-f" .  helm-find-files)
   ("C-c h" .  helm-command-prefix)
   ("M-x" .  helm-M-x)
   ("C-x b" .  helm-mini)
   ("M-s o" . helm-occur)
   ("M-r" . helm-mark-ring)
   ("C-x C-d" .  helm-browse-project)
   ;; ("C-x C-b" . ibuffer)
   ("C-x C-r" . helm-mini)
   ("C-x b" . helm-buffers-list)
   ("C-x M-b" . helm-bookmarks)
   ;; ("H-r" . helm-all-mark-rings)
   ("H-y" . helm-show-kill-ring)
   ("H-f" . helm-projectile)
   ("H-r" . helm-register)
   ("H-b" . helm-bookmarks)
   ("H-i" . helm-imenu-in-all-buffers)
   (:map helm-command-map
         ("SPC" . helm-all-mark-rings)
         ("o" . helm-occur)
         ("y" .  helm-show-kill-ring))
   (:map helm-map
         ("<tab>" . helm-execute-persistent-action)
         ("C-i" . helm-execute-persistent-action)
         ("C-z" . helm-select-action)
         ("C-M-n" . helm-next-source)
         ("C-M-p" . helm-previous-source)
         ("M-x" . helm-keyboard-quit))
   (:map helm-grep-map
         ("C-s" . isearch-forward)
         ("C-r" . isearch-backward))
   (:map help-map
         ("a" . helm-apropos)
         ("r" . helm-info-emacs)
         ("T" . helm-world-time)
         ("M" . helm-man-woman)))
  :custom
  (helm-M-x-fuzzy-match t)
  (helm-apropos-fuzzy-match t)
  (helm-autoresize-max-height 50)
  (helm-autoresize-min-height 30)
  (helm-boring-buffer-regexp-list '("\\` " "\*helm" "\*Echo Area" "\*Minibuf" "\*Eglot" "\magit" "\*WoMan" "\*Man" "helm-rg" "\*Compile" "\*Fly"))
  (helm-buffer-max-length 30) ;; default is 20
  (helm-buffers-fuzzy-matching t)
  (helm-candidate-number-limit 2000)
  (helm-completion-in-region-fuzzy-match t) ; TODO[dcascio|2021-07-05] what it do?
  (helm-display-buffer-width 80)
  (helm-display-function 'helm-default-display-buffer) ; helm-display-source-at-screen-top
  (helm-display-source-at-screen-top nil)
  ;; (helm-default-display-buffer-functions view-buffer-other-window)
  (helm-display-header-line t) ;; this is the action description
  ;; (helm-echo-input-in-header-line nil)
  (helm-ff-initial-sort-method 'alphabetically)
  (helm-ff-file-compressed-list '("gz" "bz2" "zip" "tgz" "xz"))
  (helm-ff-file-name-history-use-recentf t) ;; alternately use C-x C-r
  (helm-ff-preferred-shell-mode 'vterm-mode)
  (helm-ff-search-library-in-sexp t)
  (helm-full-frame nil)
  (helm-grep-ag-command "rg --hidden --glob !.git --smart-case --no-heading --line-number --color=always --colors 'match:fg:black' --colors 'match:bg:yellow' %s %s %s")
  (helm-grep-ag-pipe-cmd-switches '("--colors 'match:fg:black'" "--colors 'match:bg:yellow'"))
  (helm-grep-default-command  "grep -a -d skip %e -n%cH -e %p %f")
  (helm-grep-default-recurse-command "grep -a -d recurse %e -n%cH -e %p %f")
  (helm-imenu-fuzzy-match t)
  (helm-lisp-fuzzy-completion t)
  (helm-locate-fuzzy-match nil)
  (helm-move-to-line-cycle-in-source t)
  (helm-quick-update nil) ;; do not display invisible candidates
  (helm-recentf-fuzzy-match t) ;; fuzzy matching
  (helm-rg-ripgrep-executable dc/rg-path)
  (helm-scroll-amount 20) ; scroll 20 lines other window using M-<next>/M-<prior>
  (helm-semantic-fuzzy-match t)
  ;; (helm-split-window-default-side 'below) ;; open buffer in another window
  ;; (helm-split-window-in-side-p nil) ;; open helm buffer inside current window, don't occupy whole other window
  (helm-truncate-lines nil)
  (helm-mode-fuzzy-match t)
  (set-mark-command-repeat-pop t)
  (display-time-world-list
   '(("Europe/London" "London")
     ("Europe/Paris" "Paris")
     ("America/New_York" "New York")
     ("America/Los_Angeles" "Portland")
     ("Australia/Perth" "Perth")
     ("Asia/Bangkok" "Bangkok")))
  :config
  (add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)
  (when (executable-find "curl")
    (setq helm-google-suggest-use-curl-p t))
  (helm-mode 1)
  (helm-autoresize-mode 'toggle)

  (defun dc/helm-vterm-history-advice (oldfun &rest args)
    "Advice to load around helm-rg."
    (if current-prefix-arg
        ;; run helm session taking content from ~/.bash_history
        )
    (apply oldfun)))

(use-package helm-descbinds
  :init
  (helm-descbinds-mode 1))
(use-package helm-ls-git)
(use-package helm-flyspell
  :demand t
  :bind
  ("M-$" . helm-flyspell-correct))
(use-package helm-projectile
  :init
  (helm-projectile-on)
  ;; (advice-add 'helm-projectile-rg :before (lambda () (setq helm-candidate-number-limit 2000)))
  ;; (advice-add 'helm-projectile-rg :after (lambda () (setq helm-candidate-number-limit 2000)))
  :bind (("C-x f" . helm-projectile)
         ("C-c p f" . helm-projectile-find-file)))
(use-package helm-rg
  :demand t
  :custom
  (helm-rg-default-glob-string "!.git")
  (helm-rg-default-extra-args "--hidden")
  (helm-rg-default-directory 'git-root)
  :config
  (defun dc/helm-rg-advice (oldfun &rest args)
    "Advice to load around helm-rg."
    (if go-mode-p
        (let ((helm-rg-default-glob-string
               (concat helm-rg-default-glob-string " !vendor")))
          (apply oldfun))
      (apply oldfun)))

  :bind
  (("<f2>" . helm-rg)
   ("H-s" . helm-projectile-rg))
  (:map helm-rg-map
        ("M-b" . backward-word)
        ("C-c C-p" . helm-rg--bounce)
        ("C-M-n" . helm-rg--file-forward)
        ("C-M-p" . helm-rg--file-backward)
        )
  (:map helm-rg--bounce-mode-map
        ("C-c C-k" . kill-current-buffer))
  (:map isearch-mode-map
        ("s-s" . helm-rg-from-isearch)
        ("s-o" . helm-occur-from-isearch)
        ("s-m" . helm-multi-occur-from-isearch)))

(use-package helm-c-yasnippet
  :bind
  (("M-=" . helm-yas-complete)))

(use-package helm-system-packages
  :straight (:host github :repo "emacs-helm/helm-system-packages"))

(use-package helm-gitignore)

(when dc/exwm
  (use-package helm-exwm :straight (:host github :repo "emacs-helm/helm-exwm"))
  ;; (smex-update)
  )

(provide 'dc-helm)

;;; dc-helm.el ends here
